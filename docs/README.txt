How install project:

- install MAMP (https://www.mamp.info/en/)
	* run server MAMP
	* go into phpMyAdmin
	* run script "SQL.txt"
	* check if DB "pandaDB" is created with tables

- Go into folder "panda_node" into Terminal
	* npm install -g mocha (it will be install into npm general)
	* npm update (init project)
	* mocha (from root panda - it should run the tests)
	* WARNING - One of the test need 10s to be runed. For that you need to do "mocha -t 11000"

- After the test have been executed you should see a user "Dovi" into the DB :)
