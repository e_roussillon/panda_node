var assert = require("chai").assert;
var unirest = require('unirest');

var genericTest = require('../generic.js');

var userSQL = require('../../models/userSQL.js');

var logger = require('../../utils/logger.js');
var dbUtil = require('../../utils/dbUtil.js');

var apiToTest = "/api/user/register"
var api = "https://localhost:8080"+apiToTest

var appId = "123456qwerty"
var pseudoMin = "Dov"
var pseudoMax = "qwertyuiopasdfghjklzxcvbnmqwers"
var pseudoCharactersSpecial = "édouard "
var emailInvalide = "dovi@gmail .com"
var birthdayInvalide = "12/1lk2/12"

var email = "dovi@gmail.com"
var pseudo = "Dovi"
var password = "123456"
var deviceId = "123qwe"
var deviceAndroid = "android"
var deviceiOS = "ios"
var deviceOther = "other"
var birthday = "1985-4-03"


var passwordInvalide = "qwer123456"
var pseudoInvalide = "doviqwe123456" 

describe('\n'+apiToTest, function() {

	before(function(done) {
		dbUtil.createPool().getConnection(function(err, connection) {
	    	connection.query(userSQL.clearTablePassword(), function(err, results) {
	    		connection.query(userSQL.clearTable(), function(err, results) {
		    		connection.release();
		    		done();
		    	});
	    	});
	   	});
	});

	after(function() {

	});

	beforeEach(function() {
	  
	});

	describe('App ID', function() {
		it("Test is App ID invalide", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.end(function (response) 
			{
				genericTest.testAPI_ID(response.raw_body)
				done();
			});
		});
	});
	
	describe('Test Pseudo', function() {
		it("Test if Pseudo is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 0);
				assert.equal(json.response.msg, "Pseudo need to be between 4 and 30.");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":0,\"msg\":\"Pseudo need to be between 4 and 30.\"},\"data\":null}")
				done();
			});

		});

		it("Test if Pseudo is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": "" })
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 0);
				assert.equal(json.response.msg, "Pseudo need to be between 4 and 30.");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":0,\"msg\":\"Pseudo need to be between 4 and 30.\"},\"data\":null}")
				done();
			});
		});

		it("Test if Pseudo is under than 4", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudoMin })
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 0);
				assert.equal(json.response.msg, "Pseudo need to be between 4 and 30.");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":0,\"msg\":\"Pseudo need to be between 4 and 30.\"},\"data\":null}")
				done();
			});
		});

		it("Test if Pseudo is more than 30", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudoMax })
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 0);
				assert.equal(json.response.msg, "Pseudo need to be between 4 and 30.");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":0,\"msg\":\"Pseudo need to be between 4 and 30.\"},\"data\":null}")
				done();
			});
		});
	});
	
	describe('Test Email', function() {
		it("Test if Email is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudo})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 1);
				assert.equal(json.response.msg, "Email is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":1,\"msg\":\"Email is Empty\"},\"data\":null}")
				done();
			});

		});

		it("Test if Email is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudo, "email": ""})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 1);
				assert.equal(json.response.msg, "Email is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":1,\"msg\":\"Email is Empty\"},\"data\":null}")
				done();
			});
		});

		it("Test if Email is Invalide", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudo, "email": emailInvalide})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 2);
				assert.equal(json.response.msg, "Email is not valid");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":2,\"msg\":\"Email is not valid\"},\"data\":null}")
				done();
			});
		});
	});

	describe('Test Birthday', function() {
		it("Test if Birthday is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudo, "email": email})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 3);
				assert.equal(json.response.msg, "Birthday is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":3,\"msg\":\"Birthday is Empty\"},\"data\":null}")
				done();
			});
		});

		it("Test if Birthday is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudo, "email": email, "birthday": ""})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 3);
				assert.equal(json.response.msg, "Birthday is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":3,\"msg\":\"Birthday is Empty\"},\"data\":null}")
				done();
			});
		});

		it("Test if Birthday is Invalide", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudo, "email": email, "birthday": birthdayInvalide})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 4);
				assert.equal(json.response.msg, "Birthday is not valid");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":4,\"msg\":\"Birthday is not valid\"},\"data\":null}")
				done();
			});
		});
	});

	describe('Test Password', function() {
		it("Test if Password is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 5);
				assert.equal(json.response.msg, "Password is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":5,\"msg\":\"Password is Empty\"},\"data\":null}")
				done();
			});
		});

		it("Test if Password is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', "")
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 5);
				assert.equal(json.response.msg, "Password is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":5,\"msg\":\"Password is Empty\"},\"data\":null}")
				done();
			});
		});
	});

	describe('Test Platform Device', function() {
		it("Test if Platform Device is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday})
					.end(function (response) 
			{
				genericTest.testDevicePlatform(response.raw_body)
				done();
			});
		});

		it("Test if Platform Device is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday, "platform_device" : ""})
					.end(function (response) 
			{
				genericTest.testDevicePlatform(response.raw_body)
				done();
			});
		});
	});

	describe('Test Device ID', function() {
		it("Test if Device ID is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday, "platform_device": deviceiOS})
					.end(function (response) 
			{
				genericTest.testDeviceID(response.raw_body)
				done();
			});
		});

		it("Test if Device ID is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', "")
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday, "platform_device": deviceiOS})
					.end(function (response) 
			{
				genericTest.testDeviceID(response.raw_body)
				done();
			});
		});
	});

	describe('Authentication', function() {
		it("Test if Authentification with success", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', deviceId)
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday, "platform_device": deviceAndroid})
					.end(function (response) 
			{
				genericTest.testUserFound(response.raw_body)
				done();
			});
		});

		it("Test if Authentification user already exit", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', deviceId)
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday, "platform_device": deviceAndroid})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "CREATE_USER");
				assert.equal(json.response.code, 6);
				assert.equal(json.response.msg, "User allready exist");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"CREATE_USER\",\"code\":6,\"msg\":\"User allready exist\"},\"data\":null}")
				done();
			});
		});
	});	
});