var assert = require("chai").assert;
var unirest = require('unirest');

var genericTest = require('../generic.js');

var filterSQL = require('../../models/filterSQL.js');

var logger = require('../../utils/logger.js');
var dbUtil = require('../../utils/dbUtil.js');

var create_filterAPI = "/api/filter/create_filter"
var get_filterAPI = "/api/filter/get_filters"
var update_filterAPI = "/api/filter/update_filter"
var remove_filterAPI = "/api/filter/remove_filter"
var baseAPI = "https://localhost:8080"

var appId = "123456qwerty"
var email = "dovi@gmail.com"
var password = "123456"
var deviceId = "123qwe"
var deviceAndroid = "android"

describe('\nAPI Filter', function() {

	var token = "empty"
	var filterId = ""

	before(function(done) {
		unirest.post(baseAPI+"/api/user/authentication")
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Password', password)
						.header('Device-Id', deviceId)
						.send({ "email": email, "platform_device": deviceAndroid})
						.end(function (response) 
			{
				dbUtil.createPool().getConnection(function(err, connection) {
			    	connection.query(filterSQL.clearTable(), function(err, results) {
			    		connection.release();

			    		var json = JSON.parse(response.raw_body)
						assert.equal(json.response.status, 200);
						assert.equal(json.response.protocol, "API");
						assert.equal(json.response.method, "GENERIC_USER");
						assert.equal(json.response.code, -1);
						assert.equal(json.response.msg, "ok");
						token = "Bearer " + JSON.parse(json.data).token

			    		done();
			    	});
			   	});
			});
	});

	after(function() {

	});

	beforeEach(function() {
	  
	});

	describe('create_filters', function() {
		it("Test if list if empty", function (done) {
			

		    unirest.post(baseAPI+get_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "FILTER");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"[]\"}")
				done();
			});
		});

		it("Test if can add new filter", function (done) {
			
		    unirest.post(baseAPI+create_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"name": "Hello", "is_highlight": true})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "FILTER");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")
				done();
			});
		});

		it("Test if list contain one item", function (done) {
			
		    unirest.post(baseAPI+get_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "FILTER");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				var data = JSON.parse(json.data)
				assert.equal(data.length, 1);
				assert.equal(data[0]["name"], "Hello");
				assert.equal(data[0]["isHighlight"], true);
				assert.equal(data[0]["user_id"], "1");
				filterId = data[0]["_id"]
				done();
			});
		});
	});

	describe('update-filters', function() {
		it("Test if can update filter", function (done) {
			
		    unirest.post(baseAPI+update_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"name": "Lala", "is_highlight": false, "filter_id" : filterId})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "FILTER");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")
				done();
			});
		});

		it("Test if item is updated", function (done) {
			
		    unirest.post(baseAPI+get_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "FILTER");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				var data = JSON.parse(json.data)
				assert.equal(data.length, 1);
				assert.equal(data[0]["name"], "Lala");
				assert.equal(data[0]["isHighlight"], false);
				assert.equal(data[0]["user_id"], "1");
				done();
			});
		});
	});

	describe('test params', function() {
		describe('create-filters', function() {
			it("Test when Name is Missing", function (done) {
			    unirest.post(baseAPI+create_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"is_highlight": true})
					.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 2);
					assert.equal(json.response.msg, "Name is empty");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":2,\"msg\":\"Name is empty\"},\"data\":null}")
					done();
				});
			});

			it("Test when Name is Empty", function (done) {
			    unirest.post(baseAPI+create_filterAPI)
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.send({"name": "", "pseudo": "" })
						.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 2);
					assert.equal(json.response.msg, "Name is empty");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":2,\"msg\":\"Name is empty\"},\"data\":null}")
					done();
				});
			});
		});

		describe('update-filters', function() {
			it("Test when filter ID is Missing", function (done) {
			    unirest.post(baseAPI+update_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					// .send({"name": "Lala", "is_highlight": false, "filter_id" : filterId})
					.end(function (response)  
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 1);
					assert.equal(json.response.msg, "Filter is empty");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":1,\"msg\":\"Filter is empty\"},\"data\":null}")
					done();
				});
			});

			it("Test when filter ID is Empty", function (done) {
			    unirest.post(baseAPI+update_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"filter_id" : ""})
					.end(function (response)  
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 1);
					assert.equal(json.response.msg, "Filter is empty");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":1,\"msg\":\"Filter is empty\"},\"data\":null}")
					done();
				});
			});

			it("Test when Name is Missing", function (done) {
			    unirest.post(baseAPI+update_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"is_highlight": false, "filter_id" : filterId})
					.end(function (response)  
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 2);
					assert.equal(json.response.msg, "Name is empty");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":2,\"msg\":\"Name is empty\"},\"data\":null}")
					done();
				});
			});

			it("Test when Name is Empty", function (done) {
			    unirest.post(baseAPI+update_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"name": "", "is_highlight": false, "filter_id" : filterId})
					.end(function (response)  
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 2);
					assert.equal(json.response.msg, "Name is empty");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":2,\"msg\":\"Name is empty\"},\"data\":null}")
					done();
				});
			});

			it("Test when filter ID not exit '666'", function (done) {
			    unirest.post(baseAPI+remove_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"filter_id" : "666"})
					.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 0);
					assert.equal(json.response.msg, "Filter is not found");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":0,\"msg\":\"Filter is not found\"},\"data\":null}")
					done();
				});
			});

			it("Test when filter ID not exit 'hello'", function (done) {
			    unirest.post(baseAPI+remove_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"filter_id" : "hello"})
					.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 0);
					assert.equal(json.response.msg, "Filter is not found");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":0,\"msg\":\"Filter is not found\"},\"data\":null}")
					done();
				});
			});
		});

		describe('remove-filters', function() {
			it("Test when filter ID is Missing", function (done) {
			    unirest.post(baseAPI+remove_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 1);
					assert.equal(json.response.msg, "Filter is empty");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":1,\"msg\":\"Filter is empty\"},\"data\":null}")
					done();
				});
			});

			it("Test when filter ID is Empty", function (done) {
			    unirest.post(baseAPI+remove_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"filter_id" : ""})
					.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 1);
					assert.equal(json.response.msg, "Filter is empty");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":1,\"msg\":\"Filter is empty\"},\"data\":null}")
					done();
				});
			});

			it("Test when filter ID not exit", function (done) {
			    unirest.post(baseAPI+remove_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"filter_id" : "666"})
					.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 0);
					assert.equal(json.response.msg, "Filter is not found");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":0,\"msg\":\"Filter is not found\"},\"data\":null}")
					done();
				});
			});

			it("Test when filter ID not exit '666'", function (done) {
			    unirest.post(baseAPI+remove_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"filter_id" : "666"})
					.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 0);
					assert.equal(json.response.msg, "Filter is not found");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":0,\"msg\":\"Filter is not found\"},\"data\":null}")
					done();
				});
			});

			it("Test when filter ID not exit 'hello'", function (done) {
			    unirest.post(baseAPI+remove_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"filter_id" : "hello"})
					.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 403);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "FILTER");
					assert.equal(json.response.code, 0);
					assert.equal(json.response.msg, "Filter is not found");
					assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":0,\"msg\":\"Filter is not found\"},\"data\":null}")
					done();
				});
			});
		});
	});

	describe('remove-filters', function() {
		it("Test if can remove filter", function (done) {
			
		    unirest.post(baseAPI+remove_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({"filter_id" : filterId})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "FILTER");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")
				done();
			});
		});

		it("Test if list is empty", function (done) {
			
		    unirest.post(baseAPI+get_filterAPI)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "FILTER");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"FILTER\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"[]\"}")
				done();
			});
		});
	});
});