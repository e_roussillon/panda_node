var assert = require("chai").assert;
var unirest = require('unirest');

var genericTest = require('../generic.js');

var authSQL = require('../../models/authenticationSQL.js');
var privateRoomSQL = require('../../models/privateRoomSQL');

var logger = require('../../utils/logger.js');
var dbUtil = require('../../utils/dbUtil.js');
var userSQL = require('../../models/userSQL.js');
var privateMessageSQL = require('../../models/privateMessageSQL');

var apiToTest = "/api/chat/create_conversation"
var apiBase = "https://localhost:8080"
var api = apiBase+apiToTest

var appId = "123456qwerty"
var pseudo = "Dovi2"
var password = "123456"
var deviceId = "123qwe"
var deviceAndroid = "android"
var email = "dovi2@gmail.com"
var birthday = "1985-4-03"

var passwordInvalide = "qwer123456"
var pseudoInvalide = "doviqwe123456" 

describe('\n'+apiToTest, function() {

	var token = "empty"
	var userOk = 1
	var userInvalid = 2
	var userItSelf = 3

	before(function(done) {
		dbUtil.createPool().getConnection(function(err, connection) {
    		connection.query(privateRoomSQL.clearTable(), function(err, results) {
	    		connection.release();
	    		unirest.post(apiBase + "/api/user/register")
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', deviceId)
					.send({ "pseudo": pseudo, "email": email, "birthday": birthday, "platform_device": deviceAndroid})
					.end(function (response) 
				{
					dbUtil.createPool().getConnection(function(err, connection) {
						connection.query(privateMessageSQL.clearTable(), function(err, results) {
							var json = JSON.parse(response.raw_body)
		                    assert.equal(json.response.status, 200);
		                    assert.equal(json.response.protocol, "API");
		                    assert.equal(json.response.method, "GENERIC_USER");
		                    assert.equal(json.response.code, -1);
		                    assert.equal(json.response.msg, "ok");
		                    token = "Bearer " + JSON.parse(json.data).token
	                        connection.release();

	                        done();
	                    })
					})
				});
	    	});
	   	});
	});

	after(function() {
	});

	describe('Test Creation Conversation', function() {

		it("Test if can create conversation with user empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "ROOM");
				assert.equal(json.response.code, 2);
				assert.equal(json.response.msg, "User is empty");
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"ROOM\",\"code\":2,\"msg\":\"User is empty\"},\"data\":null}")

                done()
			});
		});

		it("Test if can create conversation with user missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({ "user_id": ""})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "ROOM");
				assert.equal(json.response.code, 2);
				assert.equal(json.response.msg, "User is empty");
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"ROOM\",\"code\":2,\"msg\":\"User is empty\"},\"data\":null}")

                done()
			});
		});

		it("Test if can create conversation with user invalid", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({ "user_id": userInvalid})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "ROOM");
				assert.equal(json.response.code, 3);
				assert.equal(json.response.msg, "User is invalid");
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"ROOM\",\"code\":3,\"msg\":\"User is invalid\"},\"data\":null}")

                done()
			});
		});

		it("Test if can create conversation with your self", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({ "user_id": userItSelf})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "ROOM");
				assert.equal(json.response.code, 4);
				assert.equal(json.response.msg, "Can not open conversation with your self");
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"ROOM\",\"code\":4,\"msg\":\"Can not open conversation with your self\"},\"data\":null}")

                done()
			});
		});

		it("Test if can create conversation", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({ "user_id": userOk})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "ROOM");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"ROOM\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"room_id\\\":1,\\\"pseudo\\\":\\\"dovi\\\",\\\"user_thumb\\\":\\\"\\\"}\"}")

                done()
			});
		});

		it("Test if can create conversation - same request", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Authorization', token)
					.send({ "user_id": userOk})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "ROOM");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"ROOM\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"room_id\\\":1,\\\"pseudo\\\":\\\"dovi\\\",\\\"user_thumb\\\":\\\"\\\"}\"}")

                done()
			});
		});
	});
	
});