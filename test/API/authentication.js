var assert = require("chai").assert;
var unirest = require('unirest');

var genericTest = require('../generic.js');

var authSQL = require('../../models/authenticationSQL.js');

var logger = require('../../utils/logger.js');
var dbUtil = require('../../utils/dbUtil.js');

var apiToTest = "/api/user/authentication"
var api = "https://localhost:8080"+apiToTest

var appId = "123456qwerty"
var email = "dovi@gmail.com"
var password = "123456"
var deviceId = "123qwe"
var deviceAndroid = "android"
var deviceiOS = "ios"
var deviceOther = "other"

var passwordInvalide = "qwer123456"
var emailInvalide = "dovi@gmailcom" 

describe('\n'+apiToTest, function() {

	before(function(done) {
	    dbUtil.createPool().getConnection(function(err, connection) {
	    	connection.query(authSQL.clearTable(), function(err, results) {
	    		connection.release();
	    		done()
	    	});
	    });
	});

	after(function() {
	});

	describe('App ID', function() {
		it("Test if App ID invalide", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.end(function (response) 
			{
				genericTest.testAPI_ID(response.raw_body)
				done();
			});
		});
	});
	
	describe('Pseudo', function() {
		it("Test if Pseudo is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "AUTHENTICATION");
				assert.equal(json.response.code, 0);
				assert.equal(json.response.msg, "Email is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"AUTHENTICATION\",\"code\":0,\"msg\":\"Email is Empty\"},\"data\":null}")
				done();
			});

		});

		it("Test if Pseudo is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "pseudo": "" })
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "AUTHENTICATION");
				assert.equal(json.response.code, 0);
				assert.equal(json.response.msg, "Email is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"AUTHENTICATION\",\"code\":0,\"msg\":\"Email is Empty\"},\"data\":null}")
				done();
			});
		});
	});

	describe('Password', function() {
		it("Test if Password is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.send({ "email": email })
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "AUTHENTICATION");
				assert.equal(json.response.code, 1);
				assert.equal(json.response.msg, "Password is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"AUTHENTICATION\",\"code\":1,\"msg\":\"Password is Empty\"},\"data\":null}")
				done();
			});
		});

		it("Test if Password is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', "")
					.send({ "email": email})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "AUTHENTICATION");
				assert.equal(json.response.code, 1);
				assert.equal(json.response.msg, "Password is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"AUTHENTICATION\",\"code\":1,\"msg\":\"Password is Empty\"},\"data\":null}")
				done();
			});
		});
	});	

	describe('Platform Device', function() {
		it("Test if Platform Device is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.send({ "email": email})
					.end(function (response) 
			{
				genericTest.testDevicePlatform(response.raw_body)
				done();
			});
		});

		it("Test if Platform Device is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.send({ "email": email, "platform_device": ""})
					.end(function (response) 
			{
				genericTest.testDevicePlatform(response.raw_body)
				done();
			});
		});
	});

	describe('Device ID', function() {
		it("Test if Device ID is Missing", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.send({ "email": email, "platform_device": deviceiOS})
					.end(function (response) 
			{
				genericTest.testDeviceID(response.raw_body)
				done();
			});
		});

		it("Test if Device ID is Empty", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', "")
					.send({ "email": email, "platform_device": deviceiOS})
					.end(function (response) 
			{
				genericTest.testDeviceID(response.raw_body)
				done();
			});
		});
	});

	describe('Authentication User Android or iOS', function() {
		it("Test if Authentification Android", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', deviceId)
					.send({ "email": email, "platform_device": deviceAndroid})
					.end(function (response) 
			{
				genericTest.testUserFound(response.raw_body)
				done();
			});
		});
		it("Test if Authentification iOS", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', deviceId)
					.send({ "email": email, "platform_device": deviceiOS})
					.end(function (response) 
			{
				genericTest.testUserFound(response.raw_body)
				done();
			});
		});
		it("Test if Authentification other", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', deviceId)
					.send({ "email": email, "platform_device": deviceOther})
					.end(function (response) 
			{
				genericTest.testDevicePlatform(response.raw_body)
				done();
			});
		});
	});

	describe('Authentication User Not found', function() {
		it("Test if Pseudo invalide", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', password)
					.header('Device-Id', deviceId)
					.send({ "email": emailInvalide, "platform_device": deviceAndroid})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "AUTHENTICATION");
				assert.equal(json.response.code, 0);
				assert.equal(json.response.msg, "Email is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"AUTHENTICATION\",\"code\":0,\"msg\":\"Email is Empty\"},\"data\":null}")
				done();
			});
		});

		it("Test if Password invalide", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', passwordInvalide)
					.header('Device-Id', deviceId)
					.send({ "email": email, "platform_device": deviceAndroid})
					.end(function (response) 
			{
				genericTest.testUserNotFound(response.raw_body)
				done();
			});
		});

		it("Test if Pseudo and Password invalide", function (done) {
		    unirest.post(api)
					.header('Accept', 'application/json')
					.header('App-Id', appId)
					.header('Password', passwordInvalide)
					.header('Device-Id', deviceId)
					.send({ "email": emailInvalide, "platform_device": deviceAndroid})
					.end(function (response) 
			{
				var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 403);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "AUTHENTICATION");
				assert.equal(json.response.code, 0);
				assert.equal(json.response.msg, "Email is Empty");
				assert.equal(json.data, null);
				assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"AUTHENTICATION\",\"code\":0,\"msg\":\"Email is Empty\"},\"data\":null}")
				done();
			});
		});
	});
});