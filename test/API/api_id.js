var assert = require("chai").assert;
var unirest = require('unirest');

var genericTest = require('../generic.js');

var authSQL = require('../../models/authenticationSQL.js');
var encryptUtil = require('../../utils/encryptUtil.js');

var logger = require('../../utils/logger.js');
var dbUtil = require('../../utils/dbUtil.js');

var api = "https://localhost:8080"

var appId = "123456qwerty1"
var token = "Bearer " + encryptUtil.genereteToken("1", "ios", "device123", "Thu Jul 21 1983 01:15:00 GMT-0300 (BRT)")
var userId = "1"

describe('\nAPI ID - All APIs', function() {

    before(function(done) {
        dbUtil.createPool().getConnection(function(err, connection) {
            connection.query(authSQL.updateAuthenToken(userId, token), function(err, resultsToken) {
                connection.release();
                done();
            });
        });
    });

    after(function() {

    });

    beforeEach(function() {

    });

    describe('API User', function() {
        describe('/api/user/register', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/user/register')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/user/register')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/user/register')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/user/authentication', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/user/authentication')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/user/authentication')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/user/authentication')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/user/get_user', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/user/get_user')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/user/get_user')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/user/get_user')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/user/update_profile', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/user/update_profile')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/user/update_profile')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/user/update_profile')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });
    });

    describe('API Friendship', function() {
        describe('/api/friend/ask-friendship', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/friend/ask-friendship')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/friend/ask-friendship')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/friend/ask-friendship')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/friend/accept-friendship', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/friend/accept-friendship')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/friend/accept-friendship')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/friend/accept-friendship')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/friend/block-friendship', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/friend/block-friendship')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/friend/block-friendship')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/friend/block-friendship')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/friend/get-friendship', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/friend/get-friendship')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/friend/get-friendship')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/friend/get-friendship')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });
    });

    describe('API Filters', function() {
        describe('/api/filter/create-filter', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/filter/create-filter')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/filter/create-filter')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/filter/create-filter')
                    .header('Accept', 'application/json')
                    .header('App-id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/filter/update-filter', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/filter/update-filter')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/filter/update-filter')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/filter/update-filter')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/filter/remove-filter', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/filter/remove-filter')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/filter/remove-filter')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/filter/remove-filter')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });

        describe('/api/filter/get-filters', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/filter/get-filters')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/filter/get-filters')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/filter/get-filters')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });
    });

    describe('API Chat', function() {

        describe('/api/chat/send-message', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/chat/send-message')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/chat/send-message')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/chat/send-message')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });
    });

    describe('API Auth', function() {
        describe('/api/user/renew_auth', function() {
            it("Test is API ID Missing", function(done) {
                unirest.post(api + '/api/user/renew_auth')
                    .header('Accept', 'application/json')
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID Empty", function(done) {
                unirest.post(api + '/api/user/renew_auth')
                    .header('Accept', 'application/json')
                    .header('App-Id', "")
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });

            it("Test is API ID invalide", function(done) {
                unirest.post(api + '/api/user/renew_auth')
                    .header('Accept', 'application/json')
                    .header('App-Id', appId)
                    .header('Authorization', token)
                    .end(function(response) {
                        genericTest.testAPI_ID(response.raw_body)
                        done();
                    });
            });
        });
    });
});