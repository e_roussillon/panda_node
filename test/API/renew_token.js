var assert = require("chai").assert;
var unirest = require('unirest');

var genericTest = require('../generic.js');

var authSQL = require('../../models/authenticationSQL.js');
var encryptUtil  = require('../../utils/encryptUtil.js');

var logger = require('../../utils/logger.js');
var dbUtil = require('../../utils/dbUtil.js');

var api = "https://localhost:8080"

var appId = "123456qwerty"
var token_simple = encryptUtil.genereteToken("1", "ios", "device123", "Thu Jul 21 1983 01:15:00 GMT-0300 (BRT)")
var token = "Bearer " + token_simple
var userId = "1"

describe('\nRenew Token - All APIs', function() {

	before(function(done) {
		dbUtil.createPool().getConnection(function(err, connection) {
	    	connection.query(authSQL.updateAuthenToken(userId, token_simple), function(err, resultsToken) {
	    		connection.release();
	    		done()
	    	});
	   	});
	});

	after(function() {

	});

	beforeEach(function() {
		
	});

	describe('API User', function() {
		describe('/api/user/get_user', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/user/get_user')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					logger.info(response.raw_body)
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});

		describe('/api/user/update_profile', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/user/update_profile')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});
	});	
	
	describe('API Friendship', function() {
		describe('/api/friend/ask_friendship', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/friend/ask_friendship')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});

		describe('/api/friend/accept_friendship', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/friend/accept_friendship')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});

		describe('/api/friend/block_friendship', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/friend/block_friendship')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});

		describe('/api/friend/get_friendship', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/friend/get_friendship')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});
	});

	describe('API Filters', function() {
		describe('/api/filter/create_filter', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/filter/create_filter')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});

		describe('/api/filter/update_filter', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/filter/update_filter')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});

		describe('/api/filter/remove_filter', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/filter/remove_filter')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});

		describe('/api/filter/get_filters', function() {
			it("Test is Token invalide", function (done) {
			    unirest.post(api+'/api/filter/get_filters')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});
	});

	describe('API Chat', function() {

		describe('/api/chat/send_broadcast_message', function() {
			it("Test is to renew token", function (done) {
			    unirest.post(api+'/api/chat/send_broadcast_message')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.end(function (response) 
				{
					genericTest.testTokenInvalide(response.raw_body)
					done();
				});
			});
		});
	});
	
	describe('API Auth', function() {
		describe('/api/user/renew_auth', function() {
			it("Test is can renew token", function (done) {
			    unirest.post(api+'/api/user/renew_auth')
						.header('Accept', 'application/json')
						.header('App-Id', appId)
						.header('Authorization', token)
						.send({ "user_id": userId})
						.end(function (response) 
				{
					var json = JSON.parse(response.raw_body)
					assert.equal(json.response.status, 200);
					assert.equal(json.response.protocol, "API");
					assert.equal(json.response.method, "RENEW_AUTH");
					assert.equal(json.response.code, -1);
					assert.equal(json.response.msg, "ok");
					var data = JSON.parse(json.data)
					assert.notEqual(data.token, token);
					done();
				});
			});
		});
	});
});
