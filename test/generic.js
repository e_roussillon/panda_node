var assert = require("chai").assert;

exports.testAPI_ID = function(body) {
	var json = JSON.parse(body)
	assert.equal(json.response.status, 401);
	assert.equal(json.response.protocol, "API");
	assert.equal(json.response.method, "APP_ID");
	assert.equal(json.response.code, 0);
	assert.equal(json.response.msg, "App ID invalide");
	assert.equal(json.data, null);
	assert.equal(body, "{\"response\":{\"status\":401,\"protocol\":\"API\",\"method\":\"APP_ID\",\"code\":0,\"msg\":\"App ID invalide\"},\"data\":null}")
}

exports.testTokenInvalide = function(body) {
	var json = JSON.parse(body)
	assert.equal(json.response.status, 401);
	assert.equal(json.response.protocol, "API");
	assert.equal(json.response.method, "TOKEN");
	assert.equal(json.response.code, 0);
	assert.equal(json.response.msg, "Token invalide");
	assert.equal(json.data, null);
	assert.equal(body, "{\"response\":{\"status\":401,\"protocol\":\"API\",\"method\":\"TOKEN\",\"code\":0,\"msg\":\"Token invalide\"},\"data\":null}")
}

exports.testDeviceID = function(body) {
	var json = JSON.parse(body)
	assert.equal(json.response.status, 403);
	assert.equal(json.response.protocol, "API");
	assert.equal(json.response.method, "GENERIC_USER");
	assert.equal(json.response.code, 3);
	assert.equal(json.response.msg, "Device id is empty");
	assert.equal(json.data, null);
	assert.equal(body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"GENERIC_USER\",\"code\":3,\"msg\":\"Device id is empty\"},\"data\":null}")
}

exports.testDevicePlatform = function(body) {
	var json = JSON.parse(body)
	assert.equal(json.response.status, 403);
	assert.equal(json.response.protocol, "API");
	assert.equal(json.response.method, "GENERIC_USER");
	assert.equal(json.response.code, 2);
	assert.equal(json.response.msg, "Platform Device is empty");
	assert.equal(json.data, null);
	assert.equal(body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"GENERIC_USER\",\"code\":2,\"msg\":\"Platform Device is empty\"},\"data\":null}")
}

exports.testUserNotFound = function(body) {
	var json = JSON.parse(body)
	assert.equal(json.response.status, 403);
	assert.equal(json.response.protocol, "API");
	assert.equal(json.response.method, "GENERIC_USER");
	assert.equal(json.response.code, 1);
	assert.equal(json.response.msg, "User is not found");
	assert.equal(json.data, null);
	assert.equal(body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"GENERIC_USER\",\"code\":1,\"msg\":\"User is not found\"},\"data\":null}")
}

exports.testUserFound = function(body) {
	var json = JSON.parse(body)
	assert.equal(json.response.status, 200);
	assert.equal(json.response.protocol, "API");
	assert.equal(json.response.method, "GENERIC_USER");
	assert.equal(json.response.code, -1);
	assert.equal(json.response.msg, "ok");
	var data = JSON.parse(json.data)
	assert.typeOf(data.token, 'string');
}
