var server = require("../app.js");
//Removed HTTPS security
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

//Run Test
describe("--->> TESTING API", function() {
	require("./API/create_user.js");
	require("./API/authentication.js");
	require("./API/create_room.js");
	require("./API/api_id.js");
	require("./API/renew_token.js");
	require("./API/filter.js");
});

// describe("--->> TESTING IO", function() {
// 	require("./IO/connection.js");
// 	require("./IO/localization.js");
// 	require("./IO/public_room.js");
// });



