var io = require('socket.io-client');
var assert = require("chai").assert;
var unirest = require('unirest');
var encryptUtil = require('../../utils/encryptUtil');
var enumUtil = require('../../utils/enumResponse');

var roomLocal = require('../../routes/local/chat/room');

var logger = require('../../utils/logger.js');

var socketURL = 'https://localhost:8080';

var options = {
    transports: ['websocket'],
    'force new connection': true,
    'secure': true
};

var dbUtil = require('../../utils/dbUtil.js');

var roomSQL = require('../../models/publicRoomSQL.js');
var localSQL = require('../../models/publicRoomLocalizedSQL.js');
var messagePublicSQL = require('../../models/publicMessageSQL.js');

var baseAPI = "https://localhost:8080"

var appId = "123456qwerty"
var pseudo = "Dovi"
var password = "123456"
var deviceId = "123qwe"
var deviceAndroid = "android"

var CONNECT = 'connect'
var REQUEST = 'request'
var RESULT = 'result'
var AUTHORIZED = 'authorized'
var DISCONNECT = 'disconnect'
var SET_LOCALIZATION = 'set_localization'
var MESSAGE = 'message'

describe("Socket IO - Public Message", function() {

    var token = "empty"
    var filterId = ""

    before(function(done) {
        unirest.post(baseAPI + "/api/user/authentication")
            .header('Accept', 'application/json')
            .header('app_id', appId)
            .header('password', password)
            .header('device_id', deviceId)
            .send({
                "pseudo": pseudo,
                "platform_device": deviceAndroid
            })
            .end(function(response) {

                dbUtil.createPool().getConnection(function(err, connection) {
                    connection.query(messagePrivateSQL.clearTable(), function(err, results) {
                        var json = JSON.parse(response.raw_body)
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "API");
                        assert.equal(json.response.method, "GENERIC_USER");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        token = JSON.parse(json.data).token

                        done();
                    })
                });
            });
    });


    describe("Test Message", function() {
        describe("Test Broatcast Message", function() {
            it('Test send broatcast message to room invalide', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "set_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                    //Send Broadcast message to room -66
                    unirest.post(baseAPI + "/api/chat/send-broadcast-message")
                        .header('Accept', 'application/json')
                        .header('app_id', appId)
                        .header('authorization', token)
                        .send({
                            "room_id": -66,
                            "message": "message to room public",
                            "is_room_general": false
                        })
                        .end(function(response) {
                            var json = JSON.parse(response.raw_body)
                            assert.equal(json.response.status, 403);
                            assert.equal(json.response.protocol, "API");
                            assert.equal(json.response.method, "message");
                            assert.equal(json.response.code, 1);
                            assert.equal(json.response.msg, "Room is not found");
                            assert.equal(json.data, null);
                            assert.equal(response.raw_body, "{\"response\":{\"status\":403,\"protocol\":\"API\",\"method\":\"message\",\"code\":1,\"msg\":\"Room is not found\"},\"data\":null}")

                            client1.disconnect()
                        });
                });

                client1.on(REQUEST, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send broatcast message to public room', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        //Send Broadcast message to room 5
                        unirest.post(baseAPI + "/api/chat/send-broadcast-message")
                            .header('Accept', 'application/json')
                            .header('app_id', appId)
                            .header('authorization', token)
                            .send({
                                "room_id": 5,
                                "message": "message to room public",
                                "is_room_general": true
                            })
                            .end(function(response) {
                                var json = JSON.parse(response.raw_body)
                                assert.equal(json.response.status, 200);
                                assert.equal(json.response.protocol, "API");
                                assert.equal(json.response.method, "message");
                                assert.equal(json.response.code, -1);
                                assert.equal(json.response.msg, "ok");
                                assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"message\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"isBroadcast\\\":true,\\\"roomType\\\":0,\\\"room\\\":\\\"5\\\",\\\"message\\\":\\\"message to room public\\\"}\"}")
                            });

                    } else {
                        //Get Message send by broadcast
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"message\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"isBroadcast\\\":true,\\\"roomType\\\":0,\\\"room\\\":\\\"5\\\",\\\"message\\\":\\\"message to room public\\\"}\"}")

                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send broatcast message to private room', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        //Send Broadcast message to room 5
                        unirest.post(baseAPI + "/api/chat/send-broadcast-message")
                            .header('Accept', 'application/json')
                            .header('app_id', appId)
                            .header('authorization', token)
                            .send({
                                "room_id": 5,
                                "message": "message to room public",
                                "is_room_general": false
                            })
                            .end(function(response) {
                                var json = JSON.parse(response.raw_body)
                                assert.equal(json.response.status, 200);
                                assert.equal(json.response.protocol, "API");
                                assert.equal(json.response.method, "message");
                                assert.equal(json.response.code, -1);
                                assert.equal(json.response.msg, "ok");
                                assert.equal(response.raw_body, "{\"response\":{\"status\":200,\"protocol\":\"API\",\"method\":\"message\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"isBroadcast\\\":true,\\\"roomType\\\":1,\\\"room\\\":\\\"5\\\",\\\"message\\\":\\\"message to room public\\\"}\"}")
                            });

                    } else {
                        //Get Message send by broadcast
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"message\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"isBroadcast\\\":true,\\\"roomType\\\":1,\\\"room\\\":\\\"5\\\",\\\"message\\\":\\\"message to room public\\\"}\"}")

                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });
        });

        describe("Test Public Message", function() {
            it('Test send public message to general room', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        var data = JSON.parse(json.data)

                        var isBroadcast = false
                        var mRoom = data.rooms[7]["_id"] + enumUtil.CHAT().CHAT_GENERAL.string
                        var mMessage = "hello every one"
                        var message = new roomLocal.MessageToRoom(isBroadcast, mRoom, mMessage)

                        var msg = {
                            "message": message,
                            "rooms": data.rooms
                        }

                        client1.emit(MESSAGE, msg)
                    } else {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"message\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"isBroadcast\\\":false,\\\"roomType\\\":0,\\\"room\\\":\\\"14\\\",\\\"message\\\":\\\"hello every one\\\"}\"}")
                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send public message with no info', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        client1.emit(MESSAGE, null)
                    } else {
                        assert.equal(json.response.status, 403);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, 3);
                        assert.equal(json.response.msg, "Message Obj is empty");
                        assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"message\",\"code\":3,\"msg\":\"Message Obj is empty\"},\"data\":null}")
                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send public message with Message Obj invalide', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        var data = JSON.parse(json.data)

                        var message = {"isBroadcast": null, "roomType": null, "room": null, "message": null}
                        var message = null
                        var rooms = null

                        var msg = {
                            "message": message,
                            "rooms": rooms
                        }

                        client1.emit(MESSAGE, msg)
                    } else {
                        assert.equal(json.response.status, 403);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, 3);
                        assert.equal(json.response.msg, "Message Obj is empty");
                        assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"message\",\"code\":3,\"msg\":\"Message Obj is empty\"},\"data\":null}")
                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send public message with Message isBroadcast invalide', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        var data = JSON.parse(json.data)

                        var message = {"isBroadcast": "1230", "roomType": null, "room": null, "message": null}
                        var rooms = null

                        var msg = {
                            "message": message,
                            "rooms": rooms
                        }

                        client1.emit(MESSAGE, msg)
                    } else {
                        assert.equal(json.response.status, 403);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, 4);
                        assert.equal(json.response.msg, "Broadcast is empty");
                        assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"message\",\"code\":4,\"msg\":\"Broadcast is empty\"},\"data\":null}")
                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send public message with Message RoomType invalide', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        var data = JSON.parse(json.data)

                        var message = {"isBroadcast": true, "roomType": null, "room": null, "message": null}
                        var rooms = null

                        var msg = {
                            "message": message,
                            "rooms": rooms
                        }

                        client1.emit(MESSAGE, msg)
                    } else {
                        assert.equal(json.response.status, 403);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, 5);
                        assert.equal(json.response.msg, "Room type is empty");
                        assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"message\",\"code\":5,\"msg\":\"Room type is empty\"},\"data\":null}")
                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send public message with Message Room invalide', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        var data = JSON.parse(json.data)

                        var message = {"isBroadcast": true, "roomType": enumUtil.CHAT().CHAT_GENERAL.value, "room": null, "message": null}
                        var rooms = null

                        var msg = {
                            "message": message,
                            "rooms": rooms
                        }

                        client1.emit(MESSAGE, msg)
                    } else {
                        assert.equal(json.response.status, 403);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, 6);
                        assert.equal(json.response.msg, "Room id is empty");
                        assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"message\",\"code\":6,\"msg\":\"Room id is empty\"},\"data\":null}")
                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send public message with Message Message invalide', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        var data = JSON.parse(json.data)

                        var message = {"isBroadcast": true, "roomType": enumUtil.CHAT().CHAT_GENERAL.value, "room": 1, "message": null}
                        var rooms = null

                        var msg = {
                            "message": message,
                            "rooms": rooms
                        }

                        client1.emit(MESSAGE, msg)
                    } else {
                        assert.equal(json.response.status, 403);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, 7);
                        assert.equal(json.response.msg, "Message is empty");
                        assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"message\",\"code\":7,\"msg\":\"Message is empty\"},\"data\":null}")
                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });

            it('Test send public message with Message Rooms invalide', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        var data = JSON.parse(json.data)

                        var message = {"isBroadcast": true, "roomType": enumUtil.CHAT().CHAT_GENERAL.value, "room": 1, "message": "hello"}
                        var rooms = null

                        var msg = {
                            "message": message,
                            "rooms": rooms
                        }

                        client1.emit(MESSAGE, msg)
                    } else {
                        assert.equal(json.response.status, 403);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "message");
                        assert.equal(json.response.code, 8);
                        assert.equal(json.response.msg, "Rooms list is empty");
                        assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"message\",\"code\":8,\"msg\":\"Rooms list is empty\"},\"data\":null}")
                        client1.disconnect()
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });


            it('Test send public message with Message Rooms obj invalide', function(done) {
                var client1 = io.connect(socketURL, options);

                client1.on(RESULT, function(data) {
                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    if (json.response.method == "set_localization") {
                        assert.equal(json.response.status, 200);
                        assert.equal(json.response.protocol, "IO");
                        assert.equal(json.response.method, "set_localization");
                        assert.equal(json.response.code, -1);
                        assert.equal(json.response.msg, "ok");
                        assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                        var data = JSON.parse(json.data)

                        var message = {"isBroadcast": true, "roomType": enumUtil.CHAT().CHAT_GENERAL.value, "room": 1, "message": "hello"}
                        var rooms = [{"name": "world","type": "world"},{"name": "france_1_country","type": "country"},{"name": "îles-de-france_2_state","type": "state"},{"name": "paris_3_county","type": "county"},{"name": "paris_4_city","type": "city"},{"name": "19_5_neighborhood","type": "neighborhood"},{"name": "rue-paule_9_street","type": "street"},{"name": "123_12_streetnumber","type": "streetNumber"}]

                        var msg = {
                            "message": message,
                            "rooms": rooms
                        }

                        client1.emit(MESSAGE, msg)
                        client1.disconnect()
                    } else {
                        assert.equal(json.response.status, "Should not arrive here");
                    }
                });

                client1.on(REQUEST, function(data) {

                    var jsonString = JSON.stringify(data)
                    var json = JSON.parse(jsonString)

                    assert.equal(json.response.status, 200);
                    assert.equal(json.response.protocol, "IO");
                    assert.equal(json.response.method, "get_localization");
                    assert.equal(json.response.code, -1);
                    assert.equal(json.response.msg, "ok");
                    assert.equal(json.data, null);
                    assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                    var msg = {
                        "long": "48.870138",
                        "lat": "2.348328",
                        "country": "France",
                        "state": "iles de france",
                        "county": "Paris",
                        "city": "Paris",
                        "neighborhood": "19",
                        "street": "rue paule",
                        "number": "123",
                    }

                    client1.emit(SET_LOCALIZATION, msg)
                });

                client1.on(CONNECT, function(data) {
                    var msg = {
                        "app_id": appId,
                        "token": token
                    }
                    client1.emit(AUTHORIZED, msg)
                });

                client1.on(DISCONNECT, function(data) {
                    assert.equal(data, "io client disconnect");
                    done()
                });
            });
        });
    });
});