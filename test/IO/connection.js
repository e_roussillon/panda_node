var io = require('socket.io-client');
var assert = require("chai").assert;
var unirest = require('unirest');
var encryptUtil = require('../../utils/encryptUtil');

var logger = require('../../utils/logger.js');

var socketURL = 'https://localhost:8080';

var options = {
    transports: ['websocket'],
    'force new connection': true,
    'secure': true
};

var baseAPI = "https://localhost:8080"

var appId = "123456qwerty"
var pseudo = "Dovi"
var password = "123456"
var deviceId = "123qwe"
var deviceAndroid = "android"

var CONNECT = 'connect'
var REQUEST = 'request'
var RESULT = 'result'
var AUTHORIZED = 'authorized'
var DISCONNECT = 'disconnect'

var tokenInvalide = encryptUtil.encrypt("1:ios:device123:Thu Jul 21 1983 01:15:00 GMT-0300 (BRT)")

describe("Socket IO - Connection", function() {

	var token = "empty"
	var filterId = ""

	before(function(done) {
		unirest.post(baseAPI+"/api/user/authentication")
						.header('Accept', 'application/json')
						.header('app_id', appId)
						.header('password', password)
						.header('device_id', deviceId)
						.send({ "pseudo": pseudo, "platform_device": deviceAndroid})
						.end(function (response) 
			{
	    		var json = JSON.parse(response.raw_body)
				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "API");
				assert.equal(json.response.method, "GENERIC_USER");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				token = JSON.parse(json.data).token

	    		done();
			});
	});

	describe("Test Connection Invalide", function() {

		it('Test user do not send Obj null', function(done) {
	        var client1 = io.connect(socketURL, options);

	        client1.on(REQUEST, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(RESULT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(CONNECT, function(data) {
	        	client1.emit(AUTHORIZED, null)
	        });

	        client1.on(DISCONNECT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        	done()
	        });
	    });

		it('Test user do not send API ID invalid', function(done) {
	        var client1 = io.connect(socketURL, options);

	        client1.on(REQUEST, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(RESULT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(CONNECT, function(data) {
	        	var msg = {"app_id": "123q;lmd", "token" : ""}
	        	client1.emit(AUTHORIZED, msg)
	        });

	        client1.on(DISCONNECT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        	done()
	        });
	    });

	    it('Test user do not send API ID empty', function(done) {
	        var client1 = io.connect(socketURL, options);

	        client1.on(REQUEST, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(RESULT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(CONNECT, function(data) {
	        	var msg = {"app_id": "", "token" : ""}
	        	client1.emit(AUTHORIZED, msg)
	        });

	        client1.on(DISCONNECT, function(data) {
	        	// this.timeout(10000);
	        	// logger.info(data)
	        	assert.equal(data, "io server disconnect");
	        	done()
	        });
	    });

	    it('Test user do not send token empty', function(done) {
	        var client1 = io.connect(socketURL, options);

	        client1.on(REQUEST, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(RESULT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(CONNECT, function(data) {
	        	var msg = {"app_id": appId, "token" : ""}
	        	client1.emit(AUTHORIZED, msg)
	        });

	        client1.on(DISCONNECT, function(data) {
	        	// this.timeout(10000);
	        	// logger.info(data)
	        	assert.equal(data, "io server disconnect");
	        	done()
	        });
	    });

	    it('Test user do not send token not valide', function(done) {
	        var client1 = io.connect(socketURL, options);

	        client1.on(REQUEST, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(RESULT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(CONNECT, function(data) {
	        	var msg = {"app_id": appId, "token" : "12345"}
	        	client1.emit(AUTHORIZED, msg)
	        });

	        client1.on(DISCONNECT, function(data) {
	        	// this.timeout(10000);
	        	// logger.info(data)
	        	assert.equal(data, "io server disconnect");
	        	done()
	        });
	    });

	    it('Test user do not send token outdate', function(done) {
	        var client1 = io.connect(socketURL, options);

	        client1.on(REQUEST, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(RESULT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        });

	        client1.on(CONNECT, function(data) {
	        	var msg = {"app_id": appId, "token" : tokenInvalide}
	        	client1.emit(AUTHORIZED, msg)
	        });

	        client1.on(DISCONNECT, function(data) {
	        	assert.equal(data, "io server disconnect");
	        	done()
	        });
	    });

	    describe("Auto disconect user after 10s if he DOES NOT send a token - (run with \'mocha -t 11000\')", function() {
		    it('Test user have been Auto Disconect', function(done) {
		        var client1 = io.connect(socketURL, options);

		        client1.on(RESULT, function(data) {
		        	assert.equal(data, null);
		        });

		        client1.on(REQUEST, function(data) {
		        	assert.equal(data, null);
		        });

		        client1.on(DISCONNECT, function(data) {
		        	// logger.info(data)
		        	assert.equal(data, "io server disconnect");
		        	done()
		        });
		    });
		});
    });

	describe("Test Connection Valide", function() {
	    it('Test user valide - IO need to ask \'get_localization\'', function(done) {
	        var client1 = io.connect(socketURL, options);

	        client1.on(RESULT, function(data) {
	        	assert.equal(data, null);
	        });

	        client1.on(REQUEST, function(data) {
	        	var jsonString = JSON.stringify(data)
	        	var json = JSON.parse(jsonString)

				assert.equal(json.response.status, 200);
				assert.equal(json.response.protocol, "IO");
				assert.equal(json.response.method, "get_localization");
				assert.equal(json.response.code, -1);
				assert.equal(json.response.msg, "ok");
				assert.equal(json.data, null);
				assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")
	        	client1.disconnect()
	        	
	        });

	        client1.on(CONNECT, function(data) {
	        	var msg = {"app_id": appId, "token" : token}
	        	client1.emit(AUTHORIZED, msg)
	        });

	        client1.on(DISCONNECT, function(data) {
	        	assert.equal(data, "io client disconnect");
	        	done()
	        });
	    });
	});

});