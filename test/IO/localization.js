var io = require('socket.io-client');
var assert = require("chai").assert;
var unirest = require('unirest');
var encryptUtil = require('../../utils/encryptUtil');

var logger = require('../../utils/logger.js');

var socketURL = 'https://localhost:8080';

var options = {
    transports: ['websocket'],
    'force new connection': true,
    'secure': true
};

var dbUtil = require('../../utils/dbUtil.js');

var roomSQL = require('../../models/publicRoomSQL.js');
var localSQL = require('../../models/publicRoomLocalizedSQL.js');

var baseAPI = "https://localhost:8080"

var appId = "123456qwerty"
var pseudo = "Dovi"
var password = "123456"
var deviceId = "123qwe"
var deviceAndroid = "android"

var CONNECT = 'connect'
var REQUEST = 'request'
var RESULT = 'result'
var AUTHORIZED = 'authorized'
var DISCONNECT = 'disconnect'
var SET_LOCALIZATION = 'set_localization'

describe("Socket IO - Localization", function() {

    var token = "empty"
    var filterId = ""

    before(function(done) {
        unirest.post(baseAPI + "/api/user/authentication")
            .header('Accept', 'application/json')
            .header('app_id', appId)
            .header('password', password)
            .header('device_id', deviceId)
            .send({
                "pseudo": pseudo,
                "platform_device": deviceAndroid
            })
            .end(function(response) {

            dbUtil.createPool().getConnection(function(err, connection) {
                connection.query(roomSQL.clearTable(), function(err, results) {
                    connection.query(roomSQL.initTable(), function(err, results) {
                        connection.query(localSQL.clearTable(), function(err, results) {
                            connection.release();

                            var json = JSON.parse(response.raw_body)
                            assert.equal(json.response.status, 200);
                            assert.equal(json.response.protocol, "API");
                            assert.equal(json.response.method, "GENERIC_USER");
                            assert.equal(json.response.code, -1);
                            assert.equal(json.response.msg, "ok");
                            token = JSON.parse(json.data).token

                            done();
                        });
                    });
                });
            });
        });
    });

    describe("Test Localization Invalide", function() {
        it('Test send localization empty', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 403);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, 0);
                assert.equal(json.response.msg, "Localization is invalid");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":0,\"msg\":\"Localization is invalid\"},\"data\":null}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                client1.emit(SET_LOCALIZATION, "")
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization imposible', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 403);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, 0);
                assert.equal(json.response.msg, "Localization is invalid");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":0,\"msg\":\"Localization is invalid\"},\"data\":null}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "qwer",
                    "lat": "qwe",
                    "country": "",
                    "state": "",
                    "county": "",
                    "city": "",
                    "neighborhood": "",
                    "street": "",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });
    });

    describe("Test Localization one by one param", function() {
        it('Test send localization with no country', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 403);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, 1);
                assert.equal(json.response.msg, "Country is invalid");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":1,\"msg\":\"Country is invalid\"},\"data\":null}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870135",
                    "lat": "2.348328",
                    "country": "",
                    "state": "",
                    "county": "",
                    "city": "",
                    "neighborhood": "",
                    "street": "",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization with no state', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 403);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, 2);
                assert.equal(json.response.msg, "State is invalid");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":2,\"msg\":\"State is invalid\"},\"data\":null}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870135",
                    "lat": "2.348328",
                    "country": "Paris",
                    "state": "",
                    "county": "",
                    "city": "",
                    "neighborhood": "",
                    "street": "",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization with no county', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 403);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, 3);
                assert.equal(json.response.msg, "County is invalid");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":3,\"msg\":\"County is invalid\"},\"data\":null}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870135",
                    "lat": "2.348328",
                    "country": "Paris",
                    "state": "Paris",
                    "county": "",
                    "city": "",
                    "neighborhood": "",
                    "street": "",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization with no city', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 403);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, 4);
                assert.equal(json.response.msg, "City is invalid");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":403,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":4,\"msg\":\"City is invalid\"},\"data\":null}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870135",
                    "lat": "2.348328",
                    "country": "Paris",
                    "state": "Paris",
                    "county": "Iles de france",
                    "city": "",
                    "neighborhood": "",
                    "street": "",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization with no neighborhood', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":6,\\\"name\\\":\\\"none_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":7,\\\"name\\\":\\\"none_6_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":8,\\\"name\\\":\\\"none_7_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870135",
                    "lat": "2.348328",
                    "country": "France",
                    "state": "Îles de france",
                    "county": "Paris",
                    "city": "Paris",
                    "neighborhood": "",
                    "street": "",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization with no neighborhood need to get the same result as before', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":6,\\\"name\\\":\\\"none_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":7,\\\"name\\\":\\\"none_6_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":8,\\\"name\\\":\\\"none_7_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870135",
                    "lat": "2.348328",
                    "country": "France",
                    "state": "Îles de france",
                    "county": "Paris",
                    "city": "Paris",
                    "neighborhood": "",
                    "street": "",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization with no street', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                // logger.info(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":10,\\\"name\\\":\\\"none_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":11,\\\"name\\\":\\\"none_10_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870136",
                    "lat": "2.348328",
                    "country": "France",
                    "state": "iles de france",
                    "county": "Paris",
                    "city": "Paris",
                    "neighborhood": "19",
                    "street": "",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization with no street number', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                // logger.info(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":13,\\\"name\\\":\\\"none_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870137",
                    "lat": "2.348328",
                    "country": "France",
                    "state": "iles de france",
                    "county": "Paris",
                    "city": "Paris",
                    "neighborhood": "19",
                    "street": "rue paule",
                    "number": "",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });
    });

    describe("Test Localization Full but missing one param", function() {
        it('Test send localization full', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                // logger.info(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":12,\\\"name\\\":\\\"rue-paule_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":14,\\\"name\\\":\\\"123_12_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870138",
                    "lat": "2.348328",
                    "country": "France",
                    "state": "iles de france",
                    "county": "Paris",
                    "city": "Paris",
                    "neighborhood": "19",
                    "street": "rue paule",
                    "number": "123",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        it('Test send localization full with no neighborhood', function(done) {
            var client1 = io.connect(socketURL, options);

            client1.on(RESULT, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                // logger.info(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "set_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":6,\\\"name\\\":\\\"none_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":15,\\\"name\\\":\\\"rue-paule_6_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":16,\\\"name\\\":\\\"123_15_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

                client1.disconnect()
            });

            client1.on(REQUEST, function(data) {
                var jsonString = JSON.stringify(data)
                var json = JSON.parse(jsonString)

                assert.equal(json.response.status, 200);
                assert.equal(json.response.protocol, "IO");
                assert.equal(json.response.method, "get_localization");
                assert.equal(json.response.code, -1);
                assert.equal(json.response.msg, "ok");
                assert.equal(json.data, null);
                assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

                var msg = {
                    "long": "48.870138",
                    "lat": "2.348321",
                    "country": "France",
                    "state": "iles de france",
                    "county": "Paris",
                    "city": "Paris",
                    "neighborhood": "",
                    "street": "rue paule",
                    "number": "123",
                }

                client1.emit(SET_LOCALIZATION, msg)
            });

            client1.on(CONNECT, function(data) {
                var msg = {
                    "app_id": appId,
                    "token": token
                }
                client1.emit(AUTHORIZED, msg)
            });

            client1.on(DISCONNECT, function(data) {
                assert.equal(data, "io client disconnect");
                done()
            });
        });

        // it('Test send localization full with no street', function(done) {
        //     var client1 = io.connect(socketURL, options);

        //     client1.on(RESULT, function(data) {
        //         var jsonString = JSON.stringify(data)
        //         var json = JSON.parse(jsonString)

        //         // logger.info(jsonString)

        //         assert.equal(json.response.status, 200);
        //         assert.equal(json.response.protocol, "IO");
        //         assert.equal(json.response.method, "set_localization");
        //         assert.equal(json.response.code, -1);
        //         assert.equal(json.response.msg, "ok");
        //         assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":10,\\\"name\\\":\\\"none_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":17,\\\"name\\\":\\\"123_10_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

        //         client1.disconnect()
        //     });

        //     client1.on(REQUEST, function(data) {
        //         var jsonString = JSON.stringify(data)
        //         var json = JSON.parse(jsonString)

        //         assert.equal(json.response.status, 200);
        //         assert.equal(json.response.protocol, "IO");
        //         assert.equal(json.response.method, "get_localization");
        //         assert.equal(json.response.code, -1);
        //         assert.equal(json.response.msg, "ok");
        //         assert.equal(json.data, null);
        //         assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

        //         var msg = {
        //             "long": "48.870148",
        //             "lat": "2.348322",
        //             "country": "France",
        //             "state": "iles de france",
        //             "county": "Paris",
        //             "city": "Paris",
        //             "neighborhood": "19",
        //             "street": "",
        //             "number": "123",
        //         }

        //         client1.emit(SET_LOCALIZATION, msg)
        //     });

        //     client1.on(CONNECT, function(data) {
        //         var msg = {
        //             "app_id": appId,
        //             "token": token
        //         }
        //         client1.emit(AUTHORIZED, msg)
        //     });

        //     client1.on(DISCONNECT, function(data) {
        //         assert.equal(data, "io client disconnect");
        //         done()
        //     });
        // });

        // it('Test send localization which allready exist', function(done) {
        //     var client1 = io.connect(socketURL, options);

        //     client1.on(RESULT, function(data) {
        //         var jsonString = JSON.stringify(data)
        //         var json = JSON.parse(jsonString)

        //         assert.equal(json.response.status, 200);
        //         assert.equal(json.response.protocol, "IO");
        //         assert.equal(json.response.method, "set_localization");
        //         assert.equal(json.response.code, -1);
        //         assert.equal(json.response.msg, "ok");
        //         assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"set_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":\"{\\\"current_room\\\":{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},\\\"rooms\\\":[{\\\"_id\\\":1,\\\"name\\\":\\\"world\\\",\\\"type\\\":\\\"world\\\"},{\\\"_id\\\":2,\\\"name\\\":\\\"france_1_country\\\",\\\"type\\\":\\\"country\\\"},{\\\"_id\\\":3,\\\"name\\\":\\\"îles-de-france_2_state\\\",\\\"type\\\":\\\"state\\\"},{\\\"_id\\\":4,\\\"name\\\":\\\"paris_3_county\\\",\\\"type\\\":\\\"county\\\"},{\\\"_id\\\":5,\\\"name\\\":\\\"paris_4_city\\\",\\\"type\\\":\\\"city\\\"},{\\\"_id\\\":9,\\\"name\\\":\\\"19_5_neighborhood\\\",\\\"type\\\":\\\"neighborhood\\\"},{\\\"_id\\\":10,\\\"name\\\":\\\"none_9_street\\\",\\\"type\\\":\\\"street\\\"},{\\\"_id\\\":17,\\\"name\\\":\\\"123_10_streetnumber\\\",\\\"type\\\":\\\"streetNumber\\\"}]}\"}")

        //         client1.disconnect()
        //     });

        //     client1.on(REQUEST, function(data) {
        //         var jsonString = JSON.stringify(data)
        //         var json = JSON.parse(jsonString)

        //         assert.equal(json.response.status, 200);
        //         assert.equal(json.response.protocol, "IO");
        //         assert.equal(json.response.method, "get_localization");
        //         assert.equal(json.response.code, -1);
        //         assert.equal(json.response.msg, "ok");
        //         assert.equal(json.data, null);
        //         assert.equal(jsonString, "{\"response\":{\"status\":200,\"protocol\":\"IO\",\"method\":\"get_localization\",\"code\":-1,\"msg\":\"ok\"},\"data\":null}")

        //         var msg = {
        //             "long": "48.870138",
        //             "lat": "2.348322",
        //             "country": "France",
        //             "state": "iles de france",
        //             "county": "Paris",
        //             "city": "Paris",
        //             "neighborhood": "19",
        //             "street": "",
        //             "number": "123",
        //         }

        //         client1.emit(SET_LOCALIZATION, msg)
        //     });

        //     client1.on(CONNECT, function(data) {
        //         var msg = {
        //             "app_id": appId,
        //             "token": token
        //         }
        //         client1.emit(AUTHORIZED, msg)
        //     });

        //     client1.on(DISCONNECT, function(data) {
        //         assert.equal(data, "io client disconnect");
        //         done()
        //     });
        // });
    });
});
