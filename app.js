// BASE SETUP
// =============================================================================
// HTTPS -> http://blog.mgechev.com/2014/02/19/create-https-tls-ssl-application-with-express-nodejs/
// http://stackoverflow.com/questions/22584268/node-js-https-pem-error-routinespem-read-biono-start-line
// http://chrislarson.me/blog/ssl-nodejs-express-and-socketio.html
// http://www.akadia.com/services/ssh_test_certificate.html

// var express = require('express')
//   , app = express()
//   , fs = require('fs')

//   , options = {
//     key: fs.readFileSync('./cert/key.pem', 'utf8'),
//     cert: fs.readFileSync('./cert/server.crt', 'utf8'),
//     requestCert: true
//   }
//   , server = require('https').createServer(options, app)
//   , io = require('socket.io').listen(server);

// // httpapp.get('*',function(req,res){
// //     res.redirect('https://127.0.0.1:8080'+req.url)
// // })

// server.listen(8080);
// // http.listen(8081);

// app.get('/', function (req, res) {
//       res.header('Content-type', 'text/html');
//       return res.end('<h1>Hello, Secure World!</h1>');
//     });

// var socket = io.connect('https://127.0.0.1:8080', {secure: true});

// REQUIRE
// =============================================================================
var fs = require('fs');
var https = require('https');
var express = require('express');
var app = express()
var bodyParser = require('body-parser');
var ioHandler = require('./socket/ioHandler.js');
var messageHanlder = require('./socket/message.js');
var logger = require("./utils/logger.js");
var connectionUtil = require("./utils/connectionUtil.js");
var enumUtil = require('./utils/enumResponse.js');
var routescan = require('express-routescan');

var options = {
    key: fs.readFileSync('./cert/localhost.key').toString(),
    cert: fs.readFileSync('./cert/localhost.crt').toString(),
    ciphers: 'ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES256-SHA384',
    honorCipherOrder: true,
    secureProtocol: 'TLSv1_2_method'
};

// var options = {
//     key: fs.readFileSync('./cert/key.pem', 'utf8'),
//     cert: fs.readFileSync('./cert/server.crt', 'utf8'),
//     requestCert: false,
//     rejectUnauthorized: false
//   }

var server = require('https').createServer(options, app)
// var serverIO = require('https').createServer(options, app)

// PORT
// =============================================================================
var portAPI = normalizePort(process.env.PORT || 8080);
var portSocket = normalizePort(3000);

// server.listen(portSocket, function(){
//   console.log('listening Socket on :', portSocket);
// });

server.listen(portAPI, function(){
  console.log('listening API on :', portAPI);
});

// var io = require('socket.io').listen(server);

// MONGO DB
// =============================================================================
var mysql = require('./utils/dbUtil.js');

// Make our db accessible to our router
// =============================================================================
app.use(function(req,res,next){
    logger.debug("");
    logger.debug("REQUEST API *** %s%s", req.headers["host"], req.url)

    connectionUtil.checkAppIdFromAPI(req, res, function(err, response) {
      if (response == true) {
        req.db = mysql.createPool();
        next();
      }
    });
});

// configure app to use bodyParser()
// this will let us get the data from a POST
// PARAM AS JSON
// =============================================================================
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// ERROR HANDLER
// =============================================================================
app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);

function logErrors(err, req, res, next) {
	console.log('Method logErrors');
  console.error(err.stack);
  next(err);
}

function clientErrorHandler(err, req, res, next) {
  if (req.xhr) {
  	console.log('Method clientErrorHandler');
    res.status(500).send({ error: 'Something blew up!' });
  } else {
    next(err);
  }
}

function errorHandler(err, req, res, next) {
	console.log('Method errorHandler');
  res.status(500);
  res.render('error', { error: err });
}

// INIT HANDLER SOCKET IO
// =============================================================================

// io.on('connection', function(socket){
// 	ioHandler.handler(io, socket)
// });

// START THE SERVER
// =============================================================================
// https.listen(portSocket, function(){
//   console.log('listening Socket on :', portSocket);
// });

// app.listen(portAPI, function(){
//   console.log('listening API on :', portAPI);
//   console.log("")
//   console.log("")
// });

// ROUTES CAN WILL MANAGE TO SET THE RIGHT ROUTE
// =============================================================================
// routescan(app);
routescan(app, {
    directory: [
        './routes/api/'
    ],
    ext: ['.js'], // is for enable scanning for all *.rt and *.js files
    ignoreInvalid: true, // is for ignoring invalid routes
    verbose: false,
    strictMode: true // is for filtering `all`-method and `use`-method routes.
});

// EXPORTS
// =============================================================================
exports.sendMessageToRoom = function(req, res, token, room, message) {
	messageHanlder.sendMessageToRoomFromAPI(req, res, io, token, room, enumUtil.IO().MESSAGE, message)
};

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
