var logger = require('../utils/logger.js');
var responder  = require('../utils/responder.js');
var enumUtil = require('../utils/enumResponse.js');

var room = require('../routes/local/chat/room.js');

var authSQL = require('../models/authenticationSQL.js');

exports.roomSubscription = function(socket, room) {
	roomSubscriptionLocal(socket, room)
}

exports.roomUnsubscribe = function(socket, room) {
	roomUnsubscribeLocal(socket, room)
}

exports.newLocalization = function(io, socket, data) {

	var longField = data["long"]
    var latField = data["lat"]
    var countryField = data["country"]
    var stateField = data["state"]
    var countyField = data["county"]
    var cityField = data["city"]
    var neighborhoodField = data["neighborhood"]
    var streetField = data["street"]
    var streetNumberField = data["number"]

    if (longField == null || longField.length == 0 || isNaN(longField) || latField == null || latField.length == 0 || isNaN(latField)) {
        responder.resultIO403(io, socket, enumUtil.IO().SET_LOCALIZATION, enumUtil.SET_LOCALIZATION().ERROR_LNG_AND_LAT)
    } else if (countryField == null || countryField.length == 0) {
    	responder.resultIO403(io, socket, enumUtil.IO().SET_LOCALIZATION, enumUtil.SET_LOCALIZATION().ERROR_COUNTRY)
    } else if (stateField == null || stateField.length == 0) {
    	responder.resultIO403(io, socket, enumUtil.IO().SET_LOCALIZATION, enumUtil.SET_LOCALIZATION().ERROR_STATE)
    } else if (countyField == null || countyField.length == 0) {
        responder.resultIO403(io, socket, enumUtil.IO().SET_LOCALIZATION, enumUtil.SET_LOCALIZATION().ERROR_COUNTY)
    } else if (cityField == null || cityField.length == 0) {
    	responder.resultIO403(io, socket, enumUtil.IO().SET_LOCALIZATION, enumUtil.SET_LOCALIZATION().ERROR_CITY)
    } else {

        var localisation = room.Localization(longField, latField, countryField, stateField, countyField, cityField, neighborhoodField, streetField, streetNumberField)

        room.setupRoomsPath(localisation, function(err, results) {
        	
            if (err != null) {
                responder.resultIO406(io, socket, enumUtil.IO().SET_LOCALIZATION, err)
            } else {

                logger.debug(results)
                
                if (results.length == 8) {
                    var item = results[4]

                    if (item["type"] == "city") {
                        result = {"current_room" : item, "rooms" : results}
                        roomSubscriptionLocal(socket, results, item, function(err, results) {
                            if (err != null) {
                                responder.resultIO406(io, socket, enumUtil.IO().SET_LOCALIZATION, err)
                            } else {
                                responder.resultIO(io, socket, enumUtil.IO().SET_LOCALIZATION, result)
                            }
                        })
                    } else {
                        responder.resultIO403(io, socket, enumUtil.IO().SET_LOCALIZATION, enumUtil.SET_LOCALIZATION().ERROR_INIT_ROOM)
                    }
                } else {
                    responder.resultIO403(io, socket, enumUtil.IO().SET_LOCALIZATION, enumUtil.SET_LOCALIZATION().ERROR_NUMBER_OF_ROOMS)
                }
            }
        });
    }
}

//
// GENERIC
//

function roomSubscriptionLocal (socketField, roomsBroadcastField, roomField, callback) {
    room.addRoomToUser(socketField, roomsBroadcastField, roomField, callback);
}

function roomUnsubscribeLocal (socketField, roomField, callback) {
    room.removeRoomFromUser(socketField, roomField, callback);
}
