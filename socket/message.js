var logger = require('../utils/logger.js');
var responder = require('../utils/responder.js');
var dbUtil = require('../utils/dbUtil.js');
var enumUtil = require('../utils/enumResponse.js');
var connectionUtil = require('../utils/connectionUtil');
var encryptUtil = require('../utils/encryptUtil');

var authSQL = require('../models/authenticationSQL.js');
var roomSQL = require('../models/publicRoomSQL.js');
var publicMessageSQL = require('../models/publicMessageSQL.js');

var roomLocal = require('../routes/local/chat/room.js');

exports.sendMessage = function(io, socket, msg) {
    var jsonString = JSON.stringify(msg)
    var json = JSON.parse(jsonString)

    if (json == null || json.message == null) {
        responder.resultIO403(io, socket, enumUtil.IO().MESSAGE, enumUtil.MESSAGE().ERROR_MESSAGE_OBJ_EMPTY)
    } else if (json.message.isBroadcast == null || json.message.isBroadcast.length == 0 || !(typeof(json.message.isBroadcast) === "boolean")) {
        responder.resultIO403(io, socket, enumUtil.IO().MESSAGE, enumUtil.MESSAGE().ERROR_BROADCAST_EMPTY)
    } else if (roomLocal.RoomTypeIntIsValide(json.message.roomType) == null) {
        responder.resultIO403(io, socket, enumUtil.IO().MESSAGE, enumUtil.MESSAGE().ERROR_ROOM_TYPE_EMPTY)
    } else if (json.message.room == null || json.message.room.length == 0) {
        responder.resultIO403(io, socket, enumUtil.IO().MESSAGE, enumUtil.MESSAGE().ERROR_ROOM_ID_EMPTY)
    } else if (json.message.message == null || json.message.message.length == 0) {
        responder.resultIO403(io, socket, enumUtil.IO().MESSAGE, enumUtil.MESSAGE().ERROR_MESSAGE_EMPTY)
    } else if (json.rooms == null || json.rooms.length != 8) {
        responder.resultIO403(io, socket, enumUtil.IO().MESSAGE, enumUtil.MESSAGE().ERROR_ROOMS_EMPTY)
    } else {
        var paramRoom = json.message.roomType == enumUtil.CHAT().CHAT_GENERAL.value ? enumUtil.CHAT().CHAT_GENERAL.string : enumUtil.CHAT().CHAT_TO_SELL.string

        json.rooms.forEach(function(room){
            if (room["_id"] != null && room["_id"] > 0) {
                var mRoom = room["_id"]+paramRoom
                responder.resultIOToRoom(io, mRoom, enumUtil.IO().MESSAGE, json.message)
            }
        });

    }

    // io.emit(conf.onMessage(), msg);
    // responder.resultIO(io, socket, enumUtil.IO().SET_LOCALIZATION, result)
}

exports.sendMessageToRoomFromAPI = function(req, res, io, token, room, enumResult, msg) {
    connectionUtil.checkToken(req, res, function(err, response) {
        if (response == true) {
            req.db.getConnection(function(err, connection) {
                connection.query(roomSQL.selectRoomById(room), function(err, results) {
                    dbUtil.responseSQL("selectRoomById", err, results)

                    if (err) {
                        responder.result406(req, res, enumUtil.API().MESSAGE, err)
                        connection.release();
                    } else {
                        if (results.length == 0) {
                            responder.result403(req, res, enumUtil.API().MESSAGE, enumUtil.MESSAGE().ERROR_ROOM_NOT_FOUND)
                            connection.release();
                        } else {
                            var paramRoom = msg.isRoomGeneral == true ? enumUtil.CHAT().CHAT_GENERAL.string : enumUtil.CHAT().CHAT_TO_SELL.string
                            var mRoom = room + paramRoom
                            var mArrayToken = encryptUtil.decrypt(token).split(":");

                            var paramRoomBroadcast = enumUtil.CHAT().CHAT_BROADCAST.string

                            connection.query(publicMessageSQL.insertMessagePublic(room, mArrayToken[0], msg.message, msg.isBroadcast), function(err, results) {
                                dbUtil.responseSQL("insertMessagePublic", err, results)

                                if (err) {
                                    responder.result406(req, res, enumUtil.API().MESSAGE, err)
                                } else {
                                    var data = new roomLocal.MessageToRoom(msg.isBroadcast, mRoom, msg.message)

                                    //If broadcast we will send the message only for the roomId_CB
                                    //Send result to IO
                                    responder.resultIOToRoom(io, room + paramRoomBroadcast, enumResult, data)

                                    //Send result to API 
                                    responder.result200(req, res, enumUtil.API().MESSAGE, data)
                                }
                                connection.release();
                            });
                        }
                    }
                })
            })
        }
    });
}