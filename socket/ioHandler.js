var logger = require('../utils/logger.js');
var enumUtil = require('../utils/enumResponse.js');

var authenticate = require('./authenticate.js');
var subscription = require('./subscription.js');
var message = require('./message.js');


exports.handler = function(io, socket) {
	//Need to login first
	socket.auth = false;
	
	authenticate.removeSockert(io, socket)
	authenticate.disconectByTimeout(socket)

	socket.on(enumUtil.IO().AUTHORIZED.string, function(data){
		logger.debug("");
		logger.debug("SOCKET *** Authenticated - %s", JSON.stringify(data));
		authenticate.authenticate(io, socket, data)
	});

	socket.on(enumUtil.IO().DISCONNECT.string, function(){
		logger.debug("");
		logger.debug("SOCKET *** Disconnect - %s", socket.id);
        authenticate.disconectSockert(io, socket)
    });

	socket.on(enumUtil.IO().SET_LOCALIZATION.string.toLowerCase(), function(data){
		logger.debug("");
		logger.debug("SOCKET *** SetLocalization - %s", JSON.stringify(data));
		subscription.newLocalization(io, socket, data)	
	});

	socket.on(enumUtil.IO().SUBSCRIBE.string, function(data) { 
		logger.debug("");
		logger.debug("SOCKET *** Subscribe - %s", JSON.stringify(data));
        subscription.roomSubscription(socket, data)
    })

    socket.on(enumUtil.IO().UNSUBSCRIBE.string, function(data) {
    	logger.debug("");  
    	logger.debug("SOCKET *** Unsubscribe - %s", JSON.stringify(data));
        subscription.roomUnsubscribe(socket, data)
    })

	socket.on(enumUtil.IO().MESSAGE.string, function(data){
		logger.debug("");
		logger.debug("SOCKET *** Message - %s", JSON.stringify(data));
		message.sendMessage(io, socket, data)	
	});	
}