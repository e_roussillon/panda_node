var _ = require('underscore');

var dbUtil = require('../utils/dbUtil.js');
var logger = require('../utils/logger.js');
var conUtil = require('../utils/connectionUtil.js');
var responder  = require('../utils/responder.js');
var enumUtil = require('../utils/enumResponse.js');

var authSQL = require('../models/authenticationSQL.js');

var room = require('../routes/local/chat/room.js');

exports.authenticate = function(io, socket, data) {
	//check the auth data sent by the client

	if (data == null) {
		socket.disconnect();
	} else {
		conUtil.checkAppIdFromSocketIO(io, socket, data.app_id, function(err, success){
			if (success){
				conUtil.checkTokenSocketIO(io, socket, data.token, function(err, success){
					if (success){
						logger.debug("------> NB of User %s", io.sockets.sockets.length);
						logger.debug("------> Authenticated socket %s", socket.id);
						socket.auth = true;

						_.each(io.nsps, function(nsp) {
							if(_.findWhere(nsp.sockets, {id: socket.id})) {
								logger.debug("------> Restoring socket to %s", socket.id);
								nsp.connected[socket.id] = socket;
								responder.requestIO(io, socket, enumUtil.IO().GET_LOCALIZATION, null)
							}
						});
					}
				});
			} else {
				socket.disconnect();
			}
		});
	}

	
}

// exports.removeSockert = function(io, socket) {
// 	_.each(io.nsps, function(nsp){
// 		var list = _.where(nsp.sockets, {id: socket.id})
// 		logger.debug("SOCKET ---> removed id %s", JSON.stringify(socket));
// 		// if (list.length > 0) {
// 		// 	var socket = list[0]
// 		// 	if (!socket.auth) {
// 		// 		logger.debug("SOCKET ---> removed id %s", socket.id);
// 		// 		delete nsp.connected[socket.id];
// 		// 	}
// 		// }
// 	});
// }

exports.removeSockert = function(io, socket) {
	_.each(io.nsps, function(nsp){
		if (_.findWhere(nsp.sockets, {id: socket.id})) {
			if (!socket.auth) {
				delete nsp.connected[socket.id];
			}
		}
	});
}

exports.disconectSockert = function(io, socket) {
    dbUtil.createPool().getConnection(function(err, connection) {
    	connection.query(authSQL.findAuthenBySocket(socket.id), function(err, results) {
    		dbUtil.responseSQL("findAuthenBySocket", err, results)

            if (err) {
            	connection.release();
            } else {
                if (results.length == 0) {
                    connection.release();
                } else {
                	room.removeRoomFromUser(socket, results[0]["room"], function(err, results) {

                		if (err) {
                			connection.release();
               				logger.debug("------> Error disconectSockert : %s", err);
                		} else {
                            connection.query(authSQL.removedAuthenSocket(socket.id, ""), function(err, results) {
                            	dbUtil.responseSQL("removedAuthenSocket", err, results)
                            	
                                connection.release();
                                if (err) {
                                    logger.debug("------> Error disconectSockert : %s", err);
                                } else {
                                    logger.debug("------> NB of User %s", io.sockets.sockets.length);
                                    logger.debug("------> Disconnect socket %s", socket.id);
                                }
                            });
                		} 
                	});
                }
            }
        });
    });
}

exports.disconectByTimeout = function(socket) {
	setTimeout(function(){
		//If the socket didn't authenticate, disconnect it
		if (!socket.auth) {
            logger.debug("")
			logger.debug("SOCKET *** TIMEOUT")
			socket.disconnect();
		}

	}, enumUtil.timeoutAuth());
}