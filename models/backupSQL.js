var dbUtil = require('../utils/dbUtil.js');

exports.backupCurrentUsers = function(userIdField) {
	var sql = "SELECT user._id, user.email, user.pseudo, user.description, user.birthday, user.gender, user.data_id, user.timestamp FROM user WHERE _id = ?";
	var inserts = [userIdField];
	return dbUtil.format("backupCurrentUsers", sql, inserts);
}

exports.backupUsers = function(userIdField, dateField) {
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

	var sql = "SELECT user._id, user.email, user.pseudo, user.description, user.birthday, user.gender, user.data_id, user.timestamp FROM private_room_user, (SELECT user._id, user.email, user.pseudo, user.status, user.description, user.birthday, user.gender, user.data_id, user.timestamp, private_room_user.room_id FROM user AS user INNER JOIN private_room_user AS private_room_user ON user._id = private_room_user.user_id) as user WHERE private_room_user.user_id = ? AND user.room_id = private_room_user.room_id AND user.timestamp >= ? GROUP BY user._id ORDER BY user.timestamp DESC";
	var inserts = [userIdField, dateField];
	return dbUtil.format("backupUsers", sql, inserts);
}

exports.backupFriendships = function(userIdField, dateField) {
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

	var sql = "SELECT * FROM user_friendship WHERE (user_id = ? OR to_user_id = ?) AND timestamp >= ? ORDER BY timestamp DESC";
	var inserts = [userIdField, userIdField, dateField];
	return dbUtil.format("backupFriendships", sql, inserts);
}

exports.backupPrivateRooms = function(userIdField, dateField) {
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

  var sql = "SELECT private_room._id, private_room.name, private_room.is_group, private_room.data_id, private_room.timestamp FROM private_room RIGHT JOIN private_room_user ON _id = room_id WHERE user_id = ? AND private_room.timestamp >= ? ORDER BY private_room.timestamp DESC";
  var inserts = [userIdField, dateField];
  return dbUtil.format("backupPrivateRooms", sql, inserts);
}

exports.backupPrivateRoomUser = function(userIdField, dateField) {
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

  var sql = "SELECT private_room_user.user_id, private_room_user.room_id, private_room_user.timestamp FROM private_room_user INNER JOIN (SELECT private_room._id FROM private_room RIGHT JOIN private_room_user ON _id = room_id WHERE user_id = ?) AS room ON room._id = private_room_user.room_id WHERE private_room_user.timestamp >= ? ORDER BY private_room_user.timestamp DESC";
  var inserts = [userIdField, dateField];
  return dbUtil.format("backupPrivateRoomUser", sql, inserts);
}

exports.backupPrivateMessage = function(userIdField, dateField) {
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

  var sql = "SELECT private_message._id, private_message.room_id, private_message.from_user_id, private_message.msg, private_message.is_read, private_message.data_id, private_message.timestamp FROM private_message INNER JOIN (SELECT private_room._id FROM private_room INNER JOIN private_room_user ON _id = room_id WHERE user_id = ?) AS room ON room._id = private_message.room_id WHERE private_message.timestamp >= ? ORDER BY private_message.timestamp DESC LIMIT 200";
  var inserts = [userIdField, dateField];
  return dbUtil.format("backupPrivateRoomUser", sql, inserts);
}

exports.backupFilters = function(userIdField, dateField){
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

  var sql = "SELECT * FROM user_filter WHERE user_id = ? AND timestamp >= ? ORDER BY timestamp DESC";
  var inserts = [userIdField, dateField];
  return dbUtil.format("backupFilters", sql, inserts);
}

exports.backupMessageData = function(userIdField, dateField){
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

  var sql = "SELECT data._id, data.url_small, data.url_original, data.url_blur, data.type, data.timestamp FROM data INNER JOIN (SELECT private_message._id, private_message.room_id, private_message.from_user_id, private_message.msg, private_message.is_read, private_message.data_id, private_message.timestamp FROM private_message LEFT JOIN (SELECT private_room._id FROM private_room RIGHT JOIN private_room_user ON _id = room_id WHERE user_id = ?) AS room ON room._id = private_message.room_id) as message ON data._id = message.data_id WHERE data.timestamp >= ? GROUP BY data._id ORDER BY data.timestamp DESC LIMIT 100";
  var inserts = [userIdField, dateField];
  return dbUtil.format("backupMessageData", sql, inserts);
}

exports.backupRoomData = function(userIdField, dateField){
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

  var sql = "SELECT data._id, data.url_small, data.url_original, data.url_blur, data.type, data.timestamp FROM data INNER JOIN (SELECT private_room._id, private_room.name, private_room.is_group, private_room.data_id, private_room.timestamp FROM private_room RIGHT JOIN private_room_user ON _id = room_id WHERE user_id = ?) as room ON data._id = room.data_id WHERE data.timestamp >= ? GROUP BY data._id ORDER BY data.timestamp DESC LIMIT 100";
  var inserts = [userIdField, dateField];
  return dbUtil.format("backupRoomData", sql, inserts);
}

exports.backupUserData = function(userIdField, dateField){
  if (dateField == null) {
    dateField = "2015-01-01 00:00:00"
  }

  var sql = "SELECT data._id, data.url_small, data.url_original, data.url_blur, data.type, data.timestamp FROM data INNER JOIN (SELECT user._id, user.email, user.pseudo, user.status, user.description, user.birthday, user.gender, user.data_id, user.timestamp FROM private_room_user, (SELECT user._id, user.email, user.pseudo, user.status, user.description, user.birthday, user.gender, user.data_id, user.timestamp, private_room_user.room_id FROM user AS user INNER JOIN private_room_user AS private_room_user ON user._id = private_room_user.user_id) as user WHERE private_room_user.user_id = ? AND user.room_id = private_room_user.room_id GROUP BY user._id) AS user ON data._id = user.data_id WHERE data.timestamp >= ? GROUP BY data._id ORDER BY data.timestamp DESC LIMIT 100";
  var inserts = [userIdField, dateField];
  return dbUtil.format("backupUserData", sql, inserts);
}

exports.backupUserCurrentData = function(userIdField){
  var sql = "SELECT data._id, data.url_small, data.url_original, data.url_blur, data.type, data.timestamp FROM data INNER JOIN (SELECT * FROM user WHERE _id = ?) AS user ON user.data_id = data._id";
  var inserts = [userIdField];
  return dbUtil.format("backupUserCurrentData", sql, inserts);
}
