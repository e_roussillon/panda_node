var dbUtil = require('../utils/dbUtil.js');

exports.clearTable = function() {
	var sql = "TRUNCATE TABLE private_message";
	return sql
}

exports.selectAllPrivateMessageByRoomId = function(roomIdField) {
	var sql = "SELECT * FROM private_message WHERE room_id = ? ORDER BY timestamp DESC"
	var inserts = [roomIdField];
	return dbUtil.format("selectAllPrivateMessageByRoomId", sql, inserts)
}

exports.insertPrivateMessage = function(roomIdField, sendIdField, msgField, dataIdField) {
	if (dataIdField == null) {
		dataIdField = -1
	}
	var sql = "INSERT INTO `private_message` (`room_id`, `from_user_id`, `msg`, `data_id`) VALUES (?, ?, ?, ?);"
	var inserts = [roomIdField, sendIdField, msgField, dataIdField];
	return dbUtil.format("insertPrivateMessage", sql, inserts)
}
