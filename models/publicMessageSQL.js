var dbUtil = require('../utils/dbUtil.js');
var enumUtil = require('../utils/enumResponse.js');

exports.clearTable = function() {
	var sql = "TRUNCATE TABLE public_message";
	return sql
}

exports.removedMessagePublic = function(messageIdFiled, userIdField) {
	var sql = "DELETE FROM public_message WHERE `_id`= ?  AND `user_id`= ? ";
	var inserts = [messageIdFiled, userIdField];
	return dbUtil.format("removedMessagePublic", sql, inserts);
}

exports.insertMessagePublic = function(publicRoomIdField, userIdField, msgField, isBroadcastField) {
	var array = publicRoomIdField.split("_");
	var isGeneral = "_"+array[1] === enumUtil.CHAT().CHAT_GENERAL.string ? "1" : "0"
	var isBroadcast = isBroadcastField === true ? "1" : "0"

	var sql = "INSERT INTO public_message (`public_room_id`, `is_general`, `is_broadcast`, `from_user_id`, `msg`, `metadata`, `type`) VALUES ( ? , ? , ? , ? , ? , ? , ? )";
	var inserts = [array[0], isGeneral, isBroadcast, userIdField, msgField, "", ""];
	return dbUtil.format("insertMessagePublic", sql, inserts);
}

exports.updateMessagePublic = function(messageIdField, publicRoomIdField, userIdField, msgField) {
	var array = publicRoomIdField.split("_");
	var isGeneral = "_"+array[1] === enumUtil.CHAT().CHAT_GENERAL.string ? "1" : "0"

	var sql = "UPDATE public_message SET `public_room_id`= ?, `is_general`= ? , `msg`= ? WHERE `_id`= ? AND `from_user_id`= ? ";
	var inserts = [array[0], isGeneral, msgField, messageIdField, userIdField];
	return dbUtil.format("updateMessagePublic", sql, inserts);
}

exports.selectMessagePublicByRoomId = function(publicRoomIdField) {
	var array = publicRoomIdField.split("_");
	var isGeneral = "_"+array[1] === enumUtil.CHAT().CHAT_GENERAL.string ? "1" : "0"

	var sql = "SELECT * FROM public_message WHERE public_room_id= ? AND is_general= ?";
	var inserts = [array[0], isGeneral];
	return dbUtil.format("selectMessagePublicByRoomId", sql, inserts);
}

exports.selectMessagePublicByUserId = function(userIdField) {
	var sql = "SELECT * FROM public_message WHERE user_id= ? ";
	var inserts = [userIdField];
	return dbUtil.format("selectMessagePublicByUserId", sql, inserts);
}