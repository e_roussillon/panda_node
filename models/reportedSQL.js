var dbUtil = require('../utils/dbUtil.js');

exports.findReported = function(userIdField) {
	var sql = "SELECT * FROM user_reported WHERE `user_id`= ? ";
	var inserts = [userIdField];
	return dbUtil.format("findReported", sql, inserts);
	// return "SELECT * FROM reported WHERE `user_id`=" + userIdField
}