var dbUtil = require('../utils/dbUtil.js');

exports.clearTable = function() {
	var sql = "TRUNCATE TABLE private_room; TRUNCATE TABLE private_room_user;";
	return sql
}

exports.selectAllPrivateRoomsByUserId = function(userIdField) {
	var sql = "SELECT private_room._id AS conversation_id, private_room.name, private_room.is_group, chat_data.url_small AS chat_thumb, user._id AS user_id, user.pseudo, user_data.url_small AS user_thumb, user_friendship.status AS user_status, user_friendship.last_action AS user_status_last_action, user_authentication.is_online AS user_is_online, user_authentication.timestamp AS user_last_connection, private_message.msg, private_message.timestamp AS msg_sent, message_count.num_of_msg_not_read FROM private_room, private_room_user, private_message, user, user_friendship, user_authentication, data AS user_data, data AS chat_data, (SELECT room._id AS room_id FROM private_room_user AS chat JOIN private_room AS room WHERE chat.room_id = room._id AND chat.user_id = ?) AS room_user, (SELECT message.room_id, COUNT(message.msg) as num_of_msg_not_read FROM private_message AS message WHERE message.is_read = 0) AS message_count WHERE room_user.room_id = private_room._id AND message_count.room_id = private_room._id AND private_room_user.room_id = private_room._id AND private_room_user.user_id = user._id AND private_message.room_id = private_room._id AND user_data._id = user.data_id AND chat_data._id = private_room.data_id AND (user_friendship.user_id = ? AND user_friendship.to_user_id = user._id) OR (user_friendship.user_id = user._id AND user_friendship.to_user_id = ?) AND private_room_user.user_id <> ? AND user_authentication.user_id = user._id GROUP BY private_room._id ORDER BY private_message.timestamp DESC"
	var inserts = [userIdField, userIdField, userIdField, userIdField];
	return dbUtil.format("selectAllPrivateRoomsByUserId", sql, inserts)
}

exports.selectPrivateRoomWithFriendIdByUserId = function(userIdField, toUserIdFiend) {
	var sql = "SELECT private_room._id as room_id, user.pseudo, user_data.url_small AS user_thumb FROM private_room, private_room_user, user, data AS user_data, (SELECT room._id AS room_id FROM private_room_user AS chat JOIN private_room AS room WHERE chat.room_id = room._id AND chat.user_id = ?) AS room_user WHERE private_room.is_group = 0 AND room_user.room_id = private_room._id AND private_room_user.room_id = private_room._id AND private_room_user.user_id = user._id AND user_data._id = user.data_id AND private_room_user.user_id = ? GROUP BY private_room._id"
	var inserts = [userIdField, toUserIdFiend];
	return dbUtil.format("selectPrivateRoomWithFriendIdByUserId", sql, inserts)
}

exports.insertPrivateRoom = function(userIdField, toUserIdFiend) {
	var sql = "INSERT INTO `private_room` (`name`, `is_group`, `data_id`) VALUES ('', 0, 1);  INSERT INTO `private_room_user` (`user_id`, `room_id`) VALUES (?, LAST_INSERT_ID()); INSERT INTO `private_room_user` (`user_id`, `room_id`) VALUES (? , LAST_INSERT_ID());"
	var inserts = [userIdField, toUserIdFiend];

	return dbUtil.format("insertPrivateRoom", sql, inserts);
}
