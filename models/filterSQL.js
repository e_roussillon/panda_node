var dbUtil = require('../utils/dbUtil.js');

exports.clearTable = function() {
	var sql = "TRUNCATE TABLE user_filter";
	return sql
}

exports.removedFilter = function(filterIdFiled, userIdField) {
	var sql = "DELETE FROM user_filter WHERE `_id`= ?  AND `user_id`= ? ";
	var inserts = [filterIdFiled, userIdField];
	return dbUtil.format("removedFilter", sql, inserts);
}

exports.insertFilter = function(userIdField, nameField, isHighlightField) {
	var isHighlightValue = isHighlightField === true ? 1 : 0
	var sql = "INSERT INTO user_filter (`user_id`, `name`, `is_highlight`) VALUES ( ? , ? , ? )";
	var inserts = [userIdField, nameField, isHighlightValue];
	return dbUtil.format("insertFilter", sql, inserts);
}

exports.updateFilter = function(user_filterIdFiled, userIdField, nameField, isHighlightField) {
	var isHighlightValue = isHighlightField === true ? 1 : 0
	var sql = "UPDATE user_filter SET `name`= ? , `is_highlight`= ? WHERE `_id`= ? AND `user_id`= ? ";
	var inserts = [nameField, isHighlightValue, user_filterIdFiled, userIdField];
	return dbUtil.format("updateFilter", sql, inserts);
}

exports.selectFilterByUserId = function(userIdField, isHighlight) {
	var sql = "SELECT _id as filter_id, name, is_highlight FROM user_filter WHERE `user_id`= ? AND `is_highlight`= ? ORDER BY name";
	var inserts = [userIdField, isHighlight];
	return dbUtil.format("selectFilterByUserId", sql, inserts);
}
