var dbUtil = require('../utils/dbUtil.js');

exports.friendshipNotFiend = function() { return 0}
exports.friendshipPending = function() { return 1}
exports.friendshipCancelRequest = function() { return 2}
exports.friendshipRefusedBeforeBeFriend = function() { return 3}
exports.friendshipFriend = function() { return 4}
exports.friendshipRefusedAfterBeFriend = function() { return 5}
exports.friendshipBlocked = function() { return 6}
exports.friendshipUnBlocked = function() { return 7}
exports.friendshipBothBlocked = function() { return 8}

exports.insertFriendship = function(userIdField, toUserIdField, statusField, lastActionField) {
	var minId = Math.min(userIdField, toUserIdField);
	var maxId = Math.max(userIdField, toUserIdField);

	var sql = "INSERT INTO user_friendship (`user_id`, `to_user_id`, `status`, `last_action`) VALUES ( ? , ? , ? , ? )";
	var inserts = [minId, maxId, statusField, lastActionField];
	return dbUtil.format("insertFriendship", sql, inserts);
}

exports.findFriendshipByUserIdOrToUserId = function(userIdField, toUserIdField) {
	var minId = Math.min(userIdField, toUserIdField);
	var maxId = Math.max(userIdField, toUserIdField);

	var sql = "SELECT `_id`, `user_id`, `to_user_id`, `status`, `last_action` FROM user_friendship WHERE `user_id`= ? and `to_user_id`= ? ";
	var inserts = [minId, maxId];
	return dbUtil.format("findFriendshipByUserIdOrToUserId", sql, inserts);
}

exports.updateFriendshipStatusAndWhoCancel = function(user_friendshipIdField, statusField, lastActionField) {
	var sql = "UPDATE user_friendship SET `status`= ? , `last_action`= ? WHERE `_id`= ? ";
	var inserts = [statusField, lastActionField, user_friendshipIdField];
	return dbUtil.format("updateFriendshipStatusAndWhoCancel", sql, inserts);
}

exports.findFriendshipByUserId = function(userIdField) {
	var sql = "SELECT `user`.`pseudo`, `user_friendship`.`user_id`, `user_friendship`.`to_user_id`, `user_friendship`.`status`, `user_friendship`.`last_action`, `user_friendship`.`timestamp` FROM `user_friendship` INNER JOIN `user` ON `user_friendship`.`user_id`=`user`._id OR `user_friendship`.`to_user_id`=`user`.`_id` WHERE `user`.`_id` <> ? AND `user_friendship`.`user_id` = ? OR `user_friendship`.`to_user_id` = ? ORDER BY `user_friendship`.`status`, `user_friendship`.`timestamp`";
	var inserts = [userIdField, userIdField, userIdField];
	return dbUtil.format("findFriendshipByUserId", sql, inserts);
}
