var dbUtil = require('../utils/dbUtil.js');

exports.notificationAskFriendship = function() { return 0}

exports.insertNotification = function(userIdField, fromIdField, typeField, metadataField) {
	var sql = "INSERT INTO user_notification (`user_id`, `from_id`, `type`, `metadata`) VALUES ( ? , ? , ? , ? )";
	var inserts = [userIdField, fromIdField, typeField, metadataField];
	return dbUtil.format("insertNotification", sql, inserts);
}