var dbUtil = require('../utils/dbUtil.js');

exports.clearTable = function() {
	var sql = "TRUNCATE TABLE user";
	return sql
}

exports.clearTablePassword = function() {
	var sql = "TRUNCATE TABLE user_password";
	return sql
}

exports.insertUser = function(emailField, pseudoField, birthdayField, genderField, passwordField) {
	//Default image is '1'
	var sql = "INSERT INTO user (`email`, `pseudo`, `birthday`, `gender`, `data_id`) VALUES ( ? , ? , ? , ? , '1'); INSERT INTO user_password (`user_id`, `password`) VALUES ( LAST_INSERT_ID() , ? );"
	var inserts = [emailField, pseudoField, birthdayField, genderField, passwordField];

	return dbUtil.format("insertUser", sql, inserts);
}

exports.insertSocialUser = function(emailField, pseudoField, socialIdField, socialTypeField) {
	//Default image is '1'
	var sql = "INSERT INTO user (`email`, `pseudo`, `data_id`) VALUES ( ? , ? , '1'); INSERT INTO user_social (`user_id`, `social_id`, `social_type`) VALUES ( LAST_INSERT_ID() , ? , ? );"
	var inserts = [emailField, pseudoField, socialIdField, socialTypeField];

	return dbUtil.format("insertSocialUser", sql, inserts);
}

// exports.insertUserPorfile = function(statusField, messageField, imageIdField) {
// 	var sql = "INSERT INTO user_profile (`status`, `message`, `image_id`) VALUES ( ? , ? , ? )";
// 	var inserts = [statusField, messageField, imageIdField];
// 	return dbUtil.format("insertUserPorfile", sql, inserts);
// 	// return "INSERT INTO user_profile (`status`, `message`, `image_id`) VALUES ('"+ statusField +"', '"+ messageField +"', '"+ imageIdField +"')";
// }

exports.findUserBySocialId = function(socialIdField, socialTypeField) {
	var sql = "SELECT user._id as user_id, user.email, user.pseudo FROM user JOIN user_social ON user._id = user_social.user_id WHERE `social_id`= ?  AND `social_type`= ? ";
	var inserts = [socialIdField, socialTypeField];
	return dbUtil.format("findUserBySocialId", sql, inserts);
}

exports.findUserByEmailAndPsw = function(emailField, passwordField) {
	var sql = "SELECT user._id as user_id, user.email, user.pseudo FROM user JOIN user_password ON user._id = user_password.user_id WHERE `email`= ?  AND `password`= ? ";
	var inserts = [emailField, passwordField];
	return dbUtil.format("findUserByEmailAndPsw", sql, inserts);
}

exports.findUserById = function(userIdField) {
	var sql = "SELECT user._id AS user_id, user.email, user.pseudo, user.birthday, user.gender, user.description, data._id AS image_id, data.url_blur, data.url_small, data.url_original FROM user LEFT JOIN data ON user.data_id = data._id WHERE user._id = ?";
	var inserts = [userIdField];
	return dbUtil.format("findUserById", sql, inserts);
}

exports.findUsersExpectI = function(userIdField) {
	var sql = "SELECT `user`.`_id` AS user_id, `user`.`pseudo`, `data`.`_id` AS image_id, `data`.`url_blur`, `data`.`url_small` FROM user JOIN data ON user.data_id = data._id WHERE user._id <> ? ";
	var inserts = [userIdField];
	return dbUtil.format("findUsersExpectI", sql, inserts);
	// return "SELECT `user`.`pseudo`, `user_profile`.`status`, `user_profile`.`message` FROM `user` INNER JOIN `user_profile` ON `user`.`user_profile_id`=`user_profile`.`_id` WHERE `user`.`_id` ="+ userIdField;
}

exports.updateUserById = function(userIdField, emailField, pseudoField, birthdayField, genderField, detailsField) {
	var sql = "UPDATE `user` SET `email`= ? , `pseudo`= ? , `birthday`= ? , `gender`= ? , `description`= ? WHERE `_id`= ? ";
	var inserts = [emailField, pseudoField, birthdayField, genderField, detailsField, userIdField];
	return dbUtil.format("updateUserById", sql, inserts);
}

// exports.deleteUserProfileById = function(userProfileIdField) {
// 	var sql = "DELETE FROM `user_profile` WHERE `_id`= ? ";
// 	var inserts = [userProfileIdField];
// 	return dbUtil.format("deleteUserProfileById", sql, inserts);
// }
