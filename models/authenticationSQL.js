var dbUtil = require('../utils/dbUtil.js');

exports.clearTable = function(userIdField) {
	var sql = "TRUNCATE TABLE user_authentication";
	return sql
}

exports.removedAuthenByUserId = function(userIdField) {
	var sql = "DELETE FROM user_authentication WHERE `user_id`= ? ";
	var inserts = [userIdField];
	return dbUtil.format("removedAuthenByUserId", sql, inserts);
}

exports.findAuthenNorReportedByUserId = function(userIdField) {
	var sql = "SELECT user_authentication._id, user_authentication.user_id, user_authentication.token, user_authentication.socket_id, user_authentication.room, case when user_reported._id is null then 'false' else 'true' end as is_reported, user_authentication.timestamp FROM user_authentication LEFT JOIN user_reported ON user_authentication.user_id = user_reported.user_id WHERE user_authentication.user_id = ? UNION SELECT user_authentication._id, user_authentication.user_id, user_authentication.token, user_authentication.socket_id, user_authentication.room, case  when user_reported._id is null then 'false' else 'true' end as isReported, user_authentication.timestamp FROM user_authentication RIGHT JOIN user_reported ON user_authentication.user_id = user_reported.user_id WHERE user_reported.user_id = ? ";
	var inserts = [userIdField, userIdField];
	return dbUtil.format("findAuthenNorReportedByUserId", sql, inserts);

}

exports.findAuthenByUserId = function(userIdField) {
	var sql = "SELECT * FROM user_authentication WHERE `user_id`= ? ";
	var inserts = [userIdField];
	return dbUtil.format("findAuthenByUserId", sql, inserts);
}

exports.findAuthenByToken = function(tokenField) {
	var sql = "SELECT * FROM user_authentication WHERE `token`= ? ";
	var inserts = [tokenField];
	return dbUtil.format("findAuthenByToken", sql, inserts);
}

exports.findAuthenBySocket = function(soketIdField) {
	var sql = "SELECT * FROM user_authentication WHERE `socket_id`= ? ";
	var inserts = [soketIdField];
	return dbUtil.format("findAuthenBySocket", sql, inserts);
}

exports.insertAuthen = function(userIdField, tokenField) {
	var sql = "INSERT INTO user_authentication (`user_id`, `token`) VALUES (?, ?)";
	var inserts = [userIdField, tokenField];
	return dbUtil.format("insertAuthen", sql, inserts);
}

exports.updateAuthenToken = function(userIdField, tokenField) {
	var sql = "UPDATE user_authentication SET `token`= ? WHERE `user_id`= ? ";
	var inserts = [tokenField, userIdField];
	return dbUtil.format("updateAuthenToken", sql, inserts);
}

exports.updateAuthenSocket = function(tokenField, socketIdField) {
	var sql = "UPDATE user_authentication SET `socket_id`= ? WHERE `token`= ? ";
	var inserts = [socketIdField, tokenField];
	return dbUtil.format("updateAuthenSocket", sql, inserts);
}

exports.updateAuthenSocketRoom = function(socketIdField, roomIdField) {
	var sql = "UPDATE user_authentication SET `room`= ? WHERE `socket_id`= ? ";
	var inserts = [roomIdField, socketIdField];
	return dbUtil.format("updateAuthenSocketRoom", sql, inserts);
}

exports.removedAuthenSocket = function(socketIdField) {
	var sql = "UPDATE user_authentication SET `socket_id`= '' WHERE `socket_id`= ? ";
	var inserts = [socketIdField];
	return dbUtil.format("removedAuthenSocket", sql, inserts);
}