var dbUtil = require('../utils/dbUtil.js');

exports.clearTable = function() {
	var sql = "TRUNCATE TABLE public_room_localized";
	return sql
}

exports.removedPublicRoomLocalized = function(localizationRoomIdFiled) {
	var sql = "DELETE FROM public_room_localized WHERE `_id`= ? ";
	var inserts = [localizationRoomIdFiled];
	return dbUtil.format("removedPublicRoomLocalized", sql, inserts);
}

exports.insertPublicRoomLocalized = function(publicRoomIdField, latitideField, longitudeField) {
	var sql = "INSERT INTO public_room_localized (`public_room_id`, `latitide`, `longitude`) VALUES ( ? , ? , ? )";
	var inserts = [publicRoomIdField, latitideField, longitudeField];
	return dbUtil.format("insertPublicRoomLocalized", sql, inserts);
}

exports.updatePublicRoomLocalized = function(localizationRoomIdFiled, publicRoomIdField, latitideField, longitudeField) {
	var sql = "UPDATE public_room_localized SET `public_room_id`= ? , `latitide`= ? , `longitude`= ? WHERE `_id`= ? ";
	var inserts = [publicRoomIdField, latitideField, longitudeField, localizationRoomIdFiled];
	return dbUtil.format("updatePublicRoomLocalized", sql, inserts);
}

exports.selectPublicRoomLocalizedByLatAndLong = function(latitideField, longitudeField) {
	var sql = "SELECT * FROM public_room_localized WHERE `latitide`= ? AND `longitude`= ? ";
	var inserts = [latitideField, longitudeField];
	return dbUtil.format("selectPublicRoomLocalizedByLatAndLong", sql, inserts);
}