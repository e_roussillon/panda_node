var dbUtil = require('../utils/dbUtil.js');

exports.clearTable = function() {
	var sql = "TRUNCATE TABLE public_room";
	return sql
}

exports.initTable = function() {
	var sql = "INSERT INTO public_room (`original`, `name`, `type`, `lft`, `rgt`) VALUES ( 'world' , 'world' , 'world' , 0 , 1 )";
	return sql
}

exports.selectRoomById = function(roomIdField) {
	var sql = "SELECT original, type FROM public_room WHERE _id = ?"
	var inserts = [roomIdField];
	return dbUtil.format("selectRoomById", sql, inserts)
}

exports.selectPathFromPublicRoomIdToRoot = function(childIdField) {
	var sql = "SELECT parent._id, parent.name, parent.type FROM public_room AS node, public_room AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node._id = ? ORDER BY node.lft;";
	var inserts = [childIdField];
	return dbUtil.format("selectPathFromPublicRoomIdToRoot", sql, inserts);
	// return "SELECT parent.name, parent.type FROM public_room AS node, public_room AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node._id = " + childIdField + " ORDER BY node.lft;"
}

exports.selectEntireTree = function() {
	var sql = "SELECT CONCAT( REPEAT( ' ', (COUNT(parent.name) - 1) ), node.name) AS name, node.type FROM public_room AS node, public_room AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.type <> 'world' GROUP BY node._id ORDER BY node.lft"
	return dbUtil.formatSQL("selectEntireTree", sql)
}

exports.selectAllNodesBelowPublicRoomId = function(childIdField) {
	var sql = "SELECT node._id, node.original, node.name, node.type, (COUNT(parent.name) - (sub_tree.depth + 1)) AS depth FROM public_room AS node, public_room AS parent, public_room AS sub_parent,(SELECT node.name, (COUNT(parent.name) - 1) AS depth FROM public_room AS node, public_room AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node._id = ? GROUP BY node.name, node.type ORDER BY node.lft ) AS sub_tree WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.lft BETWEEN sub_parent.lft AND sub_parent.rgt AND sub_parent.name = sub_tree.name GROUP BY node.name, node.type ORDER BY node.lft;";
	var inserts = [childIdField];
	return dbUtil.format("selectAllNodesBelowPublicRoomId", sql, inserts);
	// return "SELECT node.name, (COUNT(parent.name) - (sub_tree.depth + 1)) AS depth FROM public_room AS node, public_room AS parent, public_room AS sub_parent,(SELECT node.name, (COUNT(parent.name) - 1) AS depth FROM public_room AS node, public_room AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node._id = " + childIdField + " GROUP BY node.name ORDER BY node.lft ) AS sub_tree WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.lft BETWEEN sub_parent.lft AND sub_parent.rgt AND sub_parent.name = sub_tree.name GROUP BY node.name ORDER BY node.lft;"
}

exports.selectImmediateNodesBelowPublicRoomId = function(childIdField) {
	var sql = "SELECT node._id, node.name, (COUNT(parent.name) - (sub_tree.depth + 1)) AS depth FROM public_room AS node, public_room AS parent, public_room AS sub_parent, (SELECT node.name, (COUNT(parent.name) - 1) AS depth FROM public_room AS node, public_room AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node._id = ? GROUP BY node.name, node.type ORDER BY node.lft) AS sub_tree WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.lft BETWEEN sub_parent.lft AND sub_parent.rgt AND sub_parent.name = sub_tree.name GROUP BY node.name, node.type HAVING depth = 1 ORDER BY node.lft;";
	var inserts = [childIdField];
	return dbUtil.format("selectImmediateNodesBelowPublicRoomId", sql, inserts);
}

exports.selectImmediateNodesBelowPublicRoomIdAndName = function(childIdField, nameField) {
	var name = trimeString(nameField)
	var sql = "SELECT node._id, node.name, (COUNT(parent.name) - (sub_tree.depth + 1)) AS depth FROM public_room AS node, public_room AS parent, public_room AS sub_parent, (SELECT node.name, (COUNT(parent.name) - 1) AS depth FROM public_room AS node, public_room AS parent WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node._id = ? GROUP BY node.name, node.type ORDER BY node.lft) AS sub_tree WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.lft BETWEEN sub_parent.lft AND sub_parent.rgt AND sub_parent.name = sub_tree.name AND node.name = ? GROUP BY node.name, node.type HAVING depth = 1 ORDER BY node.lft;";
	var inserts = [childIdField, name];
	return dbUtil.format("selectImmediateNodesBelowPublicRoomIdAndName", sql, inserts);
}

exports.insertPublicRoomNewLeaf = function(parentIdField, nameField, typeField) {
	var name = trimeString(nameField+"_"+parentIdField+"_"+typeField)
	var sql = "LOCK TABLE public_room WRITE;SELECT @myRight := rgt FROM public_room WHERE _id = ? ;UPDATE public_room SET rgt = rgt + 2 WHERE rgt > @myRight;UPDATE public_room SET lft = lft + 2 WHERE lft > @myRight;INSERT INTO public_room (`original`, `name`, `type`, `lft`, `rgt`) VALUES ( ? , ? , ?, @myRight + 1, @myRight + 2);UNLOCK TABLES;";
	var inserts = [parentIdField, nameField, name, typeField];
	return dbUtil.format("insertPublicRoomNewLeaf", sql, inserts);
}

exports.insertPublicRoomFirstLeaf = function(parentIdField, nameField, typeField) {
	var name = trimeString(nameField+"_"+parentIdField+"_"+typeField)
	var sql = "LOCK TABLE public_room WRITE;SELECT @myLeft := lft FROM public_room WHERE _id = ? ;UPDATE public_room SET rgt = rgt + 2 WHERE rgt > @myLeft;UPDATE public_room SET lft = lft + 2 WHERE lft > @myLeft;INSERT INTO public_room (`original`, `name`, `type`, `lft`, `rgt`) VALUES( ? , ? , ?, @myLeft + 1, @myLeft + 2);UNLOCK TABLES;"
	var inserts = [parentIdField, nameField, name, typeField];
	return dbUtil.format("insertPublicRoomFirstLeaf", sql, inserts);
}

exports.deletePublicRoomLeaf = function(childIdField) {
	var sql = "LOCK TABLE public_room WRITE;SELECT @myLeft := lft, @myRight := rgt, @myWidth := rgt - lft + 1 FROM public_room WHERE _id = ? ;DELETE FROM public_room WHERE lft BETWEEN @myLeft AND @myRight;UPDATE public_room SET rgt = rgt - @myWidth WHERE rgt > @myRight;UPDATE public_room SET lft = lft - @myWidth WHERE lft > @myRight;UNLOCK TABLES;";
	var inserts = [childIdField];
	return dbUtil.format("deletePublicRoomLeaf", sql, inserts);
	// return "LOCK TABLE public_room WRITE;SELECT @myLeft := lft, @myRight := rgt, @myWidth := rgt - lft + 1 FROM public_room WHERE _id =" + childIdField + ";DELETE FROM public_room WHERE lft BETWEEN @myLeft AND @myRight;UPDATE public_room SET rgt = rgt - @myWidth WHERE rgt > @myRight;UPDATE public_room SET lft = lft - @myWidth WHERE lft > @myRight;UNLOCK TABLES;"
}

exports.deletePublicRoomNodeWithLeafs = function(childIdField) {
	var sql = "LOCK TABLE public_room WRITE;SELECT @myLeft := lft, @myRight := rgt, @myWidth := rgt - lft + 1 FROM public_room WHERE _id = ? ;DELETE FROM public_room WHERE lft BETWEEN @myLeft AND @myRight;UPDATE public_room SET rgt = rgt - @myWidth WHERE rgt > @myRight;UPDATE public_room SET lft = lft - @myWidth WHERE lft > @myRight;UNLOCK TABLES;";
	var inserts = [childIdField];
	return dbUtil.format("deletePublicRoomNodeWithLeafs", sql, inserts);
	// return "LOCK TABLE public_room WRITE;SELECT @myLeft := lft, @myRight := rgt, @myWidth := rgt - lft + 1 FROM public_room WHERE _id =" + childIdField + ";DELETE FROM public_room WHERE lft BETWEEN @myLeft AND @myRight;UPDATE public_room SET rgt = rgt - @myWidth WHERE rgt > @myRight;UPDATE public_room SET lft = lft - @myWidth WHERE lft > @myRight;UNLOCK TABLES;"
}

exports.deletePublicRoomNodeWithoutLeafs = function(childIdField) {
	var sql = "LOCK TABLE public_room WRITE;SELECT @myLeft := lft, @myRight := rgt, @myWidth := rgt - lft + 1 FROM public_room WHERE _id = ? ;DELETE FROM public_room WHERE lft = @myLeft;UPDATE public_room SET rgt = rgt - 1, lft = lft - 1 WHERE lft BETWEEN @myLeft AND @myRight;UPDATE public_room SET rgt = rgt - 2 WHERE rgt > @myRight;UPDATE public_room SET lft = lft - 2 WHERE lft > @myRight;UNLOCK TABLES;";
	var inserts = [childIdField];
	return dbUtil.format("deletePublicRoomNodeWithoutLeafs", sql, inserts);
	// return "LOCK TABLE public_room WRITE;SELECT @myLeft := lft, @myRight := rgt, @myWidth := rgt - lft + 1 FROM public_room WHERE _id =" + childIdField + ";DELETE FROM public_room WHERE lft = @myLeft;UPDATE public_room SET rgt = rgt - 1, lft = lft - 1 WHERE lft BETWEEN @myLeft AND @myRight;UPDATE public_room SET rgt = rgt - 2 WHERE rgt > @myRight;UPDATE public_room SET lft = lft - 2 WHERE lft > @myRight;UNLOCK TABLES;"
}

function trimeString(stringToTrim) {
	return stringToTrim.toLowerCase().trim().replace(/\s+/g, '-');
}

