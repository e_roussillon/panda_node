var logger = require('./logger.js');
var enumUtil = require('./enumResponse.js');

var full = true

function result (status, protocol, method, code, msg, data) {
	// var dataField = data == null ?  null : JSON.stringify(data)
	response = {"response" : {"status" : status, "protocol" : protocol, "method": method, "code": code, "msg" : msg}, "data" : data}
	
	logger.debug("------> HEADER 	: %s", JSON.stringify(response.response))
	logger.debug("------> DATA 	: %s", JSON.stringify(response.data))

	return response
}

exports.result200 = function(req, res, method, message) {
	logger.debug("")
	logger.debug("RESPONSE API *** %s%s", req.headers["host"], req.url)

	var protocolField = full == true ? enumUtil.GLOBAL().API.string : enumUtil.GLOBAL().API.value
	var methodField = full == true ? method.string : method.value

	var response = result(200, protocolField, methodField, -1, "ok", message)
	res.json(response, 200)
}

//Unauthorized
exports.result401 = function(req, res) {
	logger.debug("")
	logger.debug("RESPONSE API *** %s%s", req.headers["host"], req.url)

	var protocolField = full == true ? enumUtil.GLOBAL().API.string : enumUtil.GLOBAL().API.value
	var methodField = full == true ? enumUtil.API().TOKEN.string : enumUtil.API().TOKEN.value

	var response = result(401, protocolField, methodField, 0, "Token invalide", null)
	res.json(response, 401)
}

exports.result401App = function(req, res) {
	logger.debug("")
	logger.debug("RESPONSE API *** %s%s", req.headers["host"], req.url)

	var protocolField = full == true ? enumUtil.GLOBAL().API.string : enumUtil.GLOBAL().API.value
	var methodField = full == true ? enumUtil.API().APP_ID.string : enumUtil.API().APP_ID.value

	var response = result(401, protocolField, methodField, 0, "App ID invalide", null)
	res.json(response, 401)
}

//Forbidden
exports.result403 = function(req, res, method, code) {
	logger.debug("")
	logger.debug("RESPONSE API *** %s%s", req.headers["host"], req.url)

	var protocolField = full == true ? enumUtil.GLOBAL().API.string : enumUtil.GLOBAL().API.value
	var methodField = full == true ? method.string : method.value

	var response = result(403, protocolField, methodField, code.value, code.string, null)
	res.json(response, 403)
}

//Forbidden
exports.result403WithData = function(req, res, method, code, data) {
	logger.debug("")
	logger.debug("RESPONSE API *** %s%s", req.headers["host"], req.url)

	var protocolField = full == true ? enumUtil.GLOBAL().API.string : enumUtil.GLOBAL().API.value
	var methodField = full == true ? method.string : method.value

	var response = result(403, protocolField, methodField, code.value, code.string, data)
	res.json(response, 403)
}

//Not Acceptable
exports.result406 = function(req, res, method, message) {
	logger.debug("")
	logger.debug("RESPONSE API *** %s%s", req.headers["host"], req.url)

	var protocolField = full == true ? enumUtil.GLOBAL().API.string : enumUtil.GLOBAL().API.value
	var methodField = full == true ? method.string : method.value
	
	var response = result(406, protocolField, methodField, 0, "Data base error", null)
	logger.error("------>	ERROR DB *** %s", JSON.stringify(message))
	res.json(response, 406)
}

//
//IO
//

exports.requestIO = function(io, socket, method, data) {
	logger.debug("")
	logger.debug("IO *** %s", enumUtil.IO().REQUEST.string)

	var protocolField = full == true ? enumUtil.GLOBAL().IO.string : enumUtil.GLOBAL().IO.value
	var methodField = full == true ? method.string : method.value

	var response = result(200, protocolField, methodField, -1, "ok", data)
	io.to(socket.id).emit(enumUtil.IO().REQUEST.string, response);
}

exports.resultIO = function(io, socket, method, data) {
	logger.debug("")
	logger.debug("resultIO")
	logger.debug("IO *** %s", enumUtil.IO().RESULT.string)

	var protocolField = full == true ? enumUtil.GLOBAL().IO.string : enumUtil.GLOBAL().IO.value
	var methodField = full == true ? method.string : method.value

	var response = result(200, protocolField, methodField, -1, "ok", data)
	io.to(socket.id).emit(enumUtil.IO().RESULT.string, response);
}

exports.resultIOToRoom = function(io, room, method, data) {
	logger.debug("")
	logger.debug("resultIOToRoom")
	logger.debug("IO *** %s", enumUtil.IO().RESULT.string)
	logger.debug("------> ROOM 	: %s", room)

	var protocolField = full == true ? enumUtil.GLOBAL().IO.string : enumUtil.GLOBAL().IO.value
	var methodField = full == true ? method.string : method.value

	var response = result(200, protocolField, methodField, -1, "ok", data)
	io.sockets.in(room).emit(enumUtil.IO().RESULT.string, response);
}

exports.resultIO401 = function(io, socket) {
	logger.debug("")
	logger.debug("resultIO401")
	logger.debug("IO *** %s", enumUtil.IO().RESULT.string)

	var protocolField = full == true ? enumUtil.GLOBAL().IO.string : enumUtil.GLOBAL().IO.value
	var methodField = full == true ? enumUtil.API().TOKEN.string : enumUtil.API().TOKEN.value

	var response = result(401, protocolField, methodField, 0, "Token invalide", null)
	io.to(socket.id).emit(enumUtil.IO().RESULT.string, response);
}

exports.resultIO401App = function(io, socket) {
	logger.debug("")
	logger.debug("resultIO401App")
	logger.debug("IO *** %s", enumUtil.IO().RESULT.string)

	var protocolField = full == true ? enumUtil.GLOBAL().API.string : enumUtil.GLOBAL().API.value
	var methodField = full == true ? enumUtil.API().APP_ID.string : enumUtil.API().APP_ID.value

	var response = result(401, protocolField, methodField, 0, "App ID invalide", null)
	io.to(socket.id).emit(enumUtil.IO().RESULT.string, response);
	socket.disconnect();
}

exports.resultIO403 = function(io, socket, method, code) {
	logger.debug("")
	logger.debug("resultIO401App")
	logger.debug("IO *** %s", enumUtil.IO().RESULT.string)

	var protocolField = full == true ? enumUtil.GLOBAL().IO.string : enumUtil.GLOBAL().IO.value
	var methodField = full == true ? method.string : method.value

	var response = result(403, protocolField, methodField, code.value, code.string, null)
	io.to(socket.id).emit(enumUtil.IO().RESULT.string, response);
}

//Not Acceptable
exports.resultIO406 = function(io, socket, method, message) {
	logger.debug("")
	logger.debug("resultIO406")
	logger.debug("IO *** %s", enumUtil.IO().RESULT.string)

	var protocolField = full == true ? enumUtil.GLOBAL().API.string : enumUtil.GLOBAL().API.value
	var methodField = full == true ? method.string : method.value
	
	var response = result(406, protocolField, methodField, 0, "Data base error", null)
	logger.error("------>	ERROR DB *** %s", JSON.stringify(message))
	io.to(socket.id).emit(enumUtil.IO().RESULT.string, response);
}

function getFullPath(req) {
	return req.url
}



