exports.validateDate = function(date) {
    var dateWrapper = new Date(date);
    return !isNaN(dateWrapper.getDate());
}
