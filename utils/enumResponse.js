exports.timeoutAuth = function() { return 10000}


exports.GLOBAL = function(){
	return defineEnum({
	    API : {
	        value : 0,
	        string : 'API'
	    },
	    IO : {
	        value : 1,
	        string : 'IO'
	    }
	});
}

//
// API
//

exports.API = function(){
  	return defineEnum({
	    RENEW_AUTH : {
	        value : 0,
	        string : 'RENEW_AUTH'
	    },
	    TOKEN : {
	        value : 1,
	        string : 'TOKEN'
	    },
		APP_ID : {
	        value : 2,
	        string : 'APP_ID'
	    },
		AUTHENTICATION : {
	        value : 3,
	        string : 'AUTHENTICATION'
	    },
	    GENERIC_USER : {
	        value : 4,
	        string : 'GENERIC_USER'
	    },
	    CREATE_USER : {
	        value : 5,
	        string : 'CREATE_USER'
	    },
	    PROFIL : {
	        value : 6,
	        string : 'PROFIL'
	    },
	    UPDATE_PROFIL : {
	    	value : 7,
	        string : 'UPDATE_PROFIL'
	    },
	    FRIENDSHIP : {
	        value : 8,
	        string : 'FRIENDSHIP'
	    },
	    FILTER : {
	        value : 9,
	        string : 'FILTER'
	    },
			BACKUP : {
					value: 0,
					string : 'BACKUP'
			},
	    ROOM : {
	        value : 11,
	        string : 'ROOM'
	    },
	    MESSAGE : {
	        value : 12,
	        string : 'MESSAGE'.toLowerCase()
	    }
	});
}

//
//Local
//

exports.GENERIC_USER = function(){
	return defineEnum({
	    ERROR_USER_BLOCKED : {
	        value : 0,
	        string : 'User is blocked'
	    },
	    ERROR_USER_NOT_FOUND : {
	        value : 1,
	        string : 'User is not found'
	    },
	    ERROR_PLATFORM_DEVICE : {
	        value : 2,
	        string : 'Platform Device is empty'
	    },
	    ERROR_DEVICE_ID : {
	        value : 3,
	        string : 'Device id is empty'
	    }
	})
}

//
// API
//

exports.AUTHENTICATION = function(){

	return defineEnum({
	    ERROR_EMAIL : {
	        value : 0,
	        string : 'Email is Empty'
	    },
	    ERROR_PASSWORD : {
	        value : 1,
	        string : 'Password is Empty'
	    },
	    ERROR_TOKEN : {
	        value : 2,
	        string : 'Token is Empty'
	    }
	})
}

exports.CREATE_USER = function(){
	return defineEnum({
	    ERROR_PSEUDO_EMPTY : {
	        value : 0,
	        string : 'Pseudo need to be between 4 and 30.'
	    },
	    ERROR_EMAIL_EMPTY : {
	        value : 1,
	        string : 'Email is Empty'
	    },
	    ERROR_EMAIL_NOT_VALID : {
	        value : 2,
	        string : 'Email is not valid'
	    },
	    ERROR_BIRTHDAY_EMPTY : {
	        value : 3,
	        string : 'Birthday is Empty'
	    },
	    ERROR_BIRTHDAY_NOT_VALID : {
	        value : 4,
	        string : 'Birthday is not valid'
	    },
	    ERROR_PASSWORD_EMPTY : {
	        value : 5,
	        string : 'Password is Empty'
	    },
	    ERROR_USER_EXIST : {
	    	value : 6,
	        string : 'User allready exist'
	    },
	    ERROR_SOCIAL_ID_EMPTY : {
	    	value : 7,
	        string : 'Social id is Empty'
	    },
	    ERROR_SOCIAL_TYPE_EMPTY : {
	    	value : 8,
	        string : 'Social type is Empty'
	    },
	    ERROR_PASSWORD_TO_SMALL : {
	    	value : 9,
	        string : 'Password is to small'
	    },
	    ERROR_PSEUDO_NOT_VALID : {
	        value : 10,
	        string : 'Pseudo is not valid.'
	    },
	    ERROR_GENDER_EMPTY : {
	    	value : 11,
	        string : 'Gender is not valid.'
	    }
	})
}

exports.PROFIL = function(){
	return defineEnum({
		ERROR_USER_EMPTY : {
	        value : 0,
	        string : 'User is empty'
	    },
	    ERROR_USER_NOT_FOUND : {
	        value : 1,
	        string : 'User is not found'
	    }
	})
}

exports.UPDATE_PROFIL = function(){
	return defineEnum({
		ERROR_USER_EMPTY : {
	        value : 0,
	        string : 'User is empty'
	    },
	    ERROR_USER_NOT_FOUND : {
	        value : 1,
	        string : 'User is not found'
	    },
	    ERROR_PSEUDO_EMPTY : {
	        value : 2,
	        string : 'Pseudo need to be between 4 and 30.'
	    },
	    ERROR_EMAIL_EMPTY : {
	        value : 3,
	        string : 'Email is Empty'
	    },
	    ERROR_EMAIL_NOT_VALID : {
	        value : 4,
	        string : 'Email is not valid'
	    },
	    ERROR_BIRTHDAY_EMPTY : {
	        value : 5,
	        string : 'Birthday is Empty'
	    },
	    ERROR_BIRTHDAY_NOT_VALID : {
	        value : 6,
	        string : 'Birthday is not valid'
	    },
	    ERROR_USER_EXIST : {
	    	value : 7,
	        string : 'Email allready exist'
	    },
	    ERROR_GENDER_EMPTY : {
	    	value : 8,
	        string : 'Gender is not valid.'
	    }
	})
}

exports.FRIENDSHIP = function(){
	return defineEnum({
		ERROR_USER_EMPTY : {
	        value : 0,
	        string : 'User is empty'
	    },
	    ERROR_TO_BE_FRIEND_EMPTY : {
	        value : 1,
	        string : 'User to be friend with is empty'
	    },
	    ERROR_USER_NOT_FOUND : {
	        value : 2,
	        string : 'User is not found'
	    },
	    ERROR_FRIEND_WITH_YOUR_SELF : {
	        value : 3,
	        string : 'You can not be friend with your self'
	    },
	    ERROR_FRIENDSHIP_NOT_FRIEND : {
		    value : 4,
		    string : 'Need to be friend before any actions'
		},
		ERROR_TO_MANY_USERS : {
			value : 5,
		    string : 'There to many users from the request result'
		},
		ERROR_NO_ACTION : {
			value : 6,
		    string : 'There is no action possible'
		},



		ERROR_FRIENDSHIP_ARE_FRIEND : {
		    value : 5,
		    string : 'You are already friend'
		},
		ERROR_FRIENDSHIP_BLOCKED : {
		    value : 6,
		    string : 'Other User already blocked you'
		},
		ERROR_FRIENDSHIP_ALREADY_SEND : {
		    value : 7,
		    string : 'Friendship is already set'
		},
		ERROR_FRIENDSHIP_WAITING_OTHER_USER_ACTION : {
		    value : 8,
		    string : 'Waiting the other user action'
		},
		ERROR_FRIENDSHIP_BLOCKED : {
		    value : 9,
		    string : 'You can Block an user only if you are friend with'
		}
	})
}

exports.FILTER = function(){
	return defineEnum({
		ERROR_FILTER_NOT_FOUND : {
	        value : 0,
	        string : 'Filter is not found'
	    },
	    ERROR_FILTER_EMPTY : {
	        value : 1,
	        string : 'Filter is empty'
	    },
	    ERROR_NAME_EMPTY : {
	        value : 2,
	        string : 'Name is empty'
	    },
	    ERROR_USER_EMPTY : {
	        value : 3,
	        string : 'User is empty'
	    },
	})
}

exports.ASK_FRIENDSHIP = function(){
	return defineEnum({
		ERROR_USER_EMPTY : {
	        value : 0,
	        string : 'User is empty'
	    },
	    ERROR_NAME_EMPTY : {
	        value : 1,
	        string : 'Name is empty'
	    }
	})
}

exports.ROOM = function(){

	return defineEnum({
	    ERROR_ROOM_EMPTY : {
	        value : 0,
	        string : 'Room is empty'
	    },
	    ERROR_ROOM_INVALID : {
	        value : 1,
	        string : 'Room is invalid'
	    },
	    ERROR_USER_EMPTY : {
	        value : 2,
	        string : 'User is empty'
	    },
	    ERROR_USER_INVALID : {
	        value : 3,
	        string : 'User is invalid'
	    },
	    ERROR_CONVERSATION_WITH_YOUR_SELF : {
	        value : 4,
	        string : 'Can not open conversation with your self'
	    }
	})
}

exports.BACKUP = function(){

	return defineEnum({
	    ERROR_DATE : {
	        value : 0,
	        string : 'Date is invalide'
	    }
		})
}

//
// SOCKET IO
//

exports.IO = function(){

	return defineEnum({
	    CONNECT : {
	        value : 0,
	        string : 'CONNECT'.toLowerCase()
	    },
	    DISCONNECT : {
	        value : 1,
	        string : 'DISCONNECT'.toLowerCase()
	    },
	    UNAUTHORIZED : {
	        value : 2,
	        string : 'UNAUTHORIZED'.toLowerCase()
	    },
	    AUTHORIZED : {
	        value : 3,
	        string : 'AUTHORIZED'.toLowerCase()
	    },
	    SUBSCRIBE : {
	        value : 4,
	        string : 'SUBSCRIBE'.toLowerCase()
	    },
	    UNSUBSCRIBE : {
	        value : 5,
	        string : 'UNSUBSCRIBE'.toLowerCase()
	    },
	    MESSAGE : {
	        value : 6,
	        string : 'MESSAGE'.toLowerCase()
	    },
	    GET_LOCALIZATION : {
	        value : 7,
	        string : 'GET_LOCALIZATION'.toLowerCase()
	    },
	    SET_LOCALIZATION : {
	        value : 8,
	        string : 'SET_LOCALIZATION'.toLowerCase()
	    },
	    GET_ROOM : {
	        value : 9,
	        string : 'GET_ROOM'.toLowerCase()
	    },
	    REQUEST : {
	        value : 10,
	        string : 'REQUEST'.toLowerCase()
	    },
	    RESULT : {
	        value : 11,
	        string : 'RESULT'.toLowerCase()
	    }
	});
}

exports.AUTHORIZED = function(){

	return defineEnum({
	    ERROR_TOKEN_INVALID : {
	        value : 0,
	        string : 'Token invalid'
	    }
	})
}

exports.SET_LOCALIZATION = function(){

	return defineEnum({
	    ERROR_LNG_AND_LAT : {
	        value : 0,
	        string : 'Localization is invalid'
	    },
	    ERROR_COUNTRY : {
	        value : 1,
	        string : 'Country is invalid'
	    },
	    ERROR_STATE : {
	        value : 2,
	        string : 'State is invalid'
	    },
	    ERROR_COUNTY : {
	        value : 3,
	        string : 'County is invalid'
	    },
	    ERROR_CITY : {
	        value : 4,
	        string : 'City is invalid'
	    },
	    ERROR_INIT_ROOM : {
	        value : 5,
	        string : 'Can not found room'
	    },
	    ERROR_NUMBER_OF_ROOMS : {
	        value : 6,
	        string : 'Number of rooms are wrongs'
	    }
	});
}

exports.MESSAGE = function(){

	return defineEnum({
	    ERROR_USER_NOT_FOUND : {
	        value : 0,
	        string : 'User is not found'
	    },
	    ERROR_ROOM_NOT_FOUND : {
	        value : 1,
	        string : 'Room is not found'
	    },
	    ERROR_PATH_NOT_FOUND : {
	        value : 2,
	        string : 'Path is not found'
	    },
	    ERROR_MESSAGE_OBJ_EMPTY : {
	        value : 3,
	        string : 'Message Obj is empty'
	    },
	    ERROR_BROADCAST_EMPTY : {
	        value : 4,
	        string : 'Broadcast is empty'
	    },
	    ERROR_ROOM_TYPE_EMPTY : {
	        value : 5,
	        string : 'Room type is empty'
	    },
	    ERROR_ROOM_ID_EMPTY : {
	        value : 6,
	        string : 'Room id is empty'
	    },
	    ERROR_MESSAGE_EMPTY : {
	        value : 7,
	        string : 'Message is empty'
	    },
	    ERROR_ROOMS_EMPTY : {
	        value : 8,
	        string : 'Rooms list is empty'
	    }
	});
}

exports.RENEW_AUTH = function() {
	return defineEnum({
	    ERROR_CAN_NOT_RENEW_TOKEN : {
	        value : 0,
	        string : 'Can Not renew token'
	    }
	});
}

//
// CHAT
//

exports.CHAT = function(){

	return defineEnum({
	    CHAT_PRIVATE : {
	        value : -1,
	        string : '_CP'
	    },
	    CHAT_GENERAL : {
	        value : 0,
	        string : '_CG'
	    },
	    CHAT_TO_SELL : {
	        value : 1,
	        string : '_CTS'
	    },
	    CHAT_BROADCAST : {
	        value : 2,
	        string : '_CB'
	    }
	});
}

//
// GENERIC ENUM METHOD
//

function Enum() {
    this._enums = [];
    this._lookups = {};
}

Enum.prototype.getEnums = function() {
    return _enums;
}

Enum.prototype.forEach = function(callback){
    var length = this._enums.length;
    for (var i = 0; i < length; ++i){
        callback(this._enums[i]);
    }
}

Enum.prototype.addEnum = function(e) {
    this._enums.push(e);
}

Enum.prototype.getByName = function(name) {
    return this[name];
}

Enum.prototype.getByValue = function(field, value) {
    var lookup = this._lookups[field];
    if(lookup) {
        return lookup[value];
    } else {
        this._lookups[field] = ( lookup = {});
        var k = this._enums.length - 1;
        for(; k >= 0; --k) {
            var m = this._enums[k];
            var j = m[field];
            lookup[j] = m;
            if(j == value) {
                return m;
            }
        }
    }
    return null;
}

function defineEnum(definition) {
    var k;
    var e = new Enum();
    for(k in definition) {
        var j = definition[k];
        e[k] = j;
        e.addEnum(j)
    }
    return e;
}
