var mysql = require('mysql');
var logger = require('./logger.js');

var isDebug = false
var simpleConnection = mysql.createConnection(
			{
		        multipleStatements: true,
		        host     : 'localhost',
		        port     : 8889,
		        user     : 'root',
		        password : 'root',
		        database : 'pandaDB',
		        debug : isDebug
		    });

var pool = mysql.createPool(
			{
				multipleStatements: true,
				//NEED TO SET THIS CONNECTTION TO 1000 or less for prod
				connectionLimit : 1000,
				host     : 'localhost',
				port     : 8889,
				user     : 'root',
				password : 'root',
				database : 'pandaDB',
				debug : isDebug
			});

exports.createConnection = function() {
	return simpleConnection;
}

exports.createPool = function() {
	return pool;
}

exports.format = function(funcName, sql, inserts) {
	var sqlFormated = mysql.format(sql, inserts)

	logger.debug("");
	logger.debug("SQL *** %s", funcName);
	logger.debug("------> PARAM	: %s", JSON.stringify(inserts));
	logger.debug("------> SQL	: %s", sqlFormated);
	logger.debug("------> ORG	: %s", sql);

	return sqlFormated;
}

exports.formatSQL = function(funcName, sql) {
	var sqlFormated = mysql.format(sql, inserts)

	logger.debug("");
	logger.debug("SQL *** %s", funcName);
	logger.debug("------> SQL	: %s", sql);

	return sql;
}

exports.responseSQL = function(funcName, error, result) {
	logger.debug("");
	logger.debug("RESPONSE SQL *** %s", funcName);
	logger.debug("------> RESULT	: %s", JSON.stringify(result));
	logger.debug("------> ERROR	: %s", JSON.stringify(error));
}
