var moment = require('moment');

var responder  = require('./responder.js');
var encryptUtil  = require('./encryptUtil.js');
var logger = require('./logger.js');

var reportedSQL  = require('../models/reportedSQL.js');
var authSQL  = require('../models/authenticationSQL.js');

var enumUtil = require('./enumResponse.js');
var dbUtil = require('./dbUtil.js');

//6h
var connectionMax = 21600

//
// CHECKING APP ID
//

exports.checkAppIdFromAPI = function(req, res, callback){
	var appID = encryptUtil.appId(req.headers)

	checkAppId(appID, function(error, response){
		if (response == true) {
			callback(null, true)
		} else {
			callback(null, false)
			responder.result401App(req, res)
		}
	});
}

exports.checkAppIdFromSocketIO = function(io, socket, appID, callback){
	checkAppId(appID, function(error, response){
		if (response == true) {
			callback(null, true)
		} else {
			callback(null, false)
			responder.resultIO401App(io, socket)
		}
	});
}

function checkAppId(appID, callback){
	// if (appID == null || appID.length == 0 || appID != "123456qwerty") {
	 if (appID != "123456qwerty") {
		callback(null, false)
	} else {
		callback(null, true)
	}
}

//
// CHECKING TOKEN
//

exports.checkToken = function(req, res, callback) {
	logger.info("token" + req.headers["authorization"])
	var token = encryptUtil.authorization(req.headers)


	checkTokenWithRenewToken(token, req.db, false, function(error, response){
		if (response == true) {
			callback(null, true)
		} else {
			callback(null, false)
			responder.result401(req, res)
		}
	})
}

exports.renewToken = function(req, res, callback) {
	var token = encryptUtil.authorization(req.headers)

	checkTokenWithRenewToken(token, req.db, true, function(error, response){
		if (response == true) {
			callback(null, true)
		} else {
			callback(null, false)
			responder.result401(req, res)
		}
	})
}

exports.checkTokenSocketIO = function(io, socket, token, callback) {
	
	//TODO - NEED TO REMOVED THE CONNECTOR!!!!
	var mysql = require('./dbUtil.js');
    var pool  = mysql.createPool();
	
	checkTokenWithRenewToken(token, pool, false, function(error, response){
		if (response == true) {
			pool.getConnection(function(err, connection) {
                connection.query(authSQL.updateAuthenSocket(token, socket.id), function(err, results) {
                	dbUtil.responseSQL("updateAuthenSocket", err, results)

                    connection.release();
                    if (err) {
                        callback(null, false)
                        responder.resultIO401App(io, socket, enumUtil.API().TOKEN, err)
                    } else if (results.changedRows == 0) {
						callback(null, false)
						responder.resultIO401App(io, socket)
                    } else {
                    	callback(null, true)
                    }
                });
            });
		} else {
			callback(null, false)
			responder.resultIO401App(io, socket)
		}
	})
}

function checkTokenWithRenewToken(token, db, isRenewToken, callback) {
	//Check if there is a token
	if (token != null && token.length > 0) {

		var mArray = encryptUtil.genereteArray(token);
		logger.debug(mArray)
		//Check if the token is made by 4 items
		if (mArray.length === 4) {
			var date = new Date(mArray[3]);

			//Force to set time to 0 when is renew token
			var currentDate = isRenewToken == false ? moment().format("X") - moment(date).format("X") : 0

			if (date != null) {

				//check if the use need to update his token
				if (currentDate <= connectionMax) {

					db.getConnection(function(err, connection) {
                        connection.query(authSQL.findAuthenNorReportedByUserId(mArray[0]), function(err, authRows) {
                            dbUtil.responseSQL("findAuthenNorReportedByUserId", err, authRows)

                            //If more or less of 1 - something wrong
							if (authRows.length == 1 && authRows[0]["is_reported"] === "false" && authRows[0]["token"] === token) {
								callback(null, true)
							} else {
								logger.debug("line 141")
								callback(null, false)
							}

							connection.release();
                        });
                    });
				} else {
					logger.debug("line 147")
					callback(null, false)
				}
			} else {
				logger.debug("line 151")
				callback(null, false)
			}
		} else {
			logger.debug("line 155")
			callback(null, false)
		}
	} else {
		logger.debug("line 159")
		callback(null, false)
	}
}