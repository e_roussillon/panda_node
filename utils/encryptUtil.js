var _this = this;
var logger = require('./logger.js');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'GECD9Y9-a=-*xQJrWgV&bTS2XHR4hkW+du_KEJkBxvCk?JJ%tB';
var splitSimbol = "|"


function encrypt(text){
  try {
    var cipher = crypto.createCipher(algorithm,password)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
  }
  catch(err) {
      return ""
  }
}
 
function decrypt(text){
  try {
      var decipher = crypto.createDecipher(algorithm, password)
      var dec = decipher.update(text,'hex','utf8')
      dec += decipher.final('utf8');
      return dec;
  }
  catch(err) {
      return ""
  }
}

function getAuthorization(headers) {
  var token = headers["authorization"]
  if (token == null || token <= 7) {
    return ""
  } else if (token.search('Bearer ') > -1){
    return token.replace('Bearer ', '');
  } else {
    return ""
  }
}

//
//EXPORT METHOD
//

exports.authorization = function(headers) {
  var token = getAuthorization(headers)
  if (token.length == 0) {
    return ""
  } else {
    return token
  }
}

exports.userId = function(headers) {
  var token = getAuthorization(headers)
  logger.info("token : "+ token)
  if (token.length == 0) {
    return ""
  } else { 
    var mArray = decrypt(token).split(splitSimbol);
    logger.info("token array: "+ mArray)
    if (mArray == null || mArray.length != 4) {
      return ""
    } else {
      return mArray[0]
    }
  }              
}

exports.token = function(headers) {
  var token = headers["token"]
  if (token == null) {
    return ""
  } else {
    return token
  }
}

exports.deviceId = function(headers) {
  var deviceId = headers["device-id"]
  if (deviceId == null) {
    return ""
  } else {
    return deviceId
  }
}

exports.appId = function(headers) {
  var appId = headers["app-id"]
  // logger.info(headers["app-id"])
  // logger.info(headers)
  if (appId == null) {
    return ""
  } else {
    return appId
  }
}

exports.googleId = function(headers) {
  var googleId = headers["google-id"]
  if (googleId == null) {
    return ""
  } else {
    return googleId
  }
}

exports.password = function(headers) {
  var password = headers["password"]
  if (password == null) {
    return ""
  } else {
    return password
  }
}

exports.genereteArray = function(token) {
  return decrypt(token).split(splitSimbol);
}

exports.genereteToken = function(userId, platformDevice, deviceId, date) {

  if (userId == null || userId.length == 0) {
    return null
  } else if (platformDevice == null || platformDevice.length == 0) {
    return null
  } else if (deviceId == null || deviceId.length == 0) {
    return null
  } else if (date == null || date.length == 0) {
    return null
  } else {
    return encrypt(userId + splitSimbol + platformDevice + splitSimbol + deviceId + splitSimbol + date)
  }
}

exports.renewToken = function(token) {
  var mArray = decrypt(token).split(splitSimbol)
  return _this.genereteToken(mArray[0], mArray[1], mArray[2], new Date().toISOString())
}