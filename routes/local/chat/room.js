var dbUtil = require('../../../utils/dbUtil.js');
var logger = require('../../../utils/logger.js');

var publicRoomSQL = require('../../../models/publicRoomSQL.js');
var localizationRoomSQL = require('../../../models/publicRoomLocalizedSQL.js');
var authSQL = require('../../../models/authenticationSQL.js');
var enumUtil = require('../../../utils/enumResponse.js');


exports.Localization = function (longitudeField, latitudeField, countryField, stateField, countyField, cityField, neighborhoodField, streetField, streetNumberField) { 
    
    if (neighborhoodField == null || neighborhoodField.length == 0) {
        neighborhoodField = "none"
    } 

    if (streetField == null || streetField.length == 0) {
        streetField = "none"
    } 

    if (streetNumberField == null || streetNumberField.length == 0) {
        streetNumberField = "none"
    } 

    var item = {
                longitude: longitudeField, 
                latitude: latitudeField, 
                country: countryField, 
                state: stateField,
                county: countyField,
                city: cityField,
                neighborhood: neighborhoodField,
                street: streetField,
                streetNumber: streetNumberField
                }
    return item;
};

exports.Message = function (isBroadcast, isRoomGeneral, room, message) { 
    this.isBroadcast = isBroadcast;
    this.isRoomGeneral = isRoomGeneral;
    this.room = room;
    this.message = message;
};

exports.MessageToRoom = function (isBroadcast, room, message) { 
    var array = room.split("_");
    var type = "_"+array[1] 

    if (type === enumUtil.CHAT().CHAT_GENERAL.string) {
        type = enumUtil.CHAT().CHAT_GENERAL.value
    } else if (type === enumUtil.CHAT().CHAT_TO_SELL.string) {
        type = enumUtil.CHAT().CHAT_TO_SELL.value
    } else {
        type = enumUtil.CHAT().CHAT_PRIVATE.value
    }

    this.isBroadcast = isBroadcast;
    this.roomType = type
    this.room = array[0];
    this.message = message;
};

exports.RoomTypeIntIsValide = function (type) { 

    if (type == null) {
        return null
    } else if (type === enumUtil.CHAT().CHAT_GENERAL.value) {
        return enumUtil.CHAT().CHAT_GENERAL.value
    } else if (type === enumUtil.CHAT().CHAT_TO_SELL.value) {
        return enumUtil.CHAT().CHAT_TO_SELL.value
    } else if (type === enumUtil.CHAT().CHAT_PRIVATE.value) {
        return enumUtil.CHAT().CHAT_PRIVATE.value
    } else {
        return null
    }

}


//
// ADD NEW ROOM
//

exports.setupRoomsPath = function (localization, callback) { 
 
    var connection = dbUtil.createConnection();
    // connection.connect();

    connection.query(localizationRoomSQL.selectPublicRoomLocalizedByLatAndLong(localization.latitude, localization.longitude), function(err, results) {
        dbUtil.responseSQL("selectPublicRoomLocalizedByLatAndLong", err, results)

        if (err) {
            logger.debug("SOCKET ---> callback error");
            callback(err, null)
        } else {

            //No Room With this localization
            if (results.length == 0) {
                var parentIdField = "1"
                //Country
                connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomIdAndName(parentIdField, localization.country+"_"+parentIdField+"_country"), function(err, results) {
                    dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomIdAndName", err, results)

                    //Error with DB
                    if (err) {
                        logger.debug("SOCKET ---> callback error line 36");
                        callback(err, null)
                    } else {
                        if (results.length == 0) {
                            logger.debug("SOCKET ---> The country '%s' DOES NOT exist", localization.country);
                            //Check it country already existe
                            connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomId(parentIdField), function(err, results) {
                                dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomId", err, results)

                                addRoom(err, results, connection, parentIdField, localization.country, "country", function(err, results) {

                                    if (err) {
                                        callback(err, null)
                                    } else {
                                        //Create Country
                                        logger.debug("SOCKET ---> new country '%s' with id : %s", localization.country, results[results.length-2].insertId);
                                        checkRoomState(connection, results[results.length-2].insertId, localization, callback)
                                    }

                                })
                            })
                        } else {
                            //Get Country
                            logger.debug("SOCKET ---> The country '%s' DOES exist - id : %s", localization.country, results[0]["_id"]);
                            checkRoomState(connection, results[0]["_id"], localization, callback)
                        }
                    }
                })
            } else {
                //GPS exist get the rooms path
                connection.query(publicRoomSQL.selectPathFromPublicRoomIdToRoot(results[0]["public_room_id"]), function(err, results) {
                    dbUtil.responseSQL("selectPathFromPublicRoomIdToRoot", err, results)

                    if (err) {
                        callback(err, null)
                    } else {
                        callback(null, results)
                    }
                });
            }
        }
    });
};

function checkRoomState(connection, parentIdField, localization, callback) {

    //State
    connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomIdAndName(parentIdField, localization.state+"_"+parentIdField+"_state"), function(err, results) {
        dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomIdAndName", err, results)

        if (err) {
            callback(err, null)
        } else {
            if (results.length == 0) {
                logger.debug("SOCKET ---> The state '%s' DOES NOT exist", localization.state);
                //Check it state already existe
                connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomId(parentIdField), function(err, results) {
                    dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomId", err, results)

                    addRoom(err, results, connection, parentIdField, localization.state, "state", function(err, results) {

                        if (err) {
                            callback(err, null)
                        } else {
                            //Create state
                            logger.debug("SOCKET ---> new state '%s' with id : %s", localization.state, results[results.length-2].insertId);
                            checkRoomCounty(connection, results[results.length-2].insertId, localization, callback)
                        }

                    })
                })
            } else {
                //Get State
                logger.debug("SOCKET ---> The state '%s' DOES exist - id : %s", localization.state, results[0]["_id"]);
                checkRoomCounty(connection, results[0]["_id"], localization, callback)
            }
        }
    })
}

function checkRoomCounty(connection, parentIdField, localization, callback) {

    // County
    connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomIdAndName(parentIdField, localization.county+"_"+parentIdField+"_county"), function(err, results) {
        dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomIdAndName", err, results)

        if (err) {
            callback(err, null)
        } else {
            if (results.length == 0) {
                logger.debug("SOCKET ---> The county '%s' DOES NOT exist", localization.county);
                //Check it county already existe
                connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomId(parentIdField), function(err, results) {
                    dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomId", err, results)

                    addRoom(err, results, connection, parentIdField, localization.county, "county", function(err, results) {

                        if (err) {
                            callback(err, null)
                        } else {
                            //Create county
                            logger.debug("SOCKET ---> new county '%s' with id : %s", localization.county, results[results.length-2].insertId);
                            checkRoomCity(connection, results[results.length-2].insertId, localization, callback)
                        }

                    })
                })
            } else {
                //Get county
                logger.debug("SOCKET ---> The county '%s' DOES exist - id : %s", localization.county, results[0]["_id"]);
                checkRoomCity(connection, results[0]["_id"], localization, callback)
            }
        }
    })
}

function checkRoomCity(connection, parentIdField, localization, callback) {
    //City
    connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomIdAndName(parentIdField, localization.city+"_"+parentIdField+"_city"), function(err, results) {
        dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomIdAndName", err, results)

        if (err) {
            callback(err, null)
        } else {
            if (results.length == 0) {
                logger.debug("SOCKET ---> The city '%s' DOES NOT exist", localization.city);
                //Check it city already existe
                connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomId(parentIdField), function(err, results) {
                    dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomId", err, results)

                    addRoom(err, results, connection, parentIdField, localization.city, "city", function(err, results) {

                        if (err) {
                            callback(err, null)
                        } else {
                            //Create City
                            logger.debug("SOCKET ---> new city '%s' with id : %s", localization.city, results[results.length-2].insertId);
                            checkRoomNeighbour(connection, results[results.length-2].insertId, localization, callback)
                        }

                    })
                })
            } else {
                //Get City
                logger.debug("SOCKET ---> The city '%s' DOES exist - id : %s", localization.city, results[0]["_id"]);
                checkRoomNeighbour(connection, results[0]["_id"], localization, callback)
            }
        }
    })
}

function checkRoomNeighbour(connection, parentIdField, localization, callback) {
    //neighborhood
    connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomIdAndName(parentIdField, localization.neighborhood+"_"+parentIdField+"_neighborhood"), function(err, results) {
        dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomIdAndName", err, results)

        if (err) {
            callback(err, null)
        } else {
            if (results.length == 0) {
                logger.debug("SOCKET ---> The neighborhood '%s' DOES NOT exist", localization.neighborhood);
                //Check it neighborhood already existe
                connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomId(parentIdField), function(err, results) {
                    dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomId", err, results)

                    addRoom(err, results, connection, parentIdField, localization.neighborhood, "neighborhood", function(err, results) {

                        if (err) {
                            callback(err, null)
                        } else {
                            //Create neighborhood
                            logger.debug("SOCKET ---> new neighborhood '%s' with id : %s", localization.neighborhood, results[results.length-2].insertId);
                            checkRoomStreet(connection, results[results.length-2].insertId, localization, callback)
                        }

                    })
                })
            } else {
                //Get  neighborhood
                logger.debug("SOCKET ---> The neighborhood '%s' DOES exist - id : %s", localization.neighborhood, results[0]["_id"]);
                checkRoomStreet(connection, results[0]["_id"], localization, callback)
            }
        }
    })
}

function checkRoomStreet(connection, parentIdField, localization, callback) {
    //Street
    connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomIdAndName(parentIdField, localization.street+"_"+parentIdField+"_street"), function(err, results) {
        dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomIdAndName", err, results)

        if (err) {
            callback(err, null)
        } else {
            if (results.length == 0) {
                logger.debug("SOCKET ---> The street '%s' DOES NOT exist", localization.street);
                //Check it Street already existe
                connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomId(parentIdField), function(err, results) {
                    dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomId", err, results)

                    addRoom(err, results, connection, parentIdField, localization.street, "street", function(err, results) {

                        if (err) {
                            callback(err, null)
                        } else {
                            //Create Street
                            logger.debug("SOCKET ---> new street '%s' with id : %s", localization.street, results[results.length-2].insertId);
                            checkRoomStreetNumber(connection, results[results.length-2].insertId, localization, callback)
                        }

                    })
                })
            } else {
                //Get Street
                logger.debug("SOCKET ---> The street '%s' DOES exist - id : %s", localization.street, results[0]["_id"]);
                checkRoomStreetNumber(connection, results[0]["_id"], localization, callback)
            }
        }
    })
}

function checkRoomStreetNumber(connection, parentIdField, localization, callback) {
    //Street Number
    connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomIdAndName(parentIdField, localization.streetNumber+"_"+parentIdField+"_streetnumber"), function(err, results) {
        dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomIdAndName", err, results)

        if (err) {
            callback(err, null)
        } else {
            if (results.length == 0) {
                logger.debug("SOCKET ---> The streetNumber '%s' DOES NOT exist", localization.streetNumber);
                //Check it Street Number already existe
                connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomId(parentIdField), function(err, results) {
                    dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomId", err, results)

                    addRoom(err, results, connection, parentIdField, localization.streetNumber, "streetNumber", function(err, results) {

                        if (err) {
                            callback(err, null)
                        } else {
                            //Create Street Number
                            logger.debug("SOCKET ---> new streetNumber '%s' with id : %s", localization.streetNumber, results[results.length-2].insertId);
                            saveRoomWithLocalisation(connection, results[results.length-2].insertId, localization, callback)
                        }
                    })
                })
            } else {
                //Get Street Number
                logger.debug("SOCKET ---> The streetNumber '%s' DOES exist - id : %s", localization.streetNumber, results[0]["_id"]);
                saveRoomWithLocalisation(connection, results[0]["_id"], localization, callback)
            }
        }
    })
}

function saveRoomWithLocalisation(connection, roomIdField, localization, callback) {
    //GPS exist get the rooms path
    connection.query(localizationRoomSQL.insertPublicRoomLocalized(roomIdField, localization.latitude, localization.longitude), function(err, results) {
        dbUtil.responseSQL("insertPublicRoomLocalized", err, results)

        if (err) {
            callback(err, null)
        } else {
            connection.query(publicRoomSQL.selectPathFromPublicRoomIdToRoot(roomIdField), function(err, results) {
                dbUtil.responseSQL("selectPathFromPublicRoomIdToRoot", err, results)

                if (err || results.length == 0) {
                    callback(err, null)
                } else {
                    callback(null, results)
                }
            });
        }
    });
}

//
// Generic method to create a Room
//

function addRoom(err, results, connection, parentIdField, nameField, typeField, callback) {
    if (err) {
        callback(err, null)
    } else {
        addFirstLeaf(connection, parentIdField, nameField, typeField, callback)
        //
        //BUG WITH THIS CODE NEED TO SEE LATER
        //
        // if (results.length == 0) {
        //     logger.debug("SOCKET ---> Node DOES NOT have son");
        //     addFirstLeaf(connection, parentIdField, nameField, typeField, callback)                            

        //     // addNewLeaf(connection, parentIdField, nameField, typeField, callback)
        // } else {
        //     logger.debug("SOCKET ---> Node DOES have son(s)");
        //     // addFirstLeaf(connection, parentIdField, nameField, typeField, callback)                            
        //     addFirstLeaf(connection, parentIdField, nameField, typeField, callback)                            

        //     // addNewLeaf(connection, parentIdField, nameField, typeField, callback)
        // }
    }
}

function addFirstLeaf(connection, parentIdField, nameField, typeField, callback) {
	connection.query(publicRoomSQL.insertPublicRoomFirstLeaf(parentIdField, nameField, typeField), function(err, results) {
        dbUtil.responseSQL("insertPublicRoomFirstLeaf", err, results)
        callback(err, results)
	});
}

function addNewLeaf(connection, parentIdField, nameField, typeField, callback) {
	connection.query(publicRoomSQL.insertPublicRoomNewLeaf(parentIdField, nameField, typeField), function(err, results) {
		dbUtil.responseSQL("insertPublicRoomNewLeaf", err, results)
        callback(err, results)
	});
}

//
// Subscription or Unsubscribe ROOM TO USER
//
// type = -1 (CP) ChatPrivate
// type = 0 (CG) ChatGeneral
// type = 0 (CTS) ChatToSell

exports.addRoomToUser = function(socketField, roomsBroadcastField, roomField, callback) {
    dbUtil.createPool().getConnection(function(err, connection) {
        connection.query(authSQL.updateAuthenSocketRoom(socketField.id, roomField["_id"]), function(err, results) {
            dbUtil.responseSQL("updateAuthenSocketRoom", err, results)

            connection.release();

            if (err) {
                callback(err, null)
            } else {

                if (results.length == 0) {
                    logger.debug("SOCKET ---> CAN NOT joining Room : %s (%s and %s) with Socket : ", roomField["_id"], enumUtil.CHAT().CHAT_GENERAL.string, enumUtil.CHAT().CHAT_TO_SELL.string, socketField.id);
                    callback(true, null)
                } else {
                    logger.debug("SOCKET ---> joining Room : %s (%s and %s) with Socket : ", roomField["_id"], enumUtil.CHAT().CHAT_GENERAL.string, enumUtil.CHAT().CHAT_TO_SELL.string, socketField.id);
                    socketField.join(roomField["_id"]+enumUtil.CHAT().CHAT_GENERAL.string);
                    socketField.join(roomField["_id"]+enumUtil.CHAT().CHAT_TO_SELL.string);

                    roomsBroadcastField.forEach(function(room){
                        socketField.join(room["_id"]+enumUtil.CHAT().CHAT_BROADCAST.string);
                        logger.debug("SOCKET ---> joining Room : %s (%s) with Socket : ", room["_id"], enumUtil.CHAT().CHAT_BROADCAST.string, socketField.id);
                    });

                    callback(null, "ok")
                }
            }
        });
    });
}

exports.removeRoomFromUser = function(socketField, roomField, callback) {
    dbUtil.createPool().getConnection(function(err, connection) {
        connection.query(authSQL.updateAuthenSocketRoom(socketField.id, ""), function(err, results) {
            dbUtil.responseSQL("updateAuthenSocketRoom", err, results)

            connection.release();

            if (err) {
                callback(err, null)
            } else {
                logger.debug("SOCKET ---> leave Room : %s (%s and %s) with Socket : ", roomField, enumUtil.CHAT().CHAT_GENERAL.string, enumUtil.CHAT().CHAT_TO_SELL.string, socketField.id);
                socketField.join(roomField+enumUtil.CHAT().CHAT_GENERAL.string);
                socketField.join(roomField+enumUtil.CHAT().CHAT_TO_SELL.string);
                callback(null, "ok")
            }
        });
    })
}

//
// DELETE ROOM
//

exports.deleteRoom = function (parentField, withChild, callback) { 
    var connection = dbUtil.createConnection();

    connection.query(publicRoomSQL.selectImmediateNodesBelowPublicRoomId(parentField), function(err, results) {
		dbUtil.responseSQL("selectImmediateNodesBelowPublicRoomId", err, results)

    	if (err) {
    		callback(err, null)
    	} else {

    		if (results.length == 0) {
    			removeLeaf(connection, parentField, function(err, results) {
    				callback(err, results)
    			})
    		} else {

    			if (withChild == true) {
    				removeNodeWithLeaf(connection, parentField, function(err, results) {
	    				callback(err, results)
	    			});
    			} else {
    				removeNodeWithoutLeaf(connection, parentField, function(err, results) {
	    				callback(err, results)
	    			});
    			}
    		}
    	}
	});
};

function removeLeaf(connection, parentField, callback) {
	connection.query(publicRoomSQL.deletePublicRoomLeaf(parentField), function(err, results) {
        dbUtil.responseSQL("deletePublicRoomLeaf", err, results)
		callback(err, results)
	});
}

function removeNodeWithLeaf(connection, parentField, callback) {
	connection.query(publicRoomSQL.deletePublicRoomNodeWithLeafs(parentField), function(err, results) {
        dbUtil.responseSQL("deletePublicRoomNodeWithLeafs", err, results)
		callback(err, results)
	});
}

function removeNodeWithoutLeaf(connection, parentField, callback) {
	connection.query(publicRoomSQL.deletePublicRoomNodeWithoutLeafs(parentField), function(err, results) {
        dbUtil.responseSQL("deletePublicRoomNodeWithoutLeafs", err, results)
		callback(err, results)
	});
}
