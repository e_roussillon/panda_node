var responder = require('../../../utils/responder.js');
var logger = require('../../../utils/logger.js');

var privateMessageQL = require('../../../models/privateMessageSQL');
var privateRoomSQL = require('../../../models/privateRoomSQL');
var userSQL = require('../../../models/userSQL');

var dbUtil = require('../../../utils/dbUtil.js');
var enumUtil = require('../../../utils/enumResponse.js');


exports.createConversation = function(req, res, connection, tokenUser, userIdField, message, callback) {
  connection.query(privateRoomSQL.selectPrivateRoomWithFriendIdByUserId(tokenUser, userIdField), function(err, results) {
    dbUtil.responseSQL("selectPrivateRoomWithFriendIdByUserId", err, results)

    if (err) {
      if (callback != null) {
        callback(err, false)
      } else {
        responder.result406(req, res, enumUtil.API().ROOM, err)
        connection.release();
      }
    } else {
      if (results.length > 0) {
        if (message == null || message.length == 0) {
          if (callback != null) {
            callback(null, true)
          } else {
            connection.release();
            responder.result200(req, res, enumUtil.API().ROOM, results[0])
          }
        } else {
          sendMessage(req, res, connection, results, tokenUser, message, callback)
        }
      } else {
        connection.query(userSQL.findUserById(userIdField), function(err, results) {

          if (err) {
            if (callback != null) {
              callback(err, false)
            } else {
              responder.result406(req, res, enumUtil.API().ROOM, err)
              connection.release();
            }
          } else {
            if (results.length == 0) {
              if (callback != null) {
                callback(enumUtil.ROOM().ERROR_USER_INVALID, false)
              } else {
                responder.result403(req, res, enumUtil.API().ROOM, enumUtil.ROOM().ERROR_USER_INVALID)
                connection.release();
              }
            } else {
              connection.query(privateRoomSQL.insertPrivateRoom(tokenUser, userIdField), function(err, results) {
                dbUtil.responseSQL("insertPrivateRoom", err, results)

                if (err) {
                  if (callback != null) {
                    callback(err, false)
                  } else {
                    responder.result406(req, res, enumUtil.API().ROOM, err)
                    connection.release();
                  }
                } else {
                  connection.query(privateRoomSQL.selectPrivateRoomWithFriendIdByUserId(tokenUser, userIdField), function(err, results) {
                    dbUtil.responseSQL("selectPrivateRoomWithFriendIdByUserId", err, results)

                    if (err) {
                      if (callback != null) {
                        callback(err, false)
                      } else {
                        responder.result406(req, res, enumUtil.API().ROOM, err)
                        connection.release();
                      }
                    } else {
                      if (message == null || message.length == 0) {
                        if (callback != null) {
                          callback(null, true)
                        } else {
                          connection.release();
                          responder.result200(req, res, enumUtil.API().ROOM, results[0])
                        }
                      } else {
                        sendMessage(req, res, connection, results, tokenUser, message, callback)
                      }
                    }
                  })
                }
              })
            }
          }
        })
      }
    }
  });
}

function sendMessage(req, res, connection, results, tokenUser, message, callback) {
  connection.query(privateMessageQL.insertPrivateMessage(results[0].room_id, tokenUser, message, -1), function(err, resultsMessage) {
    dbUtil.responseSQL("insertPrivateMessage", err, resultsMessage)

    if (err) {
      if (callback != null) {
        callback(err, false)
      } else {
        responder.result406(req, res, enumUtil.API().ROOM, err)
        connection.release();
      }
    } else {
      //we just send back the room not the message
      if (callback != null) {
        callback(null, true)
      } else {
        responder.result200(req, res, enumUtil.API().ROOM, results[0])
        connection.release();
      }
    }

  })
}
