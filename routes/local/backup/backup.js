var async = require('async');

var backupSQL = require('../../../models/backupSQL.js');
var responder = require('../../../utils/responder.js');
var enumUtil = require('../../../utils/enumResponse.js');
var dbUtil = require('../../../utils/dbUtil.js');
var dateUtil = require('../../../utils/dateUtil.js');

var logger = require('../../../utils/logger.js');

exports.loadBackup = function(req, res, connection, userIdField, lastUpdateField) {
  if (!dateUtil.validateDate(lastUpdateField)) {
    lastUpdateField = null
  }

  var backupResult = {
    "users" : [],
    "friendships" : [],
    "privateRooms" : [],
    "privateRoomUsers" : [],
    "privateMessage" : [],
    "privateFilters" : [],
    "datas" : []
  };
  async.parallel([
    function(callback) {
      //backupCurrentUser
      connection.query(backupSQL.backupCurrentUsers(userIdField), function(err, result) {
          dbUtil.responseSQL("backupUsers", err, result)
          if (err) return callback(err);
          backupResult.users = changeObjectToInsert("user", result)
          callback();
      });
    },
    function(callback) {
      //backupUsers
      connection.query(backupSQL.backupUsers(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupUsers", err, result)
          if (err) return callback(err);

          if (backupResult.users == null || backupResult.users.length == 0) {
            backupResult.users = changeObjectToInsert("user", result)
          } else {
            backupResult.users.concat(changeObjectToInsert("user", result))
          }

          callback();
      });
    },
    function(callback) {
      //backupFriendships
      connection.query(backupSQL.backupFriendships(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupFriendships", err, result)
          if (err) return callback(err);
          backupResult.friendships = changeObjectToInsert("friendship", result)
          callback();
      });
    },
    //backupPrivateRooms
    function(callback) {
      connection.query(backupSQL.backupPrivateRooms(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupPrivateRooms", err, result)
          if (err) return callback(err);
          backupResult.privateRooms = changeObjectToInsert("private_room", result)
          callback();
      });
    },
    //backupPrivateRoomUser
    function(callback) {
      connection.query(backupSQL.backupPrivateRoomUser(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupPrivateRoomUser", err, result)
          if (err) return callback(err);
          backupResult.privateRoomUsers = changeObjectToInsert("private_room_user", result)
          callback();
      });
    },
    //backupPrivateMessage
    function(callback) {
      connection.query(backupSQL.backupPrivateMessage(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupPrivateMessage", err, result)
          if (err) return callback(err);
          backupResult.privateMessage = changeObjectToInsert("private_message", result)
          callback();
      });
    },
    // backupFilters
    function(callback) {
      connection.query(backupSQL.backupFilters(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupFilters", err, result)
          if (err) return callback(err);
          backupResult.privateFilters = changeObjectToInsert("user_filter", result)
          callback();
      });
    },
    //backupMessageData
    function(callback) {
      connection.query(backupSQL.backupMessageData(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupMessageData", err, result)
          if (err) return callback(err);
          backupResult.datas = changeObjectToInsert("data", result)
          callback();
      });
    },
    //backupRoomData
    function(callback) {
      connection.query(backupSQL.backupRoomData(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupRoomData", err, result)
          if (err) return callback(err);

          if (backupResult.datas == null || backupResult.datas.length == 0) {
            backupResult.datas = changeObjectToInsert("data", result)
          } else {
            backupResult.datas.concat(changeObjectToInsert("data", result))
          }

          callback();
      });
    },
    //backupUserData
    function(callback) {
      connection.query(backupSQL.backupUserData(userIdField, lastUpdateField), function(err, result) {
          dbUtil.responseSQL("backupUserData", err, result)
          if (err) return callback(err);

          if (backupResult.datas == null || backupResult.datas.length == 0) {
            backupResult.datas = changeObjectToInsert("data", result)
          } else {
            backupResult.datas.concat(changeObjectToInsert("data", result))
          }

          callback();
      });
    },
    //backupUserCurrentData
    function(callback) {
      connection.query(backupSQL.backupUserCurrentData(userIdField), function(err, result) {
          dbUtil.responseSQL("backupUserData", err, result)
          if (err) return callback(err);

          if (backupResult.datas == null || backupResult.datas.length == 0) {
            backupResult.datas = changeObjectToInsert("data", result)
          } else {
            backupResult.datas.concat(changeObjectToInsert("data", result))
          }

          callback();
      });
    }
    ], function(err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) {
            responder.result406(req, res, enumUtil.API().BACKUP, err)
        } else {
          var list = []

          logger.info(backupResult)

          if (backupResult.users.length > 0) {
            list.push(backupResult.users)
          }

          if (backupResult.friendships.length > 0) {
            list.push(backupResult.friendships)
          }

          if (backupResult.privateRooms.length > 0) {
            list.push(backupResult.privateRooms)
          }

          if (backupResult.privateRoomUsers.length > 0) {
            list.push(backupResult.privateRoomUsers)
          }

          if (backupResult.privateMessage.length > 0) {
            list.push(backupResult.privateMessage)
          }

          if (backupResult.privateFilters.length > 0) {
            list.push(backupResult.privateFilters)
          }

          if (backupResult.datas.length > 0) {
            list.push(backupResult.datas)
          }

          var result = {"data" : new Date().toISOString(),
                        "list" : list }

          responder.result200(req, res, enumUtil.API().BACKUP, result)
        }
        connection.release();
    });
}

function changeObjectToInsert(table, result) {
  if (result.length == 0) {
    return ""
  }
  var keys = Object.keys(result[0])
  var keysString = columnsInString(table, keys)
  var insert = "INSERT OR REPLACE INTO `"+ table + "` " + keysString +" VALUES "

  for (var i = 0; i < result.length; i++) {
    if (i > 0) {
      insert += ", "
    }

    insert += valuesInString(keys, result[i])
  }

  return insert
}

function columnsInString(table, keys) {
  var keysString = ""
  for (var i = 0; i < keys.length; i++) {
    if (i > 0) {
      keysString += ", "
    }

    if (keys[i] == "_id") {
      keysString += "`"+ table + keys[i] +"`"
    } else {
      keysString += "`"+ keys[i] +"`"
    }
  }
  return "(" + keysString + ")"
}

function valuesInString(keys, values) {
  var valuesString = ""
  for (var i = 0; i < keys.length; i++) {
    if (i > 0) {
      valuesString += ", "
    }

    var value = values[keys[i]]
    if (value instanceof Date) {
      valuesString += "'"+ value.toISOString() +"'"
    } else if (typeof value == 'string') {
      valuesString += "'"+ value.replace("'", "''") +"'"
    } else {
      valuesString += "'"+ value +"'"
    }
  }

  return "(" + valuesString + ")"
}
