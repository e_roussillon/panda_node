var responder = require('../../../utils/responder.js');
var encryptUtil = require('../../../utils/encryptUtil.js');
var enumUtil = require('../../../utils/enumResponse.js');
var dbUtil = require('../../../utils/dbUtil.js');
var logger = require('../../../utils/logger.js');

var userSQL = require('../../../models/userSQL.js');
var friendSQL = require('../../../models/friendshipSQL.js');

var createRoom = require('../chat/createRoom.js')

var userId = -1

exports.FriendshipList = function(friendRow, userId) {

  this.pendings = [];
  this.friends = [];
  this.blockeds = [];

  for (var i = 0; i < friendRow.length; i++) {
    var item = friendRow[i]

    item.last_action = item.last_action == userId ? true : false

    if (item.status == friendSQL.friendshipFriend() || (item.status == friendSQL.friendshipRefusedAfterBeFriend() && item.last_action == false)) {
      this.friends.push(item)
    } else if (item.status == friendSQL.friendshipPending() || (item.status == friendSQL.friendshipRefusedBeforBeFriend() && item.last_action == false)) {
      this.pendings.push(item)
    } else if (item.status == friendSQL.friendshipBlocked() && item.last_action == false) {
      this.blockeds.push(item)
    }
  }
};

exports.ProfileFriend = function(userField, filtersField, friendshipField) {
  this.user = userField;
  this.filters = filtersField;

  if (friendshipField != null) {
    this.friendship = friendshipField
  } else {
    this.friendship = {
      "user_id": -1,
      "to_user_id": -1,
      "status": friendSQL.friendshipNotFiend(),
      "last_action": -1
    }
  }
}

exports.updateFriendship = function(req, res, connection, pendingFriendId, updateToStatus) {
  var userId = encryptUtil.userId(req.headers)
  var message = req.body.message

  if (userId === pendingFriendId) {
    connection.release();
    callback(null, false)
    responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_FRIEND_WITH_YOUR_SELF)
  } else {

    connection.query(userSQL.findUserById(pendingFriendId), function(err, results) {
      dbUtil.responseSQL("findUserById", err, results)

      if (err) {
        connection.release();
        callback(null, false)
        responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
      } else {

        if (results.length == 1) {
          var date = new Date().toISOString()

          connection.query(friendSQL.findFriendshipByUserIdOrToUserId(userId, pendingFriendId), function(err, friendRow) {
            dbUtil.responseSQL("findFriendshipByUserIdOrToUserId", err, friendRow)

            if (err) {
              responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
              connection.release();
            } else if (friendRow.length == 0) {
              createFriendship(req, res, connection, userId, pendingFriendId, updateToStatus, message)
            } else if (friendRow.length == 1) {
              checkAndUpdateFriendship(req, res, connection, userId, pendingFriendId, updateToStatus, friendRow[0], message)
            } else {
              connection.release();
              responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_TO_MANY_USERS)
            }
          });
        } else {
          connection.release();
          responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_NOT_FOUND)
        }


      }
    });
  }

  function createFriendship(req, res, connection, userId, pendingFriendId, updateToStatus, message) {
    if (updateToStatus == friendSQL.friendshipPending()) {
      connection.query(friendSQL.insertFriendship(userId, pendingFriendId, updateToStatus, userId), function(err, results) {
        dbUtil.responseSQL("insertFriendship", err, results)

        if (err) {
          responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
          connection.release();
        } else {
          if (friendSQL.friendshipPending() == updateToStatus) {
            createRoom.createConversation(req, res, connection, userId, pendingFriendId, message, function(err, results) {
              returnReloadedFriendship(req, res, connection, pendingFriendId)
            })
          } else {
            returnReloadedFriendship(req, res, connection, pendingFriendId)
          }
        }
      });
    } else {
      responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_FRIENDSHIP_NOT_FRIEND)
      connection.release();
    }
  }

  function checkAndUpdateFriendship(req, res, connection, userId, pendingFriendId, updateToStatus, friendRow, message) {
    logger.info("METHOD checkAndUpdateFriendship - userId : " + userId + " pendingFriendId : " + pendingFriendId + " status : " + updateToStatus + " friendRow : " + friendRow)

    // exports.friendshipNotFiend = function() { return 0}
    // exports.friendshipPending = function() { return 1}
    // exports.friendshipCancelRequest = function() { return 2}
    // exports.friendshipRefusedBeforeBeFriend = function() { return 3}
    // exports.friendshipFriend = function() { return 4}
    // exports.friendshipRefusedAfterBeFriend = function() { return 5}
    // exports.friendshipBlocked = function() { return 6}
    // exports.friendshipUnBlocked = function() { return 7}
    // exports.friendshipBothBlocked = function() { return 8}


    switch (parseInt(updateToStatus)) {
      case friendSQL.friendshipNotFiend():
        logger.info("---> friendshipNotFiend")
        logger.info("------> NO ACTION")
          //SHOULD DO NOTHING
        responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_NO_ACTION + " - 125")
        connection.release();
        break;
      case friendSQL.friendshipPending():
        logger.info("---> friendshipPending")
        if (friendRow.status == friendSQL.friendshipNotFiend()) {
          logger.info("------> friendRow.status == friendSQL.friendshipNotFiend()")
          updateFriendship(req, res, connection, friendRow._id, updateToStatus, userId, pendingFriendId, message)
        } else if (friendRow.status == friendSQL.friendshipRefusedBeforeBeFriend()) {
          logger.info("------> friendRow.status == friendSQL.friendshipRefusedBeforeBeFriend()")
          updateFriendship(req, res, connection, friendRow._id, updateToStatus, userId, pendingFriendId, message)
        } else {
          logger.info("------> NO ACTION")
          responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_NO_ACTION + " - 138")
          connection.release();
        }
        break;
      case friendSQL.friendshipCancelRequest():
        logger.info("---> friendshipCancelRequest")
        updateFriendship(req, res, connection, friendRow._id, friendSQL.friendshipNotFiend(), userId, pendingFriendId, null)
        break;
      case friendSQL.friendshipRefusedBeforeBeFriend():
        logger.info("---> friendshipRefusedBeforeBeFriend")
        if (friendRow.status == friendSQL.friendshipPending()) {
          updateFriendship(req, res, connection, friendRow._id, updateToStatus, userId, pendingFriendId, null)
        } else if (friendRow.status == friendSQL.friendshipRefusedBeforeBeFriend() && friendRow.last_action != userId) {
          //Both have refuced
          updateFriendship(req, res, connection, friendRow._id, friendSQL.friendshipNotFiend(), userId, pendingFriendId, null)
        } else {
          logger.info("------> NO ACTION")
          responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_NO_ACTION + " - 152")
          connection.release();
        }
        break;
      case friendSQL.friendshipFriend():
        logger.info("---> friendshipFriend")
        if (friendRow.status == friendSQL.friendshipPending()) {
          updateFriendship(req, res, connection, friendRow._id, updateToStatus, userId, pendingFriendId, null)
        } else {
          logger.info("------> NO ACTION")
          responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_NO_ACTION + " - 160")
          connection.release();
        }
        break;
      case friendSQL.friendshipRefusedAfterBeFriend():
        logger.info("---> friendshipRefusedAfterBeFriend")
        updateFriendship(req, res, connection, friendRow._id, friendSQL.friendshipNotFiend(), userId, pendingFriendId, null)
        break;
      case friendSQL.friendshipBlocked():
        logger.info("---> friendshipBlocked")
        if (friendRow.status == friendSQL.friendshipFriend()) {
          updateFriendship(req, res, connection, friendRow._id, updateToStatus, userId, pendingFriendId, null)
        } else if (friendRow.status == friendSQL.friendshipBlocked() && friendRow.last_action != userId) {
          //Both have block
          updateFriendship(req, res, connection, friendRow._id, friendSQL.friendshipBothBlocked(), userId, pendingFriendId, null)
        } else {
          responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_NO_ACTION + " - 196")
          connection.release();
        }
        break;
      case friendSQL.friendshipUnBlocked():
        logger.info("---> friendshipUnBlocked")
        if (friendRow.status == friendSQL.friendshipBlocked() && friendRow.last_action == userId) {
          updateFriendship(req, res, connection, friendRow._id, friendSQL.friendshipFriend(), userId, pendingFriendId, null)
        } else if (friendRow.status == friendSQL.friendshipBothBlocked()) {
          updateFriendship(req, res, connection, friendRow._id, friendSQL.friendshipBlocked(), pendingFriendId, pendingFriendId, null)
        } else {
          responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_NO_ACTION + " - 210")
          connection.release();
        }
        break;
      case friendSQL.friendshipBothBlocked():
        logger.info("---> friendshipBothBlocked")
        responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_NO_ACTION + " - 215")
        connection.release();
        break;
      default:
        logger.info("---> default")
        responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_NO_ACTION + " - 219")
        connection.release();
        break;
    }
  }


  function updateFriendship(req, res, connection, friendshipId, status, userId, pendingFriendId, message) {
    logger.info("---> updateFriendship - friendshipId : " + friendshipId + " | status : " + status + " | userId : " + userId + " | pendingFriendId : " + pendingFriendId)
    connection.query(friendSQL.updateFriendshipStatusAndWhoCancel(friendshipId, status, userId), function(err, results) {
      dbUtil.responseSQL("updateFriendshipStatusAndWhoCancel", err, results)
      if (err) {
        connection.release();
        responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
      } else {
        if (friendSQL.friendshipPending() == status) {
          createRoom.createConversation(req, res, connection, tokenUser, userIdField, message, function(err, results) {
            returnReloadedFriendship(req, res, connection, pendingFriendId)
          })
        } else {
          returnReloadedFriendship(req, res, connection, pendingFriendId)
        }
      }
    });
  }

  function returnReloadedFriendship(req, res, connection, pendingFriendId) {
    var userId = encryptUtil.userId(req.headers)

    connection.query(friendSQL.findFriendshipByUserIdOrToUserId(userId, pendingFriendId), function(err, friendRow) {
      dbUtil.responseSQL("findFriendshipByUserIdOrToUserId", err, friendRow)

      if (err) {
        responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
      } else if (friendRow.length == 1) {
        responder.result200(req, res, enumUtil.API().FRIENDSHIP, friendRow[0])
      } else {
        responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.API().FRIENDSHIP.ERROR_TO_MANY_USERS)
      }

      connection.release();

    })
  }
}
