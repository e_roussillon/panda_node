var FB = require('facebook-node-sdk');
var unirest = require('unirest');
var confFacebook = require('../../../utils/facebookUtil.js');
var logger = require('../../../utils/logger.js');
var responder = require('../../../utils/responder.js');
var enumUtil = require('../../../utils/enumResponse.js');
var userSQL = require('../../../models/userSQL.js');
var loginGeneric = require('../user/login');
var dbUtil = require('../../../utils/dbUtil');
var login = require('../../local/user/login.js');

//Facebook image
//http://graph.facebook.com/10153422407309263/picture?width=480

exports.loginFacebook = function(req, res, connection, socialTokenField, deviceIdField, platformDeviceField) {
    
    logger.info("##### --> USER")
    
    var facebook = new FB({
        appID: confFacebook.facebook_id(),
        secret: confFacebook.facebook_sec(),
        scope: ['public_profile', 'email', 'user_friends', 'user_birthday']
    }).setAccessToken(socialTokenField);

    facebook.api('/me?fields=birthday,name,email,gender', function(err, data) {

        if (err != null) {
            connection.release();
            responder.result403(req, res, enumUtil.API().AUTHENTICATION, enumUtil.AUTHENTICATION().ERROR_TOKEN)
        } else {
            // {
            //   "birthday": "04/03/1985",
            //   "name": "Doudou Le Doudou",
            //   "gender": "male",
            //   "id": "535184262"
            // }
            //	http://graph.facebook.com/{ID}/picture?width=480

            var socialIdField = data.id != null ? data.id : ""
            var nameField = data.name != null ? data.name : ""
            var emailField = data.email != null ? data.email : ""
            var birthdayField = data.birthday != null ? data.birthday : ""
            var genderField = data.gender != null ? (data.gender === "male" ? true : false) : ""
            var socialTypeField = "facebook"

            logger.info("##### --> USER")
            logger.info(nameField)

            getSocialUser(req, res, connection, socialIdField, emailField, nameField, birthdayField, genderField, socialTypeField, deviceIdField, platformDeviceField)
        }

    });
}

exports.loginGoogle = function(req, res, connection, socialTokenField, userIdField, deviceIdField, platformDeviceField) {
    logger.info("------ URL ---------")
    logger.info("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + socialTokenField)
    unirest.get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + socialTokenField)
        .header('Accept', 'application/json')
        .end(function(response) {

            if (response.error != false) {
                connection.release();
                responder.result403(req, res, enumUtil.API().AUTHENTICATION, enumUtil.AUTHENTICATION().ERROR_TOKEN)
            } else {
                if (response.body.sub != null && response.body.sub.toString() === userIdField.toString()) {
                    
                    // "picture": "https://lh3.googleusercontent.com/-poze6WbLlXg/AAAAAAAAAAI/AAAAAAAAACU/ZrXlyBwE1EM/s96-c/photo.jpg",
                    var socialIdField = userIdField
            		var nameField = response.body.name != null ? response.body.name : ""
            		var emailField = response.body.email_verified == "true" ? response.body.email : null
            		var birthdayField = "" //Need to add an other API call
            		var genderField = "" //Need to add an other API call
            		var socialTypeField = "google"
                    getSocialUser(req, res, connection, socialIdField, emailField, nameField, birthdayField, genderField, socialTypeField, deviceIdField, platformDeviceField)

                } else {
                    connection.release();
                    responder.result403(req, res, enumUtil.API().AUTHENTICATION, enumUtil.AUTHENTICATION().ERROR_TOKEN)
                }
            }
        });
}

exports.loginCal = function(req, res, connection, socialIdField, emailField, pseudoField, birthdayField, genderField, socialTypeField, deviceIdField, platformDeviceField) {
    getSocialUser(req, res, connection, socialIdField, emailField, pseudoField, birthdayField, genderField, socialTypeField, deviceIdField, platformDeviceField)
}

function getSocialUser(req, res, connection, socialIdField, emailField, pseudoField, birthdayField, genderField, socialTypeField, deviceIdField, platformDeviceField) {
    connection.query(userSQL.findUserBySocialId(socialIdField, socialTypeField), function(err, reportedRows) {
        dbUtil.responseSQL("findUserBySocialId", err, reportedRows)

        if (err) {
            connection.release();
            responder.result406(req, res, enumUtil.API().CREATE_USER, err)
        } else {
            if (reportedRows.length == 0) {
                saveSocialUser(req, res, connection, socialIdField, emailField, pseudoField, birthdayField, genderField, socialTypeField, deviceIdField, platformDeviceField)
            } else {
                loginGeneric.loginSocial(req, res, connection, reportedRows[0]._id, platformDeviceField, deviceIdField);
            }
        }
    })
}

function saveSocialUser(req, res, connection, socialIdField, emailField, pseudoField, birthdayField, genderField, socialTypeField, deviceIdField, platformDeviceField) {
    var user = {
        "social_id": socialIdField,
        "social_type": socialTypeField,
        "email": emailField,
        "pseudo": pseudoField,
        "birthday": birthdayField,
        "gender": genderField
    }

    if (socialIdField == null || socialIdField.length == 0) {
        connection.release();
    	responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_SOCIAL_ID_EMPTY, data)
    } else if (socialTypeField == null || socialTypeField.length == 0) {
        connection.release();
		responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_SOCIAL_TYPE_EMPTY, data)
    } else if (login.loginUserTestParam(req, res, connection, pseudoField, emailField, birthdayField, genderField, user)  && login.loginUserTestPlatformParam(req, res, connection, platformDeviceField, deviceIdField)) {
        connection.query(userSQL.insertSocialUser(emailField, pseudoField, socialIdField, socialTypeField), function(err, reportedRows) {
            dbUtil.responseSQL("insertSocialUser", err, reportedRows)

            if (err) {
                connection.release();
                if (err.code === "ER_DUP_ENTRY") {
                    responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_USER_EXIST, user)
                } else { 
                    responder.result406(req, res, enumUtil.API().CREATE_USER, err)
                }
            } else {
                if (reportedRows[0].insertId != 0) {
                    loginGeneric.loginSocial(req, res, connection, reportedRows[0].insertId, platformDeviceField, deviceIdField);
                } else {
                    connection.release();
                    responder.result403(req, res, enumUtil.API().AUTHENTICATION, enumUtil.AUTHENTICATION().ERROR_TOKEN)
                }
            }
        })
    }
}