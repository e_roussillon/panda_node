var moment = require('moment');

var dbUtil = require('../../../utils/dbUtil.js');
var responder = require('../../../utils/responder.js');
var encryptUtil = require('../../../utils/encryptUtil.js');
var enumUtil = require('../../../utils/enumResponse.js');

var userSQL = require('../../../models/userSQL.js');
var reportedSQL = require('../../../models/reportedSQL.js');
var authSQL = require('../../../models/authenticationSQL.js');

var logger = require('../../../utils/logger.js');

var userId = 0
var token = ""
var minPseudo = 4
var maxPseudo = 30

exports.loginUserTestPlatformParam = function(req, res, connection, platformDeviceField, deviceIdField) {
    return userParam(req, res, connection, platformDeviceField, deviceIdField)
}

exports.loginUserTestParam = function(req, res, connection, pseudoField, emailField, birthdayField, genderField, data) {
    logger.info("pseudo : ", pseudoField)
    logger.info("email : ", emailField)
    logger.info("birthday : ", birthdayField)
    logger.info("gender : ", genderField)
    logger.info("data : ", data)

    if (pseudoField == null || pseudoField.length < minPseudo || pseudoField.length > maxPseudo) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_PSEUDO_EMPTY, data)
        return false
    } else if (isNaN(pseudoField) == false || validatePseudo(pseudoField) == false) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_PSEUDO_NOT_VALID, data)
        return false
    } if (emailField == null || emailField.length == 0) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_EMAIL_EMPTY, data)
        return false
    } else if (validateEmail(emailField) == false) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_EMAIL_NOT_VALID, data)
        return false
    } else if (birthdayField == null || birthdayField.length == 0) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_BIRTHDAY_EMPTY, data)
        return false
    } else if (validateBirthday(birthdayField) == false) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_BIRTHDAY_NOT_VALID, data)
        return false
    } else if (genderField == null || genderField.length == 0) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_GENDER_EMPTY, data)
        return false
    } else {
        return true
    }
}

exports.updateUserTestParam = function(req, res, connection, pseudoField, emailField, birthdayField, genderField, data) {
    logger.info("pseudo : ", pseudoField)
    logger.info("email : ", emailField)
    logger.info("birthday : ", birthdayField)
    logger.info("gender : ", genderField)
    logger.info("data : ", data)

    if (pseudoField == null || pseudoField.length < minPseudo || pseudoField.length > maxPseudo) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_PSEUDO_EMPTY, data)
        return false
    } else if (isNaN(pseudoField) == false || validatePseudo(pseudoField) == false) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_PSEUDO_NOT_VALID, data)
        return false
    } if (emailField == null || emailField.length == 0) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_EMAIL_EMPTY, data)
        return false
    } else if (validateEmail(emailField) == false) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_EMAIL_NOT_VALID, data)
        return false
    } else if (birthdayField == null || birthdayField.length == 0) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_BIRTHDAY_EMPTY, data)
        return false
    } else if (validateBirthday(birthdayField) == false) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_BIRTHDAY_NOT_VALID, data)
        return false
    } else if (genderField == null || genderField.length == 0) {
        if (connection != null) {
            connection.release();
        }
        responder.result403WithData(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_GENDER_EMPTY, data)
        return false
    } else {
        return true
    }
}

exports.loginUser = function(req, res, connection, emailField, passwordField, platformDeviceField, deviceIdField) {
    if (userParam(req, res, connection, platformDeviceField, deviceIdField) == true) {
        connection.query(userSQL.findUserByEmailAndPsw(emailField, passwordField), function(err, userRows) {
            dbUtil.responseSQL("findUserByEmailAndPsw", err, userRows)

            if (err) {
                connection.release();
                responder.result406(req, res, enumUtil.API().GENERIC_USER, err)
            } else {
                if (userRows.length == 1) {
                    loginGeneric(req, res, connection, userRows[0].user_id, platformDeviceField, deviceIdField)
                } else {
                    responder.result403(req, res, enumUtil.API().GENERIC_USER, enumUtil.GENERIC_USER().ERROR_USER_NOT_FOUND)
                    connection.release();
                }
            }
        });
    }
}

exports.loginSocial = function(req, res, connection, userIdField, platformDeviceField, deviceIdField) {
    loginGeneric(req, res, connection, userIdField, platformDeviceField, deviceIdField)
}

function userParam(req, res, connection, platformDeviceField, deviceIdField) {
    if (platformDeviceField == null || platformDeviceField == 0 || (platformDeviceField != "android" && platformDeviceField != "ios") == true) {
        if (connection != null) {
            connection.release();
        }
        responder.result403(req, res, enumUtil.API().GENERIC_USER, enumUtil.GENERIC_USER().ERROR_PLATFORM_DEVICE)
        return false
    } else if (deviceIdField == null || deviceIdField.length == 0) {
        if (connection != null) {
            connection.release();
        }
        responder.result403(req, res, enumUtil.API().GENERIC_USER, enumUtil.GENERIC_USER().ERROR_DEVICE_ID)
        return false
    } else {
        return true
    }
}

function loginGeneric(req, res, connection, userIdField, platformDeviceField, deviceIdField) {
    connection.query(reportedSQL.findReported(userIdField), function(err, reportedRows) {
        dbUtil.responseSQL("findReported", err, reportedRows)

        if (err) {
            connection.release();
            responder.result406(req, res, enumUtil.API().GENERIC_USER, err)
        } else {
            if (reportedRows.length == 0) {

                userId = userIdField
                token = encryptUtil.genereteToken(userId, platformDeviceField, deviceIdField, new Date().toISOString())

                connection.query(authSQL.findAuthenByUserId(userId), function(err, authRows) {
                    dbUtil.responseSQL("findAuthenByUserId", err, authRows)

                    //User already exist
                    if (authRows.length == 1) {
                        //Update Token
                        connection.query(authSQL.updateAuthenToken(userId, token), function(err, authTokenRows) {
                            dbUtil.responseSQL("updateAuthenToken", err, authTokenRows)

                            if (err) {
                                responder.result406(req, res, enumUtil.API().CREATE_USER, err)
                            } else {
                                responder.result200(req, res, enumUtil.API().GENERIC_USER, {
                                    "token": token,
                                    "user_id": parseInt(""+userId)
                                })
                            }
                            connection.release();

                        });
                    } else {
                        //User not already exist
                        //Create token
                        connection.query(authSQL.insertAuthen(userId, token), function(err, authTokenRows) {
                            dbUtil.responseSQL("insertAuthen", err, authTokenRows)

                            if (err) {
                                responder.result406(req, res, enumUtil.API().CREATE_USER, err)
                            } else {
                                responder.result200(req, res, enumUtil.API().GENERIC_USER, {
                                    "token": token,
                                    "user_id": parseInt(""+userId)
                                })
                            }

                            connection.release();
                        });
                    }
                });
            } else {
                //Check is not use is reported
                connection.query(authSQL.removedAuthenByUserId(userIdField), function(err, result) {
                    dbUtil.responseSQL("removedAuthenByUserId", err, result)
                    responder.result403(req, res, enumUtil.API().GENERIC_USER, enumUtil.GENERIC_USER().ERROR_USER_BLOCKED)
                    connection.release();
                });
            }
        }
    });
}

function validatePseudo(speudo) {
    // First check if any value was actually set
    if (speudo.length == 0) return false;
    // Now validate the email format using Regex
    var re = /^[a-zA-Z0-9\ ][\ \a-zA-Z0-9]+$/i;
    return re.test(speudo);
}

function validateEmail(email) {
    // First check if any value was actually set
    if (email.length == 0) return false;
    // Now validate the email format using Regex
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
    return re.test(email);
}

function validateBirthday(date) {
    var dateWrapper = new Date(date);
    return !isNaN(dateWrapper.getDate());
}
