var logger = require('../../../utils/logger.js');
var responder  = require('../../../utils/responder.js');
var login = require('../../local/user/login.js');
var enumUtil = require('../../../utils/enumResponse.js');
var dbUtil = require('../../../utils/dbUtil.js');
var encryptUtil  = require('../../../utils/encryptUtil.js');

var userSQL  = require('../../../models/userSQL.js');

var minPassword = 6

module.exports = {
    '/api/user/register': {
        methods: ['post'],
        fn: function(req, res){
            
            var pseudoField = req.body.pseudo
            var emailField = req.body.email
            var birthdayField = req.body.birthday
            var genderField = ((req.body.gender === 'true') == true) ? 1 : 0
            var passwordField = encryptUtil.password(req.headers)
            var deviceIdField = encryptUtil.deviceId(req.headers)
            var platformDeviceField = req.body.platform_device

            if (pseudoField == null) {
                pseudoField = ""
            } else {
                pseudoField = pseudoField.replace(/^\s+|\s+$/g, "");
            }

            var errorLogin = login.loginUserTestParam(req, res, null, pseudoField, emailField, birthdayField, genderField, null)

            if (passwordField == null || passwordField.length == 0) {
                responder.result403(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_PASSWORD_EMPTY)
            } else if (passwordField == null || passwordField.length < minPassword) {
                responder.result403(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_PASSWORD_TO_SMALL)
            } else if (errorLogin && login.loginUserTestPlatformParam(req, res, null, platformDeviceField, deviceIdField)) {
                req.db.getConnection(function(err, connection) {
                    connection.query(userSQL.insertUser(emailField.toLowerCase(), pseudoField.trim().toLowerCase(), birthdayField, genderField, passwordField), function(err, results) {
                        dbUtil.responseSQL("insertUser", err, results)

                        if (err == null) {
                            //Login need to release the connection
                            login.loginUser(req, res, connection, emailField.toLowerCase(), passwordField, platformDeviceField, deviceIdField)
                        } else if (err.code === "ER_DUP_ENTRY") {
                            responder.result403(req, res, enumUtil.API().CREATE_USER, enumUtil.CREATE_USER().ERROR_USER_EXIST)
                            connection.release();
                        } else {
                            responder.result406(req, res, enumUtil.API().CREATE_USER, err)
                            connection.release();
                        }
                    });
                });
            }
        }
    }
};