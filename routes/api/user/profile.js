var responder  = require('../../../utils/responder.js');
var encryptUtil  = require('../../../utils/encryptUtil.js');
var connectionUtil = require('../../../utils/connectionUtil.js');
var enumUtil = require('../../../utils/enumResponse.js');
var dbUtil = require('../../../utils/dbUtil.js');
var logger = require('../../../utils/logger.js');
var login = require('../../local/user/login.js');

var userSQL  = require('../../../models/userSQL.js');
var filterSQL = require('../../../models/filterSQL.js');
var friendSQL = require('../../../models/friendshipSQL.js');
var friendship = require('../../local/friend/friendship.js');

module.exports = {

	/**
	* Get User
	*/
    '/api/user/profile_user': {
        methods: ['post'],
        fn: function(req, res){

            connectionUtil.checkToken(req, res, function(err, response) {
                if (response == true) {
                    var userId = encryptUtil.userId(req.headers)
                    var userIdField = req.body.user_id

                    if (userIdField == null || userIdField.length == 0) {
                        responder.result403(req, res, enumUtil.API().PROFIL, enumUtil.PROFIL().ERROR_USER_EMPTY)
                    } else {
                        req.db.getConnection(function(err, connection) {
                            connection.query(userSQL.findUserById(userIdField), function(err, users) {
                                dbUtil.responseSQL("findUserById", err, users)

                                if (err) {
                                    connection.release();
                                    responder.result406(req, res, enumUtil.API().PROFIL, err)
                                } else {
                                    if (users.length == 1) {
                                        connection.query(filterSQL.selectFilterByUserId(userIdField, 1), function(err, filters) {
                                            dbUtil.responseSQL("selectFilterByUserId", err, filters)

                                            if (err) {
                                                connection.release();
                                                responder.result406(req, res, enumUtil.API().PROFIL, err)
                                            } else {

                                                connection.query(friendSQL.findFriendshipByUserIdOrToUserId(userId, userIdField), function(err, friend) {
                                                    dbUtil.responseSQL("findFriendshipByUserIdOrToUserId", err, friend)

                                                    if (err) {
                                                        responder.result406(req, res, enumUtil.API().PROFIL, err)
                                                    } else {
                                                        var profileUser = null
                                                        if (friend.length > 0) {
                                                            profileUser = new friendship.ProfileFriend(users[0], filters, friend[0])
                                                        } else {
                                                            profileUser = new friendship.ProfileFriend(users[0], filters, null)
                                                        }
                                                        responder.result200(req, res, enumUtil.API().PROFIL, profileUser)
                                                    }
                                                    connection.release();
                                                });

                                            }

                                        });
                                    } else {
                                        connection.release();
                                        responder.result403(req, res, enumUtil.API().PROFIL, enumUtil.PROFIL().ERROR_USER_NOT_FOUND)
                                    }
                                }

                            });
                        });
                    }
                }
            });
        }
    },

    // /**
    // * Get User
    // */
    // '/api/user/profile': {
    //     methods: ['post'],
    //     fn: function(req, res){
    //
    //         connectionUtil.checkToken(req, res, function(err, response) {
    //             if (response == true) {
    //                 var userId = encryptUtil.userId(req.headers)
    //
    //                 if (userId == null || userId.length == 0) {
    //                     responder.result403(req, res, enumUtil.API().PROFIL, enumUtil.PROFIL().ERROR_USER_EMPTY)
    //                 } else {
    //                     req.db.getConnection(function(err, connection) {
    //                         connection.query(userSQL.findUserById(userId), function(err, results) {
    //                             dbUtil.responseSQL("findUserById", err, results)
    //
    //                             if (err) {
    //                                 responder.result406(req, res, enumUtil.API().PROFIL, err)
    //                             } else {
    //                                 if (results.length == 1) {
    //                                     responder.result200(req, res, enumUtil.API().PROFIL, results[0])
    //                                 } else {
    //                                     responder.result403(req, res, enumUtil.API().PROFIL, enumUtil.PROFIL().ERROR_USER_NOT_FOUND)
    //                                 }
    //                             }
    //                             connection.release();
    //                         });
    //                     });
    //                 }
    //             }
    //         });
    //     }
    // },

    /**
    * Get User
    */
    '/api/user/update_profile': {
        methods: ['post'],
        fn: function(req, res){

            connectionUtil.checkToken(req, res, function(err, response) {
                if (response == true) {

                    var userId = encryptUtil.userId(req.headers)

                    var pseudoField = req.body.pseudo.replace(/^\s+|\s+$/g, "");
                    var emailField = req.body.email
                    var birthdayField = req.body.birthday
                    var genderField = ((req.body.gender === 'true') == true) ? 1 : 0
                    var detailsField = req.body.details == null ? "" : req.body.details

                    if (userId == null || userId.length == 0) {
                        responder.result403(req, res, enumUtil.API().PROFIL, enumUtil.PROFIL().ERROR_USER_EMPTY)
                    } else if (login.updateUserTestParam(req, res, null, pseudoField, emailField, birthdayField, genderField, null)){

                        req.db.getConnection(function(err, connection) {
                            connection.query(userSQL.updateUserById(userId, emailField, pseudoField, birthdayField, genderField, detailsField), function(err, results) {
                                dbUtil.responseSQL("updateUserById", err, results)

                                if (err == null) {
                                    connection.query(userSQL.findUserById(userId), function(err, results) {
                                        dbUtil.responseSQL("findUserById", err, results)

                                        if (err) {
                                            responder.result406(req, res, enumUtil.API().UPDATE_PROFIL, err)
                                        } else {
                                            if (results.length == 1) {
                                                responder.result200(req, res, enumUtil.API().UPDATE_PROFIL, results[0])
                                            } else {
                                                responder.result403(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_USER_NOT_FOUND)
                                            }
                                        }
                                        connection.release();
                                    });
                                } else if (err.code === "ER_DUP_ENTRY") {
                                    responder.result403(req, res, enumUtil.API().UPDATE_PROFIL, enumUtil.UPDATE_PROFIL().ERROR_USER_EXIST)
                                    connection.release();
                                } else {
                                    responder.result406(req, res, enumUtil.API().UPDATE_PROFIL, err)
                                    connection.release();
                                }
                            });
                        });
                    }
                }
            });
        }
    }
};
