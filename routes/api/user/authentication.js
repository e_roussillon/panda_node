var responder = require('../../../utils/responder.js');
var encryptUtil = require('../../../utils/encryptUtil.js');
var connectionUtil = require('../../../utils/connectionUtil.js');
var enumUtil = require('../../../utils/enumResponse.js');
var dbUtil = require('../../../utils/dbUtil.js');
var logger = require('../../../utils/logger.js');

var login = require('../../local/user/login.js');
var loginSocial = require('../../local/social/login.js');
var authSQL = require('../../../models/authenticationSQL.js');

module.exports = {

    /**
     * AUTHENTICATION FACEBOOK
     */
    '/api/user/authentication/facebook': {
        methods: ['post'],
        fn: function(req, res) {
            var tokenField = encryptUtil.token(req.headers)
            var deviceIdField = encryptUtil.deviceId(req.headers)
            var platformDeviceField = req.body.platform_device

            if (tokenField == null || tokenField.length == 0) {
                responder.result403(req, res, enumUtil.API().AUTHENTICATION, enumUtil.AUTHENTICATION().ERROR_TOKEN)
            } else if (login.loginUserTestPlatformParam(req, res, null, platformDeviceField, deviceIdField)) {
                req.db.getConnection(function(err, connection) {
                    loginSocial.loginFacebook(req, res, connection, tokenField, deviceIdField, platformDeviceField)
                });
            }
        }
    },

    '/api/user/authentication/google': {
        methods: ['post'],
        fn: function(req, res) {
            var tokenField = encryptUtil.token(req.headers)
            var googleIdField = encryptUtil.googleId(req.headers)
            var deviceIdField = encryptUtil.deviceId(req.headers)
            var platformDeviceField = req.body.platform_device

            if (tokenField == null || tokenField.length == 0) {
                responder.result403(req, res, enumUtil.API().AUTHENTICATION, enumUtil.AUTHENTICATION().ERROR_TOKEN)
            } else if (login.loginUserTestPlatformParam(req, res, null, platformDeviceField, deviceIdField)) {

                req.db.getConnection(function(err, connection) {
                    loginSocial.loginGoogle(req, res, connection, tokenField, googleIdField, deviceIdField, platformDeviceField)
                });

            }
        }
    },

    /**
     * AUTHENTICATION
     */
    '/api/user/authentication/social_cal': {
        methods: ['post'],
        fn: function(req, res) {
            var socialIdField = req.body.social_id
            var emailField = req.body.email
            var pseudoField = req.body.pseudo
            var birthdayField = req.body.birthday
            var genderField = req.body.gender
            var socialTypeField = req.body.social_type
            var deviceIdField = encryptUtil.deviceId(req.headers)
            var platformDeviceField = req.body.platform_device

            req.db.getConnection(function(err, connection) {
                loginSocial.loginCal(req, res, connection, socialIdField, emailField, pseudoField, birthdayField, genderField, socialTypeField, deviceIdField, platformDeviceField)
            });
        }
    },

    /**
     * AUTHENTICATION
     */
    '/api/user/authentication': {
        methods: ['post'],
        fn: function(req, res) {
            var emailField = req.body.email
            var passwordField = encryptUtil.password(req.headers)
            var deviceIdField = encryptUtil.deviceId(req.headers)
            var platformDeviceField = req.body.platform_device

            if (emailField == null || emailField.length == 0 || validateEmail(emailField) == false) {
                responder.result403(req, res, enumUtil.API().AUTHENTICATION, enumUtil.AUTHENTICATION().ERROR_EMAIL)
            } else if (passwordField == null || passwordField.length == 0) {
                responder.result403(req, res, enumUtil.API().AUTHENTICATION, enumUtil.AUTHENTICATION().ERROR_PASSWORD)
            } else {
                req.db.getConnection(function(err, connection) {
                    login.loginUser(req, res, connection, emailField.toLowerCase(), passwordField, platformDeviceField, deviceIdField)
                });
            }
        }
    },

    /**
     * RENEW AUTH
     */
    '/api/user/renew_auth': {
        methods: ['post'],
        fn: function(req, res) {

            connectionUtil.renewToken(req, res, function(err, response) {
                if (response == true) {
                    var userId = encryptUtil.userId(req.headers)
                    var newAuthorization = encryptUtil.renewToken(encryptUtil.authorization(req.headers))

                    req.db.getConnection(function(err, connection) {
                        connection.query(authSQL.updateAuthenToken(userId, newAuthorization), function(err, results) {
                            dbUtil.responseSQL("updateAuthenToken", err, results)

                            if (err) {
                                responder.result406(req, res, enumUtil.API().RENEW_AUTH, err)
                            } else {
                                responder.result200(req, res, enumUtil.API().RENEW_AUTH, {
                                    "token": newAuthorization,
                                    "user_id": parseInt(""+userId)
                                })
                            }
                            connection.release();
                        });
                    });
                } else {
                    responder.result403(req, res, enumUtil.API().RENEW_AUTH, enumUtil.RENEW_AUTH().ERROR_CAN_NOT_RENEW_TOKEN)
                }
            });
        }
    }
};


function validateEmail(email) {
    // First check if any value was actually set
    if (email.length == 0) return false;
    // Now validate the email format using Regex
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
    return re.test(email);
}
