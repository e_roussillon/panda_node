var responder = require('../../../utils/responder.js');
var encryptUtil = require('../../../utils/encryptUtil.js');
var connectionUtil = require('../../../utils/connectionUtil.js');
var enumUtil = require('../../../utils/enumResponse.js');
var dbUtil = require('../../../utils/dbUtil.js');

var friendship = require('../../local/friend/friendship.js');
var userSQL = require('../../../models/userSQL.js');
var notifSQL = require('../../../models/notificationSQL.js');
var friendSQL = require('../../../models/friendshipSQL.js');

module.exports = {

    /**
     * Get My Friendship
     */
    '/api/friend/users': {
        methods: ['post'],
        fn: function(req, res) {
            connectionUtil.checkToken(req, res, function(err, response) {
                if (response == true) {

                    var userId = encryptUtil.userId(req.headers)

                    if (userId == null || userId.length == 0) {
                        responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_EMPTY)
                    } else {

                        req.db.getConnection(function(err, connection) {
                            connection.query(userSQL.findUsersExpectI(userId), function(err, results) {
                                dbUtil.responseSQL("findUserById", err, results)
                                connection.release();

                                if (err) {
                                    responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
                                } else {
                                    responder.result200(req, res, enumUtil.API().FRIENDSHIP, results)
                                }
                            });
                        });
                    }
                }
            });
        }
    },

    /**
     * Set Friendship
     */
    '/api/friend/update': {
        methods: ['post'],
        fn: function(req, res) {
            connectionUtil.checkToken(req, res, function(err, response) {
                if (response == true) {
                    var userId = encryptUtil.userId(req.headers)
                    var toBeFriend = req.body.to_user_id
                    var updateToStatus = req.body.friend_ship

                    if (userId == null || userId.length == 0) {
                        responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_EMPTY)
                    } else if (toBeFriend == null || toBeFriend.length == 0) {
                        responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_TO_BE_FRIEND_EMPTY)
                    } else {
                        req.db.getConnection(function(err, connection) {
                            friendship.updateFriendship(req, res, connection, toBeFriend, updateToStatus)
                        });
                    }
                }
            });
        }
    },

    // /**
    //  * Create Friendship
    //  */
    // '/api/friend/ask_friendship': {
    //     methods: ['post'],
    //     fn: function(req, res) {
    //         connectionUtil.checkToken(req, res, function(err, response) {
    //             if (response == true) {

                    // var userId = encryptUtil.userId(req.headers)
                    // var toBeFriend = req.body.to_be_friend_id

                    // if (userId == null || userId.length == 0) {
                    //     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_EMPTY)
                    // } else if (toBeFriend == null || toBeFriend.length == 0) {
                    //     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_TO_BE_FRIEND_EMPTY)
                    // } else {

    //                     req.db.getConnection(function(err, connection) {
    //                         connection.query(userSQL.findUserById(userId), function(err, results) {
    //                             dbUtil.responseSQL("findUserById", err, results)

    //                             if (err) {
    //                                 responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
    //                                 connection.release();
    //                             } else {

    //                                 if (results.length == 1) {
    //                                     friendship.pendingFriendship(req, res, connection, toBeFriend, function(error, result) {
    //                                         //Add new notification
    //                                         if (result) {
    //                                             connection.query(notifSQL.insertNotification(toBeFriend, userId, notifSQL.notificationAskFriendship(), ""), function(err, results) {
    //                                                 dbUtil.responseSQL("insertNotification", err, results)

    //                                                 if (err) {
    //                                                     responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
    //                                                 } else {
    //                                                     responder.result200(req, res, enumUtil.API().FRIENDSHIP, null)
    //                                                 }
    //                                                 connection.release();
    //                                             });
    //                                         }
    //                                     });
    //                                 } else {
    //                                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_NOT_FOUND)
    //                                     connection.release();
    //                                 }
    //                             }
    //                         });
    //                     });

    //                 }
    //             }
    //         });
    //     }
    // },

    // /**
    //  * Accept Friendship
    //  */
    // '/api/friend/accept_friendship': {
    //     methods: ['post'],
    //     fn: function(req, res) {
    //         connectionUtil.checkToken(req, res, function(err, response) {
    //             if (response == true) {

    //                 var userId = encryptUtil.userId(req.headers)
    //                 var toBeFriend = req.body.to_be_friend_id
    //                 var accepted = (req.body.accepted === 'true')

    //                 if (userId == null || userId.length == 0) {
    //                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_EMPTY)
    //                 } else if (toBeFriend == null || toBeFriend.length == 0) {
    //                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_TO_BE_FRIEND_EMPTY)
    //                 } else {

    //                     req.db.getConnection(function(err, connection) {
    //                         connection.query(userSQL.findUserById(userId), function(err, results) {
    //                             dbUtil.responseSQL("findUserById", err, results)

    //                             if (err) {
    //                                 responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
    //                                 connection.release();
    //                             } else {

    //                                 if (results.length == 1) {

    //                                     if (accepted == true) {
    //                                         //FriendShip will realise the connection
    //                                         friendship.acceptFriendship(req, res, connection, toBeFriend, function(error, result) {
    //                                             if (result) {
    //                                                 responder.result200(req, res, enumUtil.API().FRIENDSHIP, null)
    //                                             }
    //                                         });
    //                                     } else {
    //                                         //FriendShip will realise the connection
    //                                         friendship.refuseFriendship(req, res, connection, toBeFriend, function(error, result) {
    //                                             if (result) {
    //                                                 responder.result200(req, res, enumUtil.API().FRIENDSHIP, null)
    //                                             }
    //                                         });
    //                                     }
    //                                 } else {
    //                                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_NOT_FOUND)
    //                                     connection.release();
    //                                 }
    //                             }
    //                         });
    //                     });
    //                 }
    //             }
    //         });
    //     }
    // },

    // /**
    //  * Block Friendship
    //  */
    // '/api/friend/block_friendship': {
    //     methods: ['post'],
    //     fn: function(req, res) {
    //         connectionUtil.checkToken(req, res, function(err, response) {
    //             if (response == true) {

    //                 var userId = encryptUtil.userId(req.headers)
    //                 var toBeFriend = req.body.to_be_friend_id

    //                 if (userId == null || userId.length == 0) {
    //                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_EMPTY)
    //                 } else if (toBeFriend == null || toBeFriend.length == 0) {
    //                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_TO_BE_FRIEND_EMPTY)
    //                 } else {

    //                     req.db.getConnection(function(err, connection) {
    //                         connection.query(userSQL.findUserById(userId), function(err, results) {
    //                             dbUtil.responseSQL("findUserById", err, results)

    //                             if (err) {
    //                                 responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
    //                                 connection.release();
    //                             } else {
    //                                 if (results.length == 1) {
    //                                     //FriendShip will realise the connection
    //                                     friendship.blockFriendship(req, res, connection, toBeFriend, function(error, result) {

    //                                         if (result) {
    //                                             responder.result200(req, res, enumUtil.API().FRIENDSHIP, null)
    //                                         }
    //                                     });
    //                                 } else {
    //                                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_NOT_FOUND)
    //                                     connection.release();
    //                                 }
    //                             }
    //                         });
    //                     });
    //                 }
    //             }
    //         });
    //     }
    // },

    // /**
    //  * Get My Friendship
    //  */
    // '/api/friend/get_friendship': {
    //     methods: ['post'],
    //     fn: function(req, res) {
    //         connectionUtil.checkToken(req, res, function(err, response) {
    //             if (response == true) {

    //                 var userId = encryptUtil.userId(req.headers)

    //                 if (userId == null || userId.length == 0) {
    //                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_EMPTY)
    //                 } else {

    //                     req.db.getConnection(function(err, connection) {
    //                         connection.query(userSQL.findUserById(userId), function(err, results) {
    //                             dbUtil.responseSQL("findUserById", err, results)

    //                             if (err) {
    //                                 responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
    //                                 connection.release();
    //                             } else {
    //                                 if (results.length == 1) {
    //                                     connection.query(friendSQL.findFriendshipByUserId(userId), function(err, results) {
    //                                         dbUtil.responseSQL("findFriendshipByUserId", err, results)

    //                                         if (err) {
    //                                             responder.result406(req, res, enumUtil.API().FRIENDSHIP, err)
    //                                         } else {
    //                                             var response = new friendship.FriendshipList(results, userId);
    //                                             responder.result200(req, res, enumUtil.API().FRIENDSHIP, response)
    //                                         }
    //                                         connection.release();
    //                                     });
    //                                 } else {
    //                                     responder.result403(req, res, enumUtil.API().FRIENDSHIP, enumUtil.FRIENDSHIP().ERROR_USER_NOT_FOUND)
    //                                     connection.release();
    //                                 }
    //                             }
    //                         });
    //                     });
    //                 }
    //             }
    //         });
    //     }
    // }
};
