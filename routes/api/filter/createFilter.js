var responder  = require('../../../utils/responder.js');
var encryptUtil  = require('../../../utils/encryptUtil.js');
var connectionUtil = require('../../../utils/connectionUtil.js');
var enumUtil = require('../../../utils/enumResponse.js');
var dbUtil = require('../../../utils/dbUtil.js');
var logger = require('../../../utils/logger.js');
var _ = require("underscore");

var filterSQL = require('../../../models/filterSQL.js');

module.exports = {

    /**
	* Create Filter
	*/
    '/api/filter/create_filter': {
        methods: ['post'],
        fn: function(req, res){
        	connectionUtil.checkToken(req, res, function(err, response) {
                if (response == true) {
                    var userId = encryptUtil.userId(req.headers)
                    var name = req.body.name
                    var isHighlight = (req.body.is_highlight === 'true')
                    var date = new Date().toISOString()

                    if (userId == null || userId.length == 0) {
                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_USER_EMPTY)
                    } else if (name == null || name.length == 0) {
                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_NAME_EMPTY)
                    } else {
                        req.db.getConnection(function(err, connection) {
                            connection.query(filterSQL.insertFilter(userId, name, isHighlight), function(err, results) {
                                dbUtil.responseSQL("removedAuthenSocket", err, results)

                                if (err) {
                                   responder.result406(req, res, enumUtil.API().FILTER, err)
                                } else {
                                    responder.result200(req, res, enumUtil.API().FILTER, {"filter_id" : results.insertId})
                                }
                                connection.release();
                            });
                        });
                    }
                }
        	});
        }
    },

    /**
	* Update Filter
	*/
    '/api/filter/update_filter': {
        methods: ['post'],
        fn: function(req, res){
        	connectionUtil.checkToken(req, res, function(err, response) {
                if (response == true) {
                    var userId = encryptUtil.userId(req.headers)
                    var filterId = req.body.filter_id
                    var name = req.body.name
                    var isHighlight = (req.body.is_highlight === 'true')

                    if (filterId == null || filterId.length == 0) {
                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_FILTER_EMPTY)
                    } else if (userId == null || userId.length == 0) {
                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_USER_EMPTY)
                    } else if (name == null || name.length == 0) {
                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_NAME_EMPTY)
                    }  else {
                        req.db.getConnection(function(err, connection) {
                            connection.query(filterSQL.updateFilter(filterId, userId, name, isHighlight), function(err, results) {
                                dbUtil.responseSQL("updateFilter", err, results)

                                if (err) {
                                   responder.result406(req, res, enumUtil.API().FILTER, err)
                                } else {
                                    if (results.affectedRows == 0) {
                                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_FILTER_NOT_FOUND)
                                    } else {
                                        responder.result200(req, res, enumUtil.API().FILTER, null)
                                    }
                                }
                                connection.release();
                            });
                        });
                    }
                }
        	});
        }
    },

    /**
	* Remove Filter
	*/
    '/api/filter/remove_filter': {
        methods: ['post'],
        fn: function(req, res){
        	connectionUtil.checkToken(req, res, function(err, response) {
                if (response == true) {
                    var userId = encryptUtil.userId(req.headers)
                    var filterId = req.body.filter_id

                    if (filterId == null || filterId.length == 0) {
                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_FILTER_EMPTY)
                    } else {
                        req.db.getConnection(function(err, connection) {
                            connection.query(filterSQL.removedFilter(filterId, userId), function(err, results) {
                                dbUtil.responseSQL("removedFilter", err, results)

                                if (err) {
                                   responder.result406(req, res, enumUtil.API().FILTER, err)
                                } else {
                                    if (results.affectedRows == 0) {
                                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_FILTER_NOT_FOUND)
                                    } else {
                                        responder.result200(req, res, enumUtil.API().FILTER, null)
                                    }
                                }
                                connection.release();
                            });
                        });
                    }
                }
        	});
        }
    },

    /**
	* Get Filters
	*/
    '/api/filter/filters': {
        methods: ['post'],
        fn: function(req, res){
        	connectionUtil.checkToken(req, res, function(err, response) {
                if (response == true) {
                    var isHighlight = (req.body.is_highlight === 'true') ? 1 : 0

                    var userId = encryptUtil.userId(req.headers)
                    logger.info("userId : " + userId)

                    if (userId == null || userId.length == 0) {
                        responder.result403(req, res, enumUtil.API().FILTER, enumUtil.FILTER().ERROR_USER_EMPTY)
                    } else {
                        req.db.getConnection(function(err, connection) {
                            connection.query(filterSQL.selectFilterByUserId(userId, isHighlight), function(err, results) {
                                dbUtil.responseSQL("selectFilterByUserId", err, results)

                                if (err) {
                                   responder.result406(req, res, enumUtil.API().FILTER, err)
                                } else {
                                    responder.result200(req, res, enumUtil.API().FILTER, results)
                                }
                                connection.release();
                            });
                        });
                    }
                }
        	});
        }
    }
};
