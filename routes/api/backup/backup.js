var logger = require('../../../utils/logger.js');
var connectionUtil = require('../../../utils/connectionUtil.js');
var encryptUtil = require('../../../utils/encryptUtil.js');

var backup = require('../../local/backup/backup.js');

module.exports = {
    '/api/backup': {
        methods: ['post'],
        fn: function(req, res){

          connectionUtil.checkToken(req, res, function(err, response) {
            if (response == true) {
              var lastUpdateField = req.body.last_backup_date
              var userIdField = encryptUtil.userId(req.headers)

              req.db.getConnection(function(err, connection) {
                backup.loadBackup(req, res, connection, userIdField, lastUpdateField)
              })
            }
          })
        }
    }
};
