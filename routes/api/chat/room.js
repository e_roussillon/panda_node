var logger = require('../../../utils/logger.js');
var responder = require('../../../utils/responder.js');
var encryptUtil = require('../../../utils/encryptUtil.js');
var connectionUtil = require('../../../utils/connectionUtil.js');
var enumUtil = require('../../../utils/enumResponse.js');
var dbUtil = require('../../../utils/dbUtil');

var publicRoom = require('../../local/chat/room.js');
var publicCreateRoom = require('../../local/chat/createRoom.js');

var publicRoomSQL = require('../../../models/publicRoomSQL.js');
var privateRoomSQL = require('../../../models/privateRoomSQL');
var userSQL = require('../../../models/userSQL');

var app = require('../../../app.js');



module.exports = {

  /**
   * Create Filter  country->administrative_area_level_1->administrative_area_level_2->locality->route
   */
  // '/api/chat/get-room': {
  //     methods: ['post'],
  //     fn: function(req, res){
  //     	connectionUtil.checkToken(req, res, function(err, response) {
  //             if (response == true) {

  //                 publicRoom.startNewRoom(4, "paris", "city", -1, -1, function(err, response) {
  //                     if (err) {
  //                        responder.result200(req, res, err)
  //                     } else {
  //                         responder.result200(req, res, response)
  //                     }
  //                 });

  //                 // var lngField = req.body.lng
  //                 // var latField = req.body.lat
  //                 // var countryField = req.body.country
  //                 // var stateField = req.body.state
  //                 // var cityField = req.body.city
  //                 // var neighborhoodField = req.body.neighborhood
  //                 // var streetField = req.body.street
  //                 // var numberField = req.body.number

  //                 // if (lngField == null || lngField.length == 0) {
  //                 //     responder.result403(req, res, errorlng, "lng is invalid")
  //                 // } else if (latField == null || latField.length == 0) {
  //                 //     responder.result403(req, res, errorlat, "lat is invalid")
  //                 // } else if (countryField == null || countryField.length == 0) {
  //                 //     responder.result403(req, res, errorCountry, "Country is invalid")
  //                 // } else {
  //                     // req.db.getConnection(function(err, connection) {




  //                     //     connection.query(publicRoomSQL.insertPublicRoom(1, "France", "country", -1, -1), function(err, rows, fields) {
  //                     //         connection.release();
  //                     //         if (err) {
  //                     //            responder.result500(req, res, "error")
  //                     //         } else {
  //                     //             responder.result200(req, res, rows)
  //                     //         }
  //                     //     });
  //                     // });
  //                 // }
  //             }
  //     	});
  //     }
  // },

  '/api/chat/create_conversation': {
    methods: ['post'],
    fn: function(req, res) {
      connectionUtil.checkToken(req, res, function(err, response) {
        if (response == true) {
          var tokenUser = encryptUtil.userId(req.headers)
          var userIdField = req.body.user_id

          if (userIdField == null || userIdField.length == 0) {
            responder.result403(req, res, enumUtil.API().ROOM, enumUtil.ROOM().ERROR_USER_EMPTY)
          } else if (tokenUser == userIdField) {
            responder.result403(req, res, enumUtil.API().ROOM, enumUtil.ROOM().ERROR_CONVERSATION_WITH_YOUR_SELF)
          } else {
            req.db.getConnection(function(err, connection) {
              publicCreateRoom.createConversation(req, res, connection, tokenUser, userIdField, "")
            });
          }
        }
      });
    }
  },

  '/api/chat/get_all_conversations': {
    methods: ['post'],
    fn: function(req, res) {
      connectionUtil.checkToken(req, res, function(err, response) {
        if (response == true) {
          var userId = encryptUtil.userId(req.headers)

          req.db.getConnection(function(err, connection) {
            connection.query(privateRoomSQL.selectAllPrivateRoomsByUserId(userId), function(err, results) {
              dbUtil.responseSQL("selectAllPrivateRoomsByUserId", err, results)

              if (err) {
                responder.result406(req, res, enumUtil.API().ROOM, err)
              } else {
                responder.result200(req, res, enumUtil.API().ROOM, results)
              }
              connection.release();
            });
          });
        }
      });
    }
  },

  '/api/chat/send_broadcast_message': {
    methods: ['post'],
    fn: function(req, res) {
      connectionUtil.checkToken(req, res, function(err, response) {
        if (response == true) {

          var token = encryptUtil.authorization(req.headers)
          var isBroadcastField = true
          var isRoomGeneralField = (req.body.is_room_general === 'true')
          var roomField = req.body.room_id
          var messageField = req.body.message == null ? "" : req.body.message

          if (roomField == null || roomField.length == 0) {
            responder.result403(req, res, enumUtil.API().ROOM, enumUtil.ROOM().ERROR_ROOM_EMPTY)
          } else {
            var response = new publicRoom.Message(isBroadcastField, isRoomGeneralField, roomField, messageField);
            app.sendMessageToRoom(req, res, token, roomField, response);
          }
        }
      });
    }
  },

  // '/api/chat/send-public-message': {
  //     methods: ['post'],
  //     fn: function(req, res){
  //         connectionUtil.checkToken(req, res, function(err, response) {
  //             if (response == true) {

  //                 var token = req.headers["authorization"]
  //                 var isBroadcastField = (req.body.is_broadcast === 'true')
  //                 var isRoomGeneralField = (req.body.id_room_general === 'true')
  //                 var roomField = req.body.room_id
  //                 var messageField = req.body.message == null ? "" : req.body.message

  //                 if (roomField == null || roomField.length == 0) {
  //                     responder.result403(req, res, enumUtil.API().ROOM, enumUtil.ROOM().ERROR_ROOM_EMPTY)
  //                 } else {
  //                     var response = new publicRoom.Message(isBroadcastField, isRoomGeneralField, roomField, messageField);
  //                     app.sendMessageToRoom(req, res, token, roomField, response);
  //                 }
  //             }
  //         });
  //     }
  // },

  // '/api/chat/send-private-message': {
  //     methods: ['post'],
  //     fn: function(req, res){
  //         connectionUtil.checkToken(req, res, function(err, response) {
  //             if (response == true) {

  //                 var token = req.headers["authorization"]
  //                 var roomField = req.body.room_id
  //                 var messageField = req.body.message == null ? "" : req.body.message

  //                 if (roomField == null || roomField.length == 0) {
  //                     responder.result403(req, res, enumUtil.API().ROOM, enumUtil.ROOM().ERROR_ROOM_EMPTY)
  //                 } else {
  //                     var response = new publicRoom.Message(isBroadcastField, isRoomGeneralField, roomField, messageField);
  //                     app.sendMessageToRoom(req, res, token, roomField, response);
  //                 }
  //             }
  //         });
  //     }
  // }

  // DO NOT DELETE METHOD - THE METHOD WORK, I DO NOT WANT TO LET OPEN FOR THE MOMENT
  // '/api/chat/delete-room': {
  //     methods: ['post'],
  //     fn: function(req, res){
  //         connectionUtil.checkToken(req, res, function(err, response) {
  //             if (response == true) {

  //                 var publicRoomIdField = req.body.publicRoomId
  //                 var withChildField = (req.body.withChild === 'true')

  //                 if (publicRoomIdField == null || publicRoomIdField.length == 0) {
  //                     responder.result403(req, res, errorRoomId, "room is invalid")
  //                 } else {
  //                     publicRoom.deleteRoom(publicRoomIdField, withChildField, function(err, response) {
  //                         if (err) {
  //                            responder.result200(req, res, err)
  //                         } else {
  //                             responder.result200(req, res, response)
  //                         }
  //                     });
  //                 }
  //             }
  //         });
  //     }
  // }
};
